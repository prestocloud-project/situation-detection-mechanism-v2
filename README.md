### Introduction to the Situation Detection Mechanism

The Situation Detection Mechanism (SDM) is a component responsible to detect situations which will eventually trigger the re-deployment and reconfiguration of the cloud application processing topology. To do so, it leverages the WSO2 Siddhi library.

The SDM accepts input in the form of json rule messages, and json monitoring messages, both of which are retrieved from a RabbitMQ Broker. It also creates its output as json messages sent to the same Broker. Scalability rule messages convey the constraints of the DevOps on the runtime execution of the application. Depending on the monitoring data input, these are triggered and produce a 'situation'. Moreover, monitoring data input is used to create an average of the `response_time` metric (only for the edge version of components), which is transmitted to the Broker to be used by a Context Analysis service aimed at edge devices.

Communication with the Broker is achieved using a dedicated Java RabbitMQ library (for situations and scalability rules input) and a custom version of the Siddhi IO RabbitMQ extension (to receive monitoring data and publish aggregations). Monitoring messages are assumed to be sent by individual application component instances over regular intervals. Monitoring topics are assumed to follow a `monitoring_data_topic.application_hex_id.application_hex_instance_id.component_location_type.component_name.component_instance_id` structure. The `monitoring_data_topic` can be set using the relevant parameter of the SDM. The `application_hex_id`, `application_hex_instance_id` and `component_instance_id` can be freely chosen, the only constraint being that `component_instance_id` should be unique for each deployed application component *instance*. The `component_name`, also called `fragid`  is the name of the general application fragment type. The output of the SDM are json messages informing of the situations which have been detected, encoded in json. 

#### Input of the Situation Detection Mechanism

---

The Scalability rules of the DevOps are sent to a RabbitMQ Broker using json rule messages which are produced by a suitable UI. These messages are sent to the topic of the broker indicated by the `scalability_rules_manipulation_topic` parameter of the configuration file of the component, which is described below, as are also other parameters which are required for the connection to the broker.

An example of a json rule message follows:

```json
{

  "callbackURL": "http://5.6.7.8:8080/api/v1/callback/policy/9",
  "graphHexID": "F1sJQuetaU",
  "graphInstanceHexID": "ubBFY9f3e0",
  "policyHexID": "Xs3UieyyCj",
  "expressionModelList": [
    {
      "function": "AVG",
      "componentHexID": "B2azqQD28Y",
      "componentName": " Video_Transcoder ",
      "metric": "cpu_perc",
      "operand": "GREATER",
      "threshold": "70"
    },
    {
      "function": "AVG",
      "componentHexID": "B2azqQD28Y",
      "componentName": "Video_Transcoder",
      "metric": "mem_perc",
      "operand": "GREATER",
      "threshold": "70"
    }
  ],
  "policyPeriod": 600,
  "inertialPeriod": 1200,

  "actionModelsList": [
    {
      "ruleAction": "scaleOut",
      "componentName": "Video_Transcoder",
      "componentHexID": "B2azqQD28Y"
    }
  ],
  "creation": true
}
```

The above message corresponds to the following scalability rule:

> **For fragment** Video_Transcoder, **if** **avg**(cpu_perc)>70 **and**  **avg**(mem_perc)>70 **for** *Timeperiod* = 600 seconds **and no similar rule has arrived for** *Inertialperiod*= 1200 seconds then **scale out**   

 Json monitoring messages are produced by monitoring agents installed on each VM/edge device. An example of a json monitoring message follows:

```json
{
  "disk_available": 10664.874934197273,
  "res_inst": "5c4c:a145:1a6d:165a:75c8:3c20:9883:46a4",
  "net_sent": 66.56974874934731,
  "loadBalancerMonitoringModels": [
    {
      "linvocations_sec": 0,
      "loadbalancer_for_fragid": "",
      "linvocations_responsetime": 0
    }
  ],
  "cpu_user": 11.251587765416915,
  "cpu_io_wait": 32.70020900000089,
  "throughput_rate_fps": 79.79988906194065,
  "latency_ms": 15.748031056236956,
  "fragid": "b9e9c1888f",
  "cpu_sys": 18.195730001834942,
  "mem_perc": 13.928150896000902,
  "packets_sent": 87.74182582080718,
  "mem_free": 97.99430813362241,
  "packets_received": 12.725430815791594,
  "net_rec": 74.1870053652409,
  "cpu_perc": 84.6958210419062,
  "response_time_ms": 66.92189467172797,
  "timestamp": 1562943827
}
```

If a rule containing a monitoring metric not defined above is received, then this metric is added as a top-level metric in the above list of expected metrics (i.e as a sibling of `loadBalancerMonitoringModels` and not as its child).  

Moreover, the SDM receives input from events containing information on predicted values of monitoring data. An example monitoring event follows:

```json
{
  "prediction_disk_available": 14.928814499126963,
  "res_inst": "5c4c:a145:1a6d:165a:75c8:3c20:9883:46a4",  
  "prediction_net_sent": 80.92059314247958,
  "prediction_cpu_user": 54.50995416926624,
  "prediction_cpu_io_wait": 74.95179569825935,
  "prediction_throughput_rate_fps": 42.75881252102638,
  "prediction_latency_ms": 28.91930496915711,
  "fragid": "b9e9c1888f",
  "prediction_cpu_sys": 20.728345710216622,
  "prediction_mem_perc": 18.024147302496395,
  "prediction_packets_sent": 37.98146985978311,
  "prediction_mem_free": 25.99788079576807,
  "prediction_packets_received": 69.43932715146006,
  "prediction_net_rec": 23.026318033727556,
  "prediction_cpu_perc": 35.02090100401828,
  "prediction_response_time_ms": 16.481293949660557,
  "prediction_timestamp": 1568807371,
  "prediction_interval": 10m
}
```

#### Running the Situation Detection Mechanism as a Java application

---

In order to start the Situation Detection Mechanism, as a Java application the following steps should be followed:

1. A Java properties file should be created containing the attributes which were initially specified in D5.2. A table containing the updated attributes and their descriptions is included below:
   
   | **Configuration option**                 | **Description**                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
   | ---------------------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
   | **number_of_severity_zones**             | The number of Severity_zones to be created per Scalability rule sent by the PrEstoCloud UI. A larger number indicates that a different response should be tracked for a large amount of different severity_zones                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
   | **broker_ip_address**                    | The IP address of the Communications Broker                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
   | **broker_uri**                           | The complete URI of the Communications Broker. The uri should follow the format `amqp://$username:$password@$hostname:$port` where `username` is the username of the Broker, `password` is the password of the Broker, `hostname` is the hostname of the Broker and `port` is either 5671 (when TLS encryption is required) or 5672 (when no encryption is required). When encryption is required, the Broker should have been appropriately configured to accept certificates which are retrieved from a Vault instance by the Situation Detection Mechanism                                                                                                                                                                                                                                                                                                                                                                                       |
   | **truststore_password**                  | The password for the truststore which is retrieved from the vault address                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
   | **exchange_name**                        | The name of the AMQP exchange which is used by the component                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
   | **mca_fragment_data_topic**              | The name of the AMQP topic which is used to transmit to the Mobile Context Analyzer data related to the response time of fragment instances                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
   | **number_of_events_for_context**         | The number of monitoring events  which should be averaged per component instance, in the `avg_response_time` metric which is transmitted to the Mobile Context Analyzer                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |
   | **monitoring_data_topic**                | The name of the AMQP topic which is used to retrieve monitoring data from the agents installed on processing nodes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
   | **scalability_rules_manipulation_topic** | The name of the AMQP topic which is used to receive and delete Scalability Rules                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
   | **realtime_monitoring_metrics**          | A list of comma-separated metrics which will be monitored by the component, as well as their minimum and maximum values separated by a colon and enclosed in braces. In the case of an unbounded attribute, the unavailability of the maximum or the minimum value is denoted by a single slash(‘-‘). Furthermore, local metrics (i.e collected from each monitoring agent) should be prefixed by the word `local` while aggregate metrics (i.e collected by the load balancer or the lambda proxy monitoring agents) should be prefixed by the word `aggregate`. For example: local_disk_available[0:-],local_cpu_perc[0:100], aggregate_linvocations_sec[0:-]. In the end of the list containing the realtime monitoring metrics, a list of comma-separated fragment names can be added (inside brackets - for example {VideoTranscoder,VideoStreamer}) when it is known that only these fragments should be monitored for the particular metric. |
   | **predicted_monitoring_metrics**         | The predicted monitoring metrics of the application. These are specified as realtime monitoring metrics, with the exception that their prefix is always `prediction_` rather than `local` or `aggregate`.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
   | **unbounded_attribute_samples**          | The maximum number of samples to be used to estimate the upper and/or lower values of an unbounded monitoring attribute                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |
   | **vault_address**                        | The IP address of the Vault installation which will provide secure communication to the Broker                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |
   | **vault_token**                          | The secret token which will be used to verify the secure communication with the Broker                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
   | **keyStorePath**                         | The path to the keyStore (including the name of the keyStore itself), required for the communication to the RabbitMQ Broker **(deprecated in favour of runtime retrieval of certificate when needed)**                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
   | **keyPassphrase**                        | The passphrase of the keyStore **(deprecated in favour of runtime retrieval of certificate when needed)**                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
   | **trustStorePath**                       | The path to the trustStore (including the name of the keyStore itself), required for the communication to the RabbitMQ Broker **(deprecated in favour of runtime retrieval of certificate when needed)**                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
   | **trustPassphrase**                      | The passphrase of the trustStore **(deprecated in favour of runtime retrieval of certificate when needed)**                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
   | **ui_information_topic**                 | The topic which will be used to inform the UI in the case that a relevant Scalability rule has been triggered                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
   | **ui_situations_detected_topic**         | The topic which will be used to inform the UI with the situation which has been detected, also providing a generic recommendation on the number of instances which could be used for adaptation without taking into account any feedback or the current state of the topology. This topic is intended to be used primarily for experimentation and visualization of the Situation Detection capability, as well as a simulation of the action of the RARecom                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |

2. The SDM project should be cloned, and ideally imported and built using Intellij IDEA. The entry point of execution is the main method in the App.java file, and the relevant run configuration can be selected to initiate the SDM. The App.java class accepts as a command-line argument the absolute file path (including the file name) pointing to the Java properties file which was created in step 1.

3. The SDM will now wait for any new Scalability rules sent from the UI when created by the DevOps, subscribing to the `scalability_rules_manipulation_topic` which is specified in the Java properties configuration file. Please note, that if a new Scalability rule contains a metric name which has not been defined in the configuration file, this metric will be considered a fully unbounded metric with values of type 'double', and will be processed as such by the SDM.

4. Once a new rule arrives, it is added to the list of effective rules. The SDM will then create a new situation event whenever the thresholds specified in the rule(s) sent up until now are violated. The SDM will also listen for new rule events (additions/deletions), as specified in step 3. 
   
   An example situation event sent to the Broker is the following:
   
   ```json
   {
     "event": {
       "rule_id": "Xs3UieyyCj",
       "timestamp": 1553785248,
       "res_inst": "",
       "fragid": "1cc5Fragment",
       "zone": 2,
       "action": "scale_out",
       "attributes": {
         "avg_cpu_perc":  92,
         "avg _mem_perc": 71
       }
     }
   }
   ```
   
    The above situation event indicates that at epoch time 1553785248, due to the average values of the `cpu_perc` and `mem_perc`attributes being 92 and 71 respectively, based on the Scalability rule with id `Xs3UieyyCj`, a `scale_out` action has been decided for the fragment whose id is `1cc5Fragment`. Situations are published using the dedicated `AMQP_Publisher` class (in src/main/java/iccs/gr/presto/messaging).
   
   In addition to situations, the SDM continuously calculates the average of the `response_time` metric, and transmits these values to the `mca_fragment_data_topic` topic of the Broker, adhering to the format described below:
   
   ```json
   {
       "event": {
           "avg_response_time": 58.5028759952596,
           "fragid": "FaceDetector",
           "res_inst": "5c4c:a145:1a6d:165a:75c8:3c20:9883:46a4"
       }
   }
   ```

#### Running the SDM as a Docker Container

---

To run the SDM as a Docker container, Docker-compose should be available and the project should be cloned to a local directory on your hard disk. Inside this directory, switch to the `docker-build` folder and firstly edit the `configuration.properties` file to point to the appropriate broker and vault installation, using the correct credentials.

Then, from the `docker-build` directory, run the following commands in a terminal:

```bash
docker-compose build
docker-compose up
```

The component should then be up and running with the settings specified in the configuration.properties file.

#### More options

---

More options on the configuration of the SDM can be accessed in the Globals.java file, in the gr.iccs.presto package
