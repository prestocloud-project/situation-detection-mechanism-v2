package gr.iccs.presto;

import gr.iccs.presto.utility_classes.Fragment;
import gr.iccs.presto.utility_classes.SubRule;

import java.util.ArrayList;
import java.util.HashMap;

import static gr.iccs.presto.Globals.EMPTY;
import static gr.iccs.presto.Globals.unbounded_metric_samples;
import static gr.iccs.presto.TransformationsEngine.*;

public class SiddhiQuery {
    public static int query_counter = 0;
    private String definition;
    private HashMap<String,String> metric_names_map; // this field contains the mapping from the original naming of the fields belonging to each stream, to their final naming for the output stream
    private ArrayList<String> input_stream_names = new ArrayList<>();
    private String output_stream_name;
    private boolean unbound_attribute_query;



    public SiddhiQuery (String definition,String output_stream_name)
    {
        this.definition = definition;
        this.metric_names_map = new HashMap<>();
        this.output_stream_name = output_stream_name;
    }

    public SiddhiQuery (String definition,HashMap<String,String>metric_names_map,String output_stream_name)
    {
        this.definition = definition;
        this.metric_names_map = metric_names_map;
        this.output_stream_name = output_stream_name;
    }


    public SiddhiQuery (ArrayList<SubRule> sub_rules_involving_fragment, Fragment fragment, int time_window){

        output_stream_name = "output_query_stream_"+fragment.getFragment_name()+"_"+query_counter;

        String aggregation_alias_of_metric  = EMPTY;

        ArrayList<String> aggregation_operands = new ArrayList<>();
        ArrayList<String> metrics = new ArrayList<>();
        ArrayList<String> operands = new ArrayList<>();
        ArrayList<String> fragment_names = new ArrayList<>();
        ArrayList<String> metric_thresholds = new ArrayList<>();

        boolean aggregated_metrics_required_for_rule = false;
        boolean local_metrics_required_for_rule = false;

        for (SubRule sub_rule : sub_rules_involving_fragment) {

            String current_metric = sub_rule.getMetric_name();

            if (!aggregated_metrics_required_for_rule) {
                aggregated_metrics_required_for_rule = is_aggregated_metric(fragment.getMonitoring_metrics().get(current_metric));
            }
            if (!local_metrics_required_for_rule) {
                local_metrics_required_for_rule = is_local_metric(fragment.getMonitoring_metrics().get(current_metric));
            }

            //String aggregation_operand_1 = "avg";
            aggregation_operands.add(get_aggregation_function(sub_rule));
            fragment_names.add(sub_rule.getMonitored_fragment_id());
            //String metric_1 = "cpu";
            metrics.add(sub_rule.getMetric_name());
            //String produced_metric_1_name = aggregation_operand_1 + "_" + metric_1;

            //String metric_1_operand = ">";
            operands.add(get_conditional_operand(sub_rule.getOperand()));
            //String metric_1_threshold = "50";
            metric_thresholds.add(String.valueOf(sub_rule.getThreshold()));
        }

        String siddhi_metric_descriptions_string = EMPTY;

        for (int i=0; i<aggregation_operands.size(); i++){
            String siddhi_metric_description;
            if (i<aggregation_operands.size()-1) {
                if (!aggregation_operands.get(i).equals(EMPTY)) {
                    aggregation_alias_of_metric = get_aggregation_alias_of_metric(aggregation_operands.get(i),metrics.get(i));
                    siddhi_metric_description = (aggregation_operands.get(i) + "(" + metrics.get(i) + ") as " + aggregation_alias_of_metric + ", ");
                } else{
                    siddhi_metric_description = metrics.get(i)+", ";
                }
            }else{
                if (!aggregation_operands.get(i).equals(EMPTY)) {
                    aggregation_alias_of_metric = get_aggregation_alias_of_metric(aggregation_operands.get(i),metrics.get(i));
                    siddhi_metric_description = (aggregation_operands.get(i) + "(" + metrics.get(i) + ") as " + aggregation_alias_of_metric);
                    //TODO create new Siddhi stream if necessary
                }else{
                    siddhi_metric_description = metrics.get(i);
                }
            }
            siddhi_metric_descriptions_string = siddhi_metric_descriptions_string+siddhi_metric_description;
        }


        String siddhi_metric_conditions_string = EMPTY;
        for (int i=0; i<metrics.size(); i++){
            if (i<metrics.size()-1) {
                siddhi_metric_conditions_string = siddhi_metric_conditions_string + get_aggregation_alias_of_metric(aggregation_operands.get(i),metrics.get(i)) + operands.get(i) + metric_thresholds.get(i) + " and ";
            }else{
                siddhi_metric_conditions_string = siddhi_metric_conditions_string + get_aggregation_alias_of_metric(aggregation_operands.get(i),metrics.get(i)) + operands.get(i) + metric_thresholds.get(i);
            }

        }

        if (aggregated_metrics_required_for_rule && local_metrics_required_for_rule){
            input_stream_names.add(fragment.getCombined_metrics_monitoring_stream_name());
        }else if (aggregated_metrics_required_for_rule){
            input_stream_names.add(fragment.getAggregate_monitoring_data_stream_name());
        }else if (local_metrics_required_for_rule){
            input_stream_names.add(fragment.getLocal_monitoring_data_stream_name());
        }

        StringBuilder from_statement_sb = new StringBuilder("from ");

        for (int counter=0; counter<input_stream_names.size();counter++) {
            if (counter<input_stream_names.size()-1) {
                from_statement_sb.append(input_stream_names.get(counter) + ", ");
            }else{
                from_statement_sb.append(input_stream_names.get(counter));
            }
        }

        definition =
                        from_statement_sb.toString()+"#window.time("+time_window+" sec ) [fragid == '"+fragment_names.get(0)+"']"+"\n" +
                        "select "+siddhi_metric_descriptions_string+", fragid, res_inst\n"+
                        "having "+siddhi_metric_conditions_string+"\n"+
                        "insert into "+output_stream_name+";\n\n";

        query_counter++;
    }


    public HashMap<String, String> getMetric_names_map() {
        return metric_names_map;
    }

    public void setMetric_names_map(HashMap<String, String> metric_names_map) {
        this.metric_names_map = metric_names_map;
    }

    /**
     * This convenience method creates a Siddhi query which outputs into a suitable stream the calculated upper and lower bounds for a given (partially) unbounded metric, when both bounds are unknown
     * @param metric_name The name of the unbounded metric
     * @return A string containing a Siddhi query describing the processing which should be carried out to derive the bounds of the metric
     */
    public static SiddhiQuery create_bounds_stream_query(String input_stream_name, String metric_name){
        return create_bounds_stream_query(input_stream_name, metric_name,false,false,Double.NaN);
    }



    /**
     * This method creates a Siddhi query which outputs into a suitable stream the calculated upper and lower bounds for a given (partially) unbounded metric, when one or both bounds are unknown. The upper and lower bounds are computed based on the result from the equation of Chebyshev, that for any distribution 96% of its samples will be within 5 standard deviations of the mean.
     * @param metric_name The name of the (partially) unbounded metric
     * @param has_lower_bound Whether the metric has a lower bound
     * @param has_upper_bound Whether the metric has an upper bound
     * @param existing_bound The value of the existing (upper or lower) bound
     * @return A string containing a Siddhi query describing the processing which should be carried out to derive the bound(s) of the metric
     */
    public static SiddhiQuery create_bounds_stream_query(String unbounded_metric_input_stream_name, String metric_name, boolean has_lower_bound, boolean has_upper_bound, double existing_bound) {

        String bounds_stream_query = EMPTY;
        String unbounded_metric_output_stream_name = "unbounded_metric_"+metric_name+";\n";

        if (has_lower_bound && !has_upper_bound) {
            bounds_stream_query = bounds_stream_query+
                    "from " + unbounded_metric_input_stream_name + "#window.length(" + unbounded_metric_samples + ")\n" +
                    "select "+existing_bound*1.0+" as lower_bound, avg(" + metric_name + ")+5*stdDev(" + metric_name + ") as upper_bound\n"+
                    "insert into "+unbounded_metric_output_stream_name;
        }
        else if (has_upper_bound && !has_lower_bound){
            bounds_stream_query = bounds_stream_query+
                    "from " + unbounded_metric_input_stream_name + "#window.length(" + unbounded_metric_samples + ")\n" +
                    "select avg(" + metric_name + ")-5*stdDev(" + metric_name + ") as lower_bound, "+existing_bound*1.0+" as upper_bound\n"+
                    "insert into "+unbounded_metric_output_stream_name;
        }else if (!has_lower_bound && !has_upper_bound){
            bounds_stream_query = bounds_stream_query+
                    "from " + unbounded_metric_input_stream_name + "#window.length(" + unbounded_metric_samples + ")\n" +
                    "select avg(" + metric_name + ")-5*stdDev(" + metric_name + ") as lower_bound, avg(" + metric_name + ")+5*stdDev(" + metric_name + ") as upper_bound\n"+
                    "insert into "+unbounded_metric_output_stream_name;
        }

        SiddhiQuery siddhi_query = new SiddhiQuery(bounds_stream_query,unbounded_metric_output_stream_name);
        siddhi_query.setInput_stream_names(new ArrayList(){{add(unbounded_metric_input_stream_name);}});

        return siddhi_query;
    }



    public String getDefinition() {
        return definition;
    }

    public void setDefinition(String definition) {
        this.definition = definition;
    }

    public ArrayList<String> getInput_stream_names() {
        return input_stream_names;
    }

    public void setInput_stream_names(ArrayList<String> input_stream_names) {
        this.input_stream_names = input_stream_names;
    }

    public String getOutput_stream_name() {
        return output_stream_name;
    }

    public void setOutput_stream_name(String output_stream_name) {
        this.output_stream_name = output_stream_name;
    }
}
