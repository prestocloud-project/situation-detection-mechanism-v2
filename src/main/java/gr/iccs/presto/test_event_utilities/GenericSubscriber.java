package gr.iccs.presto.test_event_utilities;

import consumer.AmqpConsumer;
import exceptions.AlreadySubscribedException;
import exceptions.NoHostException;
import exceptions.SubscribingFailedException;

import java.io.IOException;

public class GenericSubscriber {

    public static  java.util.function.BiConsumer<String, String> consumerFunction = (message, topic) -> doSomething(message, topic);

    //java.util.function.BiConsumer<String, String> consumerFunction = this::doSomething;

    //consumer​.useSSL(keyStorePath, keyPassphrase, trustStorePath, trustPassphrase);
    public static void main(String[] args) {
        AmqpConsumer consumer = new AmqpConsumer("3.124.17.87",false,consumerFunction);
        try {
            consumer.subscribe("#");
        } catch (AlreadySubscribedException | NoHostException | IOException e){
            System.out.println(e.getMessage());
        } catch (SubscribingFailedException s){
                main(new String[]{});
                //waitXminAndRetry(3);
        }
    }
//doSomething is the method that gets executed on every received message
    private static void doSomething(String message, String topic) {
    }

    private void  processing_function(String message, String topic){
        System.out.println("A message was received in topic "+topic);
        System.out.println("MESSAGE "+message);
    }

    private void waitXminAndRetry(int minutes){
        try{
            Thread.sleep(minutes*60*1000);
        }catch (InterruptedException i){
            main(new String[]{});
        }
    }
}
