package gr.iccs.presto.test_event_utilities;

import gr.iccs.presto.Rule;
import gr.iccs.presto.utility_classes.SubRule;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import producer.AmqpProducer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;
import java.util.logging.Level;
import java.util.logging.Logger;

import static gr.iccs.presto.Globals.EMPTY;
import static gr.iccs.presto.test_event_utilities.NewEventPublisher.producer​;
import static gr.iccs.presto.test_event_utilities.TestEventPublisher.number_of_simulated_fragments;
import static gr.iccs.presto.test_event_utilities.UtilityMethods.GeneratedDataCharacter.*;

public class UtilityMethods {

    public static AmqpProducer producer;
    public static String callback_url = "http://192.168.33.2:8000";//"https://jsonplaceholder.typicode.com/posts";
    public static String broker_ip_address = "192.168.33.2";
    public static String[] monitoring_metrics = "disk_available, cpu_sys, cpu_user, cpu_io_wait, cpu_perc, mem_free, mem_perc, net_rec, net_sent, packets_sent, packets_received, throughput_rate_fps, response_time_ms, latency_ms"/*, _custom_metric1_ms_average"*/.split(", ");
    private static String [] unique_fragment_IDs = new String[number_of_simulated_fragments];
    private static String graph_instance_hexID = UUID.randomUUID().toString().replaceAll("-",EMPTY).substring(0,10);
    private static String graph_hexID = UUID.randomUUID().toString().replaceAll("-",EMPTY).substring(0,10);
    private static int unique_fragment_ID_counter=0;
    private static String monitoring_data_topic = "sdm.monitoring";
    private static String policies_topic = "sdm.policies.cruder";
    private static ArrayList <String> policy_ids = new ArrayList<>();

    public enum GeneratedDataCharacter {random, constant,constant_pool}
    private static int CONSTANT_POOL_SIZE = 3;

    public enum rule_action{
        create,delete,delete_all
    }

    static {
        // Create as many UUID's as there are different fragments
        //-------------------------------------------------------------------------------
        for (int i=0; i<number_of_simulated_fragments; i++){
            unique_fragment_IDs[i] = UUID.randomUUID().toString().replaceAll("-",EMPTY).substring(0,10);
        }

    }
    public static String get_next_unique_fragment_ID(){
        return unique_fragment_IDs[unique_fragment_ID_counter++];
    }

    public static void generic_test(Rule rule, String fragment_id, rule_action action, boolean data_generation, GeneratedDataCharacter generated_data_character, boolean publish_rule, int number_of_publishers, int number_of_lambda_proxies, int number_of_load_balancers, int interval_millis, String[] metrics_generated_values_and_bounds
    ) {

        //1. Sanity check---------------------------------------------------------------
        if ((number_of_lambda_proxies>0 && number_of_load_balancers>0)||(number_of_lambda_proxies>number_of_publishers || number_of_load_balancers>number_of_publishers)){
            try{
                throw new Exception("Wrong data input, cannot have this combination of load balancers, lambda proxies and publishers");
            }catch (Exception e){
                e.printStackTrace();
                System.exit(-1);
            }
        }
        //-------------------------------------------------------------------------------
        //2. Publish scalability rule -----------------------------
        String policy_id = UUID.randomUUID().toString().replaceAll("-",EMPTY).substring(0,10);
        if (action.equals(rule_action.create)){
            policy_ids.add(policy_id);
        }
        if (publish_rule) {
            publish_generic_scalability_rule(rule, action,policy_id);
        }
        //-------------------------------

        //3. Configure and start monitoring event publishing through "different" monitoring agents.
        //Each loop iteration reflects a "different" monitoring agent publishing events
        if (data_generation) {
            for (int i=0; i<number_of_publishers; i++) {
                boolean is_lambda_proxy=false,is_load_balancer=false;
                if (number_of_lambda_proxies>0 && i<number_of_lambda_proxies){
                    is_lambda_proxy = true;
                }
                else if (number_of_load_balancers>0 && i<number_of_load_balancers){
                    is_load_balancer = true;
                }
                boolean finalIs_lambda_proxy = is_lambda_proxy;
                boolean finalIs_load_balancer = is_load_balancer;


                Thread monitoring_info_publisher = new Thread(() -> publish_monitoring_events(generated_data_character, interval_millis, finalIs_load_balancer, finalIs_lambda_proxy, fragment_id, metrics_generated_values_and_bounds));
                monitoring_info_publisher.setDaemon(true);
                monitoring_info_publisher.start();
                try {
                    Thread.sleep(interval_millis / number_of_publishers); //To guarantee that monitoring data is equally distributed in time
                }catch (InterruptedException ie){
                    Logger.getAnonymousLogger().log(Level.SEVERE,"Was interrupted while sleeping before proceeding to next monitoring event publisher");
                    ie.printStackTrace();
                }
            }
        }

    }

    private static void publish_monitoring_events(GeneratedDataCharacter generated_data_character, int interval_millis, boolean is_load_balancer, boolean is_lambda_proxy, String fragment_id, String[] metrics_and_thresholds) {

        JSONObject [] monitoring_models = new JSONObject [number_of_simulated_fragments];
        String fragment_instance_id = IPGenerator.getRandomIpV6();

        while (true) {
            HashMap<String, Integer> metrics_and_thresholds_map = new HashMap<>();
            HashMap<String, Boolean> unbounded_metrics_map = new HashMap<>(); //This map contains the name of a metric, along with a boolean indicating whether the particular metric is unbounded (true) or bounded (false). It is filled with appropriate information from the metrics_and_thresholds variable.

            for (int i = 0; i < metrics_and_thresholds.length; i += 3) {
                metrics_and_thresholds_map.put(metrics_and_thresholds[i], Integer.parseInt(metrics_and_thresholds[i + 1]));
                unbounded_metrics_map.put(metrics_and_thresholds[i],Boolean.parseBoolean(metrics_and_thresholds[i + 2]));
            }

            JSONArray aggregated_monitoring_models = new JSONArray();
            JSONObject monitoring_model_1 = new JSONObject();


            if (is_load_balancer || is_lambda_proxy) {
                if (metrics_and_thresholds_map.containsKey("linvocations_responsetime")) {

                    monitoring_model_1.put("linvocations_responsetime",
                            ThreadLocalRandom.current().nextDouble(
                                    get_origin(metrics_and_thresholds_map,
                                            "linvocations_responsetime",
                                            unbounded_metrics_map.containsKey("linvocations_responsetime")),
                                    get_bound(metrics_and_thresholds_map, "linvocations_responsetime",
                                            unbounded_metrics_map.containsKey("linvocations_responsetime"))
                            ));

                    if (metrics_and_thresholds_map.containsKey("linvocations_sec")) {

                        monitoring_model_1.put("linvocations_sec",
                                ThreadLocalRandom.current().nextDouble(
                                        get_origin(metrics_and_thresholds_map,
                                                "linvocations_sec",
                                                unbounded_metrics_map.containsKey("linvocations_sec")),
                                        get_bound(metrics_and_thresholds_map,
                                                "linvocations_sec",
                                                unbounded_metrics_map.containsKey("linvocations_sec"))
                                ));

                    } else {
                        monitoring_model_1.put("linvocations_sec",
                                ThreadLocalRandom.current().nextDouble(
                                        0, 100
                                ));
                    }

                    monitoring_model_1.put("loadbalancer_for_fragid", fragment_id);
                    aggregated_monitoring_models.add(monitoring_model_1);

                } else if (metrics_and_thresholds_map.containsKey("linvocations_sec")) {

                    monitoring_model_1.put("linvocations_responsetime",
                            ThreadLocalRandom.current().nextDouble(
                                    0, 10
                            ));
                    monitoring_model_1.put("linvocations_sec",
                            ThreadLocalRandom.current().nextDouble(
                                    get_origin(metrics_and_thresholds_map,
                                            "linvocations_sec",
                                            unbounded_metrics_map.containsKey("linvocations_sec")),
                                    get_bound(metrics_and_thresholds_map,
                                            "linvocations_sec",
                                            unbounded_metrics_map.containsKey("linvocations_sec"))
                            ));

                    monitoring_model_1.put("loadbalancer_for_fragid", fragment_id);
                    aggregated_monitoring_models.add(monitoring_model_1);

                } else //no lambda metric explicitly mentioned
                {
                    monitoring_model_1.put("linvocations_responsetime",
                            ThreadLocalRandom.current().nextDouble(
                                    0, 10
                            ));
                    monitoring_model_1.put("linvocations_sec",
                            ThreadLocalRandom.current().nextDouble(
                                    0, 100
                            ));
                    monitoring_model_1.put("loadbalancer_for_fragid", fragment_id);
                    aggregated_monitoring_models.add(monitoring_model_1);
                }
            }
            else{
                monitoring_model_1.put("linvocations_responsetime", 0);
                monitoring_model_1.put("linvocations_sec", 0);
                monitoring_model_1.put("loadbalancer_for_fragid", EMPTY);
                aggregated_monitoring_models.add(monitoring_model_1);
            }

            if (is_lambda_proxy){
                for (int i=0; i< monitoring_models.length; i++){
                    monitoring_models[i] = new JSONObject();
                    monitoring_models[i].put("linvocations_responsetime", ThreadLocalRandom.current().nextDouble(0, 10));
                    monitoring_models[i].put("linvocations_sec", ThreadLocalRandom.current().nextDouble(0, 1000));
                    monitoring_models[i].put("loadbalancer_for_fragid",unique_fragment_IDs[i]);
                    aggregated_monitoring_models.add(monitoring_models[i]);
                }
            }

            //Add the rest of the (non-lambda) monitoring metrics to the event
            JSONObject next_event = new JSONObject();
            int current_counter = 0;
            for (String monitoring_metric : monitoring_metrics) {
                double origin, bound; //origin is the lowest possible generated observation, and bound is the highest possible observation to be generated.

                if (generated_data_character.equals(random)) {

                    boolean bound_status = false;
                    if (unbounded_metrics_map.get(monitoring_metric)!=null && unbounded_metrics_map.get(monitoring_metric)){
                        bound_status = true;
                    }
                    origin = get_origin(metrics_and_thresholds_map, monitoring_metric,bound_status);
                    bound = get_bound(metrics_and_thresholds_map, monitoring_metric,bound_status);

                    next_event.put(monitoring_metric, ThreadLocalRandom.current().nextDouble(origin, bound));
                }else if (generated_data_character.equals(constant)) { //Constant values generated for the monitoring metrics
                    if (!metrics_and_thresholds_map.containsKey(monitoring_metric)) {
                        next_event.put(monitoring_metric, 50.0);
                    } else {
                        next_event.put(monitoring_metric, metrics_and_thresholds_map.get(monitoring_metric));
                    }
                }
                else if (generated_data_character.equals(constant_pool)) { //Simulation from a pool of values around a constant value
                    if (!metrics_and_thresholds_map.containsKey(monitoring_metric)) {
                        next_event.put(monitoring_metric, (50.0 - CONSTANT_POOL_SIZE/2) + current_counter % CONSTANT_POOL_SIZE);
                    } else {
                        next_event.put(monitoring_metric, (metrics_and_thresholds_map.get(monitoring_metric) - CONSTANT_POOL_SIZE/2) + current_counter % CONSTANT_POOL_SIZE);
                    }
                    current_counter++;
                }
            }


            next_event.put("loadBalancerMonitoringModels", aggregated_monitoring_models);
            next_event.put("res_inst", fragment_instance_id); //"1cc5:1cc5:1cc5:1cc5:1cc5:1cc5:1cc5:1cc5");
            next_event.put("fragid", fragment_id);
            next_event.put("timestamp", System.currentTimeMillis() / 1000L);

            double random =  ThreadLocalRandom.current().nextDouble(0,1);
            String execution_location =  random>0.5 ? "edge" : "cloud";

            publish(monitoring_data_topic+"."+graph_hexID+"."+graph_instance_hexID+"."+execution_location+"."+fragment_id+"."+fragment_instance_id, next_event.toJSONString());

            try {
                Thread.sleep(interval_millis);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    private static void publish_generic_scalability_rule (Rule rule, rule_action action, String policy_id){
        boolean is_created_now;
        String policy_hex_id;
        if (action.equals(rule_action.create)){
            is_created_now = true;
            policy_hex_id = policy_id;
        }else if (action.equals(rule_action.delete)){
            is_created_now = false;
            if (policy_ids.size()>0) {
                policy_hex_id = policy_ids.get(0);
            }else{
                return;
            }
        }else{
            is_created_now = false;
            policy_hex_id = EMPTY;
        }

        JSONObject message = new JSONObject();
        message.put("callbackURL", callback_url);
        message.put("graphHexID", "F1sJQuetaU");
        message.put("graphInstanceHexID", "ubBFY9f3e0");
        message.put("policyHexID", policy_hex_id);
        JSONArray sub_rule_list = new JSONArray();

        for (SubRule subrule : rule.getSubrules()) {
            JSONObject subrule_object = new JSONObject();
            subrule_object.put("function", subrule.getAggregation_function());
            subrule_object.put("componentHexID", !subrule.getMonitored_fragment_id().equalsIgnoreCase(EMPTY) ? subrule.getMonitored_fragment_id() : "1cc5Fragment");
            subrule_object.put("componentName", !subrule.getMonitored_fragment_name().equalsIgnoreCase(EMPTY) ? subrule.getMonitored_fragment_name() : "ICCS_Fragment");
            subrule_object.put("metric", subrule.getMetric_name());
            subrule_object.put("operand", subrule.getOperand().toString());
            subrule_object.put("threshold", subrule.getThreshold());

            sub_rule_list.add(subrule_object);
        }


        message.put("expressionModelList", sub_rule_list);

        message.put("policyPeriod", "6");
        message.put("inertialPeriod", "10");

        JSONArray action_models_list = new JSONArray();
        JSONObject action_model = new JSONObject();
        action_model.put("ruleAction", rule.getScalability_action());
        action_model.put("componentName", rule.getFragment_name_to_scale());
        action_model.put("componentHexID", rule.getFragment_id_to_scale());
        action_model.put("context", "1");//Default values
        action_model.put("topicUrl", EMPTY);//Default values
        action_model.put("topicName", EMPTY);//Default values
        action_model.put("topicPort", EMPTY);//Default values

        action_models_list.add(action_model);

        message.put("actionModelsList", action_models_list);
        message.put("creation", is_created_now);


        publish(policies_topic, message.toString());
    }


    public static void publish (String topic, String message){
        producer​ = new AmqpProducer(broker_ip_address,false);
        try {
            producer​.publish(message, topic);
        } catch (Exception e) {//IOException e){
            System.out.println(e.getMessage());
        }
    }


    private static int get_origin(HashMap <String,Integer> metrics_and_thresholds_map, String monitoring_metric, boolean unbounded) {
        int origin;
        if (!metrics_and_thresholds_map.containsKey(monitoring_metric)) {
            origin = 0;
        } else {
            if (unbounded) {
                origin = 0;
            } else {
                if (metrics_and_thresholds_map.get(monitoring_metric) <= 50) {
                    origin = 0;
                } else {
                    origin = 2 * metrics_and_thresholds_map.get(monitoring_metric) - 100;
                }
            }
        }
        return origin;
    }

    private static int get_bound(HashMap<String,Integer> metrics_and_thresholds_map, String monitoring_metric, boolean unbounded){

        int bound;
        if (!metrics_and_thresholds_map.containsKey(monitoring_metric)) {
            bound = 100;
        } else {
            if (unbounded){
                bound = 2*metrics_and_thresholds_map.get(monitoring_metric);
            }else {
                if (metrics_and_thresholds_map.get(monitoring_metric) <= 50) {
                    bound = 2 * metrics_and_thresholds_map.get(monitoring_metric);
                } else {
                    bound = 100;
                }
            }
        }
        return  bound;
    }

    public static void delay_seconds(int seconds) {
        try {
            Thread.sleep(seconds* 1000);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
