package gr.iccs.presto.test_event_utilities;

import producer.AmqpProducer;

import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

public class GenericPublisher {

    public static String broker_ip_address = "147.102.17.118";
    public static AmqpProducer producer = new AmqpProducer(broker_ip_address,false);

    public static void main(String [] args) {

        String[] data_to_publish = {

                "{\"callbackURL\":\"https://jsonplaceholder.typicode.com/posts\",\"graphHexID\":\"F1sJQuetaU\",\"graphInstanceHexID\":\"ubBFY9f3e0\",\"policyHexID\":\"Xs3UieyyCn\",\"expressionModelList\":[{\"function\":\"AVG\",\"componentHexID\":\"MN5S6NmR1F\",\"componentName\":\"VideoTranscoder\",\"metric\":\"cpu_perc\",\"operand\":\"GREATER\",\"threshold\":\"80\"},{\"function\":\"AVG\",\"componentHexID\":\"MN5S6NmR1F\",\"componentName\":\"VideoTranscoder\",\"metric\":\"mem_perc\",\"operand\":\"GREATER\",\"threshold\":\"60\"}],\"policyPeriod\":6,\"inertialPeriod\":10,\"actionModelsList\":[{\"context\":\"1\",\"ruleAction\":\"scaleOut\",\"componentName\":\"VideoTranscoder\",\"componentHexID\":\"MN5S6NmR1F\",\"topicUrl\":\"\",\"topicName\":\"\",\"topicPort\":\"\"}],\"creation\":true}",

                "{\"callbackURL\":\"https://jsonplaceholder.typicode.com/posts\",\"graphHexID\":\"F1sJQuetaY\",\"graphInstanceHexID\":\"ubBFY9f3e0\",\"policyHexID\":\"Xs3UieyyCj\",\"expressionModelList\":[{\"function\":\"AVG\",\"componentHexID\":\"MN5S6NmR1F\",\"componentName\":\"VideoTranscoder\",\"metric\":\"cpu_perc\",\"operand\":\"LESS\",\"threshold\":\"50\"},{\"function\":\"AVG\",\"componentHexID\":\"MN5S6NmR1F\",\"componentName\":\"VideoTranscoder\",\"metric\":\"mem_perc\",\"operand\":\"LESS\",\"threshold\":\"40\"}],\"policyPeriod\":6,\"inertialPeriod\":10,\"actionModelsList\":[{\"context\":\"1\",\"ruleAction\":\"scaleIn\",\"componentName\":\"VideoTranscoder\",\"componentHexID\":\"MN5S6NmR1F\",\"topicUrl\":\"\",\"topicName\":\"\",\"topicPort\":\"\"}],\"creation\":true}",

                "{\"callbackURL\":\"https://jsonplaceholder.typicode.com/posts\",\"graphHexID\":\"F1sJQuetaY\",\"graphInstanceHexID\":\"ubBFY9f3e0\",\"policyHexID\":\"Xs3UieyyBn\",\"expressionModelList\":[{\"function\":\"AVG\",\"componentHexID\":\"MN5S6NmR1H\",\"componentName\":\"PercussionDetector\",\"metric\":\"cpu_perc\",\"operand\":\"GREATER\",\"threshold\":\"90\"},{\"function\":\"AVG\",\"componentHexID\":\"MN5S6NmR1H\",\"componentName\":\"PercussionDetector\",\"metric\":\"mem_perc\",\"operand\":\"GREATER\",\"threshold\":\"90\"}],\"policyPeriod\":6,\"inertialPeriod\":10,\"actionModelsList\":[{\"context\":\"1\",\"ruleAction\":\"scaleOut\",\"componentName\":\"PercussionDetector\",\"componentHexID\":\"MN5S6NmR1H\",\"topicUrl\":\"\",\"topicName\":\"\",\"topicPort\":\"\"}],\"creation\":true}",

                "{\"callbackURL\":\"https://jsonplaceholder.typicode.com/posts\",\"graphHexID\":\"F1sJQuetaY\",\"graphInstanceHexID\":\"ubBFY9f3e0\",\"policyHexID\":\"Xs3UieyyCk\",\"expressionModelList\":[{\"function\":\"AVG\",\"componentHexID\":\"MN5S6NmR1G\",\"componentName\":\"FaceDetector\",\"metric\":\"cpu_perc\",\"operand\":\"LESS\",\"threshold\":\"20\"},{\"function\":\"AVG\",\"componentHexID\":\"MN5S6NmR1G\",\"componentName\":\"FaceDetector\",\"metric\":\"mem_perc\",\"operand\":\"LESS\",\"threshold\":\"10\"}],\"policyPeriod\":6,\"inertialPeriod\":10,\"actionModelsList\":[{\"context\":\"1\",\"ruleAction\":\"scaleIn\",\"componentName\":\"FaceDetector\",\"componentHexID\":\"MN5S6NmR1G\",\"topicUrl\":\"\",\"topicName\":\"\",\"topicPort\":\"\"}],\"creation\":true}"

        };
        ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(4);
        for(String data : data_to_publish) {
            executor.execute(new Runnable() {
                @Override
                public void run() {
                    safe_publish(data);
                }
            });
        }
    }
    public static void safe_publish(String message)  {
        try {
            producer.publish(message,"policies.cruder");
            //Thread.sleep(5*1000);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

}

