package gr.iccs.presto.test_event_utilities;

import gr.iccs.presto.Rule;
import gr.iccs.presto.utility_classes.Operand;
import gr.iccs.presto.utility_classes.SubRule;

import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

import static gr.iccs.presto.test_event_utilities.UtilityMethods.*;

public class TestEventPublisher {


    private static int monitoring_data_interval_msec_same_publisher = 10*1000;
    public static int number_of_simulated_fragments = 2;

    public static void main(String[] args) {

        int inter_rules_interval = 10;

        run_multiple_rule_creation_deletion_test(1000,0.8,0.15);
        //run_final_iccs_test();
        //run_different_fragment_scalability_rule_test();
        //run_replay_test();
        //run_simple_constant_1_metric_rule();
        //run_mca_data_generation_monitoring_test();
        //run_classic_metric_test(); //Complex test with two rules and multiple attributes in each rule
        //runtime_custom_metric_test(); //TODO Needs to add a custom metric to the list of transmitted metrics in the UtilityMethods class
        //iccs_integration_test();

        while (true) ;
    }

    private static void run_multiple_rule_creation_deletion_test(int number_of_rules, double percentage_of_creation, double percentage_of_deletion) {

        String monitored_fragment_name= "AditessSUO_cloud1003";
        String fragment_hexID = "fXYCbH0mKY";

        for (int i = 0; i<number_of_rules; i++){

            rule_action action;
            double next_random_double =ThreadLocalRandom.current().nextDouble(0,1);

            if(next_random_double<percentage_of_creation){
                action = rule_action.create;
            }else if (next_random_double<(percentage_of_creation+percentage_of_deletion)){
                action = rule_action.delete;
            }else{
                action = rule_action.delete_all;
            }

            SubRule rule_mem_perc = new SubRule(
                    "AVG",
                    "mem_perc",
                    Operand.valueOf("GREATER"),
                    fragment_hexID,
                    monitored_fragment_name,
                    50
            );
            Rule rule = new Rule(new ArrayList<SubRule>(){{
                add(rule_mem_perc);
            }},"scaleOut");


            generic_test(rule,
                    monitored_fragment_name,
                    action,
                    false,
                    GeneratedDataCharacter.random,
                    true,
                    0,
                    0,
                    0,
                    1000,
                    new String[]{
                            "mem_perc","10","true"
                    });
        }
    }

    private static void run_final_iccs_test() {

        String fragment1 = "VideoTranscoder";
        String fragment2 = "PercussionDetector";
        String fragment3 = "FaceDetector";

        SubRule rule1_memory = new SubRule("AVG",
                "mem_perc",
                Operand.valueOf("GREATER"),
                fragment1,
                fragment1,
                60
        );

        SubRule rule1_cpu = new SubRule("AVG",
                "cpu_perc",
                Operand.valueOf("GREATER"),
                fragment1,
                fragment1,
                80
        );

        Rule rule1 = new Rule(new ArrayList<SubRule>() {{
            add(rule1_memory);
            add(rule1_cpu);
        }}, "scaleOut");

        SubRule rule2_memory = new SubRule("AVG",
                "mem_perc",
                Operand.valueOf("LESS"),
                fragment1,
                fragment1,
                40
        );

        SubRule rule2_cpu = new SubRule("AVG",
                "cpu_perc",
                Operand.valueOf("LESS"),
                fragment1,
                fragment1,
                50);

        Rule rule2 = new Rule(new ArrayList<SubRule>() {{
            add(rule2_memory);
            add(rule2_cpu);
        }}, "scaleIn");

        SubRule rule3_memory = new SubRule("AVG",
                "mem_perc",
                Operand.valueOf("GREATER"),
                fragment2,
                fragment2,
                90);

        SubRule rule3_cpu = new SubRule("AVG",
                "cpu_perc",
                Operand.valueOf("GREATER"),
                fragment2,
                fragment2,
                90);

        Rule rule3 = new Rule(new ArrayList<SubRule>() {{
            add(rule3_memory);
            add(rule3_cpu);
        }}, "scaleOut");

        SubRule rule4_memory = new SubRule("AVG",
                "mem_perc",
                Operand.valueOf("LESS"),
                fragment3,
                fragment3,
                10);

        SubRule rule4_cpu = new SubRule("AVG",
                "cpu_perc",
                Operand.valueOf("LESS"),
                fragment3,
                fragment3,
                20);

        Rule rule4 = new Rule(new ArrayList<SubRule>() {{
            add(rule4_memory);
            add(rule4_cpu);
        }}, "scaleIn");

        generic_test(rule1,
                fragment1,
                rule_action.create,
                true,
                GeneratedDataCharacter.random,
                true,
                3,
                0,
                0,
                500,
                new String[]{
                        "cpu_perc", "85", "true",
                        "mem_perc", "70", "false",
                });


        generic_test(rule2,
                fragment1,
                rule_action.create,
                true,
                GeneratedDataCharacter.random,
                true,
                0,
                0,
                0,
                500,
                new String[]{
                        "cpu_perc", "30", "true",
                        "mem_perc", "40", "false",
                });


        generic_test(rule3,
                fragment2,
                rule_action.create,
                true,
                GeneratedDataCharacter.random,
                true,
                2,
                0,
                0,
                500,
                new String[]{
                        "cpu_perc", "70", "true",
                        "mem_perc", "75", "false",
                });


        generic_test(rule4,
                fragment3,
                rule_action.create,
                true,
                GeneratedDataCharacter.random,
                true,
                2,
                0,
                0,
                500,
                new String[]{
                        "cpu_perc", "70", "true",
                        "mem_perc", "75", "false",
                });

    }

    private static void iccs_integration_test() {
        String fragment1_id = "tf_FaceDet";

        SubRule rule1_memory = new SubRule(
                "AVG",
                "mem_perc",
                Operand.valueOf("GREATER"),
                fragment1_id,
                "tf_FaceDet",
                70);

        SubRule rule1_linvocations = new SubRule
                ("AVG",
                        "cpu_perc",
                        Operand.valueOf("GREATER"),
                        fragment1_id,
                        "tf_FaceDet",
                        70);

        Rule rule1 = new Rule(new ArrayList<SubRule>() {{
            add(rule1_memory);
            add(rule1_linvocations);
        }}, "scaleOut");


        //---------------------------------------------------------

        SubRule rule2_cpu = new SubRule(
                "AVG",
                "cpu_perc",
                Operand.valueOf("LESS"),
                fragment1_id,
                "tf_FaceDet",
                50);


        Rule rule2 = new Rule(new ArrayList<SubRule>(){{
            add(rule2_cpu);
        }},"scaleIn");




// Publish the newly created rules, along with monitoring information following the specification set in the last argument of the generic_test method.


        generic_test(rule1,
                fragment1_id,
                rule_action.create,
                true,
                GeneratedDataCharacter.random,
                true,
                1,
                0,
                0,
                500,
                new String[]{
                        "cpu_perc", "70", "true",
                        "mem_perc", "75", "false",
                });

        generic_test(rule2, fragment1_id,
                rule_action.create,
                false,
                GeneratedDataCharacter.random,
                true,
                0,
                0,
                0,
                monitoring_data_interval_msec_same_publisher,
                new String[]{
                        "cpu_perc", "30","false",
                });

    }

    private static void run_different_fragment_scalability_rule_test(){
        String fragment1_id = "1CC5Fragment_1";
        String fragment2_id = "1CC5Fragment_2";


        SubRule rule1_memory = new SubRule(
                "AVG",
                "mem_perc",
                Operand.valueOf("GREATER"),
                fragment1_id,
                "ICCSFragment_1",
                70);

        SubRule rule1_cpu = new SubRule
                ("AVG",
                        "cpu_perc",
                        Operand.valueOf("GREATER"),
                        fragment2_id,
                        "ICCSFragment_2",
                        70);

        Rule rule1 = new Rule(new ArrayList<SubRule>() {{
            add(rule1_memory);
            add(rule1_cpu);
        }}, "scaleOut");

        generic_test(rule1,
                fragment1_id,
                rule_action.create,
                true,
                GeneratedDataCharacter.random,
                true,
                2,
                1,
                0,
                1000,
                new String[]{
                        "mem_perc", "90", "false",
                });
        generic_test(rule1,
                fragment2_id,
                rule_action.create,
                true,
                GeneratedDataCharacter.random,
                false,
                2,
                0,
                0,
                1000,
                new String[]{
                        "cpu_perc", "90", "false"
                });


    }

    private static void run_replay_test() {
        String fragment1_id = "1CC5Fragment";

        SubRule rule1_memory = new SubRule(
                "AVG",
                "mem_perc",
                Operand.valueOf("GREATER"),
                fragment1_id,
                "ICCSFragment_1",
                60);

        SubRule rule1_cpu = new SubRule
                ("AVG",
                        "cpu_perc",
                        Operand.valueOf("GREATER"),
                        fragment1_id,
                        "ICCSFragment_1",
                        80);

        Rule rule1 = new Rule(new ArrayList<SubRule>() {{
            add(rule1_memory);
            add(rule1_cpu);
        }}, "scaleOut");

        SubRule rule2_memory = new SubRule(
                "AVG",
                "mem_perc",
                Operand.valueOf("LESS"),
                fragment1_id,
                "ICCSFragment_1",
                40);

        SubRule rule2_cpu = new SubRule
                ("AVG",
                        "cpu_perc",
                        Operand.valueOf("LESS"),
                        fragment1_id,
                        "ICCSFragment_1",
                        50);

        Rule rule2 = new Rule(new ArrayList<SubRule>() {{
            add(rule2_memory);
            add(rule2_cpu);
        }}, "scaleIn");


        generic_test(rule1,
                fragment1_id,
                rule_action.create,
                false,
                null,
                true,
                0,
                0,
                0,
                0,
                new String[]{
                        "", "", "",
                        "", "", ""
                });

        generic_test(rule2,
                fragment1_id,
                rule_action.create,
                false,
                null,
                true,
                0,
                0,
                0,
                0,
                new String[]{
                        "", "", "",
                        "", "", ""
                });

    }

    private static void run_mca_data_generation_monitoring_test() {
        String fragment1_id = "1cc5Fragment";
        String fragment2_id = "TestFragment";

        SubRule rule1_cpu_perc = new SubRule(
                "AVG",
                "response_time_ms",
                Operand.valueOf("GREATER"),
                fragment1_id,
                "ICCSFragment_1",
                5);
        Rule rule1 = new Rule(new ArrayList<SubRule>() {{
            add(rule1_cpu_perc);
        }}, "scaleOut");

        generic_test(rule1,
                fragment1_id,
                rule_action.create,
                true,
                GeneratedDataCharacter.random,
                false,
                2,
                1,
                0,
                10*1000,
                new String[]{
                        "response_time_ms", "10","false"
                });
/*
        generic_test(rule1,
                fragment2_id,
                true,
                true,
                GeneratedDataCharacter.constant,
                false,
                5,
                1,
                0,
                10*1000,
                new String[]{
                        "response_time_ms", "10","false"
                });
*/
    }

    private static void run_simple_constant_1_metric_rule() {
        String fragment1_id = "1cc5Fragment";
        SubRule rule1_cpu_perc = new SubRule(
                "AVG",
                "cpu_perc",
                Operand.valueOf("GREATER"),
                fragment1_id,
                "ICCSFragment_1",
                70);
        Rule rule1 = new Rule(new ArrayList<SubRule>() {{
            add(rule1_cpu_perc);
        }}, "scaleOut");

        generic_test(rule1,
                fragment1_id,
                rule_action.create,
                true,
                GeneratedDataCharacter.constant,
                true,
                1,
                1,
                0,
                100,
                new String[]{
                        "cpu_perc", "80","false"
                });

    }

    private static void run_classic_metric_test() {

        String fragment1_id = "1cc5Fragment";
        SubRule rule1_packets = new SubRule(
                "AVG",
                "packets_received",
                Operand.valueOf("GREATER"),
                fragment1_id,
                "ICCSFragment_1",
                700);

        SubRule rule1_memory = new SubRule(
                "AVG",
                "mem_perc",
                Operand.valueOf("GREATER"),
                fragment1_id,
                "ICCSFragment_1",
                70);

        SubRule rule1_linvocations = new SubRule
                ("AVG",
                        "linvocations_sec",
                        Operand.valueOf("GREATER"),
                        fragment1_id,
                        "ICCSFragment_1",
                        20);

        Rule rule1 = new Rule(new ArrayList<SubRule>() {{
            add(rule1_packets);
            add(rule1_memory);
            add(rule1_linvocations);
        }}, "scaleOut");


        //---------------------------------------------------------


        String fragment2_id = get_next_unique_fragment_ID();
        SubRule rule2_cpu = new SubRule(
                "AVG",
                "cpu_perc",
                Operand.valueOf("LESS"),
                fragment2_id,
                "ICCSFragment_2",
                50);

        SubRule rule2_disk = new SubRule("AVG",
                "disk_available",
                Operand.valueOf("GREATER"),
                fragment2_id,
                "ICCSFragment_2",
                10000);

        SubRule rule2_mem = new SubRule("AVG",
                "mem_perc",
                Operand.valueOf("LESS"),
                fragment2_id,
                "ICCSFragment_2",
                40);

        Rule rule2 = new Rule(new ArrayList<SubRule>(){{
            add(rule2_cpu);
            add(rule2_disk);
            add(rule2_mem);
        }},"scaleIn");




// Publish the newly created rules, along with monitoring information following the specification set in the last argument of the generic_test method.


        generic_test(rule1,
                fragment1_id,
                rule_action.create,
                true,
                GeneratedDataCharacter.random,
                true,
                2,
                1,
                0,
                monitoring_data_interval_msec_same_publisher,
                new String[]{
                        "packets_received", "2000", "true",
                        "mem_perc", "96", "false",
                        "linvocations_sec","1000","true"
                });

        generic_test(rule2, fragment2_id,
                rule_action.create,
                true,
                GeneratedDataCharacter.random,
                true,
                1,
                0,
                0,
                monitoring_data_interval_msec_same_publisher,
                new String[]{
                        "cpu_perc", "30","false",
                        "disk_available", "16000","true",
                        "mem_perc", "15", "false"
                });

    }

    private static void runtime_custom_metric_test() {

        String custom_metric_name = "_custom_metric1_ms_average";
        String monitored_fragment_name= "AditessSUO_cloud1003";
        String fragment_hexID = "fXYCbH0mKY";
        SubRule rule3_custom_metric = new SubRule(
                "NONE",
                custom_metric_name,
                Operand.valueOf("GREATER"),
                fragment_hexID,
                monitored_fragment_name,
                10
        );

        Rule rule3 = new Rule(new ArrayList<SubRule>(){{
            add(rule3_custom_metric);
        }},"scaleOut");


        //1. Create data for the rule but do not publish it

        generic_test(
                rule3,
                monitored_fragment_name,
                rule_action.create,
                true,
                GeneratedDataCharacter.random,
                false,
                1,
                0,
                0,
                1000,
                new String[]{
                        custom_metric_name,"10","true"
                });

        //2. Publish data, and see the errors which may result from attributes not expected by Siddhi
        delay_seconds(10);

        //3. Publish the rule, theoretically the errors should stop
        generic_test(rule3,
                monitored_fragment_name,
                rule_action.create,
                false,
                GeneratedDataCharacter.random,
                true,
                5,
                0,
                0,
                1000,
                new String[]{
                        custom_metric_name,"10","true"
                });

    }
}
