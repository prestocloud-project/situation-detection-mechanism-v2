package gr.iccs.presto.test_event_utilities;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class IPGenerator {

    private static String [] IPv6AddressPool = {};

    public static String getIPv6fromPool(){
        int next_char_int = ThreadLocalRandom.current().nextInt(0, 9);
        return IPv6AddressPool[next_char_int];
    }

    public static String getRandomIpV6(){
        char [] generated_address = new char[39];
        for (int i=0;i<39;i++){
            if ((i+1)%5 ==0){
                generated_address[i] = ':';
            }else {
                int next_char_int = ThreadLocalRandom.current().nextInt(0, 15);
                switch (next_char_int) {
                    case 0:
                        generated_address[i] = '0';
                        break;
                    case 1:
                        generated_address[i] = '1';
                        break;
                    case 2:
                        generated_address[i] = '2';
                        break;
                    case 3:
                        generated_address[i] = '3';
                        break;
                    case 4:
                        generated_address[i] = '4';
                        break;
                    case 5:
                        generated_address[i] = '5';
                        break;
                    case 6:
                        generated_address[i] = '6';
                        break;
                    case 7:
                        generated_address[i] = '7';
                        break;
                    case 8:
                        generated_address[i] = '8';
                        break;
                    case 9:
                        generated_address[i] = '9';
                        break;
                    case 10:
                        generated_address[i] = 'a';
                        break;
                    case 11:
                        generated_address[i] = 'b';
                        break;
                    case 12:
                        generated_address[i] = 'c';
                        break;
                    case 13:
                        generated_address[i] = 'd';
                        break;
                    case 14:
                        generated_address[i] = 'e';
                        break;
                    case 15:
                        generated_address[i] = 'f';
                        break;
                }
            }
        }
        return new String(generated_address);
    }
}
