package gr.iccs.presto.test_event_utilities;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import producer.AmqpProducer;

import java.util.concurrent.ThreadLocalRandom;

import static gr.iccs.presto.Globals.EMPTY;
import static gr.iccs.presto.test_event_utilities.UtilityMethods.publish;

public class NewEventPublisher {

    public static AmqpProducer producer​;
    public static String broker_ip_address = "3.120.91.124";

    public static void main(String [] args) {

        String metric = "cpu_perc";
        double value = 80;

        //publish_scalability_rule();
        new Thread()
        {
            public void run() {
                System.out.println("Thread 1");
                publish_random_events_for_metric_around_value(metric,80, true);
            }
        }.start();
        new Thread()
        {
            public void run() {
                System.out.println("New thread 2");
                publish_random_events_for_metric_around_value(metric,20, true);
            }
        }.start();
    }

    private static void publish_random_events_for_metric_around_value(String metric, double value, boolean send_empty) {

        while (true){
            JSONObject next_event = new JSONObject();

            if (!metric.equals("disk_available")) {
                next_event.put("disk_available", ThreadLocalRandom.current().nextDouble(0, 100));
            }
            if (!metric.equals("cpu_sys")) {
                next_event.put("cpu_sys",ThreadLocalRandom.current().nextDouble(0,100));
            }
            if (!metric.equals("cpu_user")) {
                next_event.put("cpu_user",ThreadLocalRandom.current().nextDouble(0,100));
            }
            if (!metric.equals("cpu_io_wait")) {
                next_event.put("cpu_io_wait",ThreadLocalRandom.current().nextDouble(0,100));
            }
            if (!metric.equals("cpu_perc")) {
                next_event.put("cpu_perc",ThreadLocalRandom.current().nextDouble(0,100));
            }
            if (!metric.equals("mem_free")) {
                next_event.put("mem_free",ThreadLocalRandom.current().nextDouble(0,100));
            }
            if (!metric.equals("mem_perc")) {
                next_event.put("mem_perc",ThreadLocalRandom.current().nextDouble(0,100));
            }
            if (!metric.equals("net_rec")) {
                next_event.put("net_rec",ThreadLocalRandom.current().nextDouble(0,100));
            }
            if (!metric.equals("net_sent")) {
                next_event.put("net_sent",ThreadLocalRandom.current().nextDouble(0,100));
            }
            if (!metric.equals("packets_sent")) {
                next_event.put("packets_sent",ThreadLocalRandom.current().nextDouble(0,100));
            }
            if (!metric.equals("packets_received")) {
                next_event.put("packets_received",ThreadLocalRandom.current().nextDouble(0,100));
            }
            if (!metric.equals("throughput_rate_fps")){
                next_event.put("throughput_rate_fps",ThreadLocalRandom.current().nextDouble(0,100));
            }
            if (!metric.equals("response_time_ms")){
                next_event.put("response_time_ms",ThreadLocalRandom.current().nextDouble(0,100));
            }
            if (!metric.equals("latency_ms")){
                next_event.put("latency_ms",ThreadLocalRandom.current().nextDouble(0,100));
            }

            next_event.put(metric, ThreadLocalRandom.current().nextDouble(2*value-100,100));
            //TODO remove below code!

            JSONArray lambda_monitoring_models = new JSONArray();

            JSONObject mm1_parent = new JSONObject(), monitoring_model_1 = new JSONObject();

            if(send_empty) {
                monitoring_model_1.put("linvocations_responsetime", ThreadLocalRandom.current().nextDouble(0, 10));
                monitoring_model_1.put("linvocations_sec", ThreadLocalRandom.current().nextDouble(0, 100));
                monitoring_model_1.put("loadbalancer_for_fragid", "xQddgG8f9");
            }
            else{
                monitoring_model_1.put("linvocations_responsetime", 0.0);
                monitoring_model_1.put("linvocations_sec", 0.0);
                monitoring_model_1.put("loadbalancer_for_fragid", EMPTY);
            }
            //mm1_parent.put("tag",monitoring_model_1);

            JSONObject monitoring_model_2 = new JSONObject();

            if(!send_empty) {
                monitoring_model_2.put("linvocations_responsetime", ThreadLocalRandom.current().nextDouble(0, 10));
                monitoring_model_2.put("linvocations_sec", ThreadLocalRandom.current().nextDouble(0, 100));
                monitoring_model_2.put("loadbalancer_for_fragid", "px221q9tt");
            }
            else {
                monitoring_model_2.put("linvocations_responsetime", 0.0);
                monitoring_model_2.put("linvocations_sec", 0.0);
                monitoring_model_2.put("loadbalancer_for_fragid", EMPTY);
            }
            //mm2_parent.put("tag",monitoring_model_2);

            JSONObject monitoring_model_3 = new JSONObject();

            if(!send_empty) {
                monitoring_model_3.put("linvocations_responsetime", ThreadLocalRandom.current().nextDouble(0, 10));
                monitoring_model_3.put("linvocations_sec", ThreadLocalRandom.current().nextDouble(0, 100));
                monitoring_model_3.put("loadbalancer_for_fragid", "1cc5Fragment");
            }
            else {
                monitoring_model_3.put("linvocations_responsetime", 0);
                monitoring_model_3.put("linvocations_sec", 0);
                monitoring_model_3.put("loadbalancer_for_fragid", EMPTY);
            }
            //mm3_parent.put("tag",monitoring_model_3);

            lambda_monitoring_models.add(monitoring_model_1);
            lambda_monitoring_models.add(monitoring_model_2);
            lambda_monitoring_models.add(monitoring_model_3);

            next_event.put("loadBalancerMonitoringModels",lambda_monitoring_models);


            next_event.put("res_inst","1cc5:1cc5:1cc5:1cc5:1cc5:1cc5:1cc5:1cc5");
            next_event.put("fragid","1cc5Fragment");
            next_event.put("timestamp",System.currentTimeMillis());

            publish("monitoring",next_event.toJSONString());

            try {
                Thread.sleep(2*1000);
            }catch (Exception e){
                e.printStackTrace();
            }
        }

    }

}
