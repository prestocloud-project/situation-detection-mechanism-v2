package gr.iccs.presto.experimentation;

import io.siddhi.core.SiddhiAppRuntime;
import io.siddhi.core.SiddhiManager;
import io.siddhi.core.event.Event;
import io.siddhi.core.query.output.callback.QueryCallback;
import io.siddhi.core.stream.input.InputHandler;
import io.siddhi.core.util.EventPrinter;


public class TestApiApp {

    public static void main(String [] args) {
        String definition = "@config(async = 'true') define stream cseEventStream (symbol string, price float, volume long);";
        String query = "@info(name = 'query1') from cseEventStream#window.time(500)  select symbol, sum(price) as price, sum(volume) as volume group by symbol insert into outputStream ;";
        SiddhiManager siddhiManager = new SiddhiManager();

        SiddhiAppRuntime executionPlanRuntime = siddhiManager.createSiddhiAppRuntime(definition + query);

        executionPlanRuntime.addCallback("query1", new QueryCallback() {
            @Override
            public void receive(long timeStamp, Event[] inEvents, Event[] removeEvents) {
                EventPrinter.print(timeStamp, inEvents, removeEvents);
                System.out.println(String.valueOf(timeStamp)+inEvents+removeEvents);
            }
        });

        InputHandler inputHandler = executionPlanRuntime.getInputHandler("cseEventStream");
        executionPlanRuntime.start();


        while (true) {
            try {

                inputHandler.send(new Object[]{"DEF", 700f, 100l});
                Thread.sleep(1000);
                inputHandler.send(new Object[]{"ABC", 700f, 100l});
                Thread.sleep(1000);
                inputHandler.send(new Object[]{"WSO2", 60.5f, 200l});
                Thread.sleep(1000);
                inputHandler.send(new Object[]{"DEF", 700f, 100l});
                Thread.sleep(1000);
                inputHandler.send(new Object[]{"ABC", 700f, 100l});
                Thread.sleep(1000);
                inputHandler.send(new Object[]{"WSO2", 60.5f, 200l});
                Thread.sleep(1000);
                inputHandler.send(new Object[]{"DEF", 700f, 100l});
                Thread.sleep(1000);
                inputHandler.send(new Object[]{"ABC", 700f, 100l});
                Thread.sleep(1000);
                inputHandler.send(new Object[]{"WSO2", 60.5f, 200l});
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        //executionPlanRuntime.shutdown();
    }

}
