package gr.iccs.presto.experimentation;


import io.siddhi.core.SiddhiAppRuntime;
import io.siddhi.core.SiddhiManager;
import io.siddhi.core.event.Event;
import io.siddhi.core.stream.input.InputHandler;
import io.siddhi.core.stream.output.StreamCallback;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import static gr.iccs.presto.Globals.EMPTY;

public class TestApp {

    public static void main(String[] args) throws InterruptedException {
            SiddhiManager siddhiManager = new SiddhiManager();

            String siddhiApp =
                    "define stream cseEventStream (symbol string, price float, volume long, timestamp long);" +
                    "@info(name = 'query1') " +
                    "from cseEventStream[700 > price] " +
                    "select * " +
                    "insert into outputStream ;";


            siddhiApp = "define stream cseEventStream (symbol string, price float, volume long, timestamp long);" +
                    "define stream joinEventStream (volume long);" +
                    "\n" +
                    "@info(name = 'query1') " +
                    "from cseEventStream#window.lengthBatch(5) unidirectional \n" +
                    "join joinEventStream#window.length(1)\n" +
                    "on cseEventStream.volume == joinEventStream.volume \n"+
                    "select cseEventStream.symbol , cseEventStream.price , cseEventStream.volume , cseEventStream.timestamp\n" +
                    "insert into outputStream ;";

            SiddhiAppRuntime siddhiAppRuntime = siddhiManager.createSiddhiAppRuntime(siddhiApp);
            siddhiAppRuntime.addCallback("outputStream", new StreamCallback() {
                public int eventCount = 0;
            	public int timeSpent = 0;
                long startTime = System.currentTimeMillis();

            	@Override
                public void receive(Event[] events) {
                    String uuid_of_receiver = UUID.randomUUID().toString().replaceAll("-",EMPTY).substring(0,10);

                    for (Event event : events) {
                    SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss.SSS");
                    Date dt = new Date();
                    String S = sdf.format(dt);
                    System.out.println(S + ", " + uuid_of_receiver + ", " + toMap(event));
                    /*
                    eventCount++;
                        timeSpent += (System.currentTimeMillis() - (Long) event.getData(3));
                    if (eventCount % 1000000 == 0) {
                            System.out.println("Throughput : " + (eventCount * 1000) / ((System.currentTimeMillis()) -
                                    startTime));
                            System.out.println("Time spent :  " + (timeSpent * 1.0 / eventCount));
                            startTime = System.currentTimeMillis();
                            eventCount = 0;
                            timeSpent = 0;
                        }
                    */
                    }

            }
            });


            InputHandler cseHandler = siddhiAppRuntime.getInputHandler("cseEventStream");
            InputHandler joinHandler = siddhiAppRuntime.getInputHandler("joinEventStream");

        siddhiAppRuntime.start();
            while (true) {
                cseHandler.send(new Object[]{"WSO2", 55.6f, new Long(100), System.currentTimeMillis()});
                joinHandler.send(new Object[]{new Long(100)});
                Thread.sleep(1000);
                cseHandler.send(new Object[]{"IBM", 75.6f, new Long(100), System.currentTimeMillis()});
                joinHandler.send(new Object[]{new Long(100)});
                Thread.sleep(1000);
                cseHandler.send(new Object[]{"WSO2", 100f, new Long(80), System.currentTimeMillis()});
                joinHandler.send(new Object[]{new Long(80)});
                Thread.sleep(1000);
                cseHandler.send(new Object[]{"IBM", 75.6f, new Long(100), System.currentTimeMillis()});
                joinHandler.send(new Object[]{new Long(100)});
                Thread.sleep(1000);
                cseHandler.send(new Object[]{"WSO2", 55.6f, new Long(100), System.currentTimeMillis()});
                joinHandler.send(new Object[]{new Long(100)});
                Thread.sleep(1000);
                cseHandler.send(new Object[]{"IBM", 75.6f, new Long(100), System.currentTimeMillis()});
                joinHandler.send(new Object[]{new Long(100)});
                Thread.sleep(1000);
                cseHandler.send(new Object[]{"WSO2", 100f, new Long(80), System.currentTimeMillis()});
                joinHandler.send(new Object[]{new Long(80)});
                Thread.sleep(1000);
                cseHandler.send(new Object[]{"IBM", 75.6f, new Long(100), System.currentTimeMillis()});
                joinHandler.send(new Object[]{new Long(100)});
            }

        }
}
