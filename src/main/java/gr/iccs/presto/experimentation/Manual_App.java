package gr.iccs.presto.experimentation;

import io.siddhi.core.SiddhiAppRuntime;
import io.siddhi.core.SiddhiManager;
import io.siddhi.core.event.Event;
import io.siddhi.core.stream.input.InputHandler;
import io.siddhi.core.stream.output.StreamCallback;
import org.apache.commons.io.IOUtils;

import java.io.InputStream;
import java.io.StringWriter;

import static gr.iccs.presto.Globals.EMPTY;
import static gr.iccs.presto.Globals.local_monitoring_data_stream_name_prefix;

public class Manual_App {

    public static void main(String[] args) throws InterruptedException {
        String siddhiApp = EMPTY;

        try {
            ClassLoader loader = Thread.currentThread().getContextClassLoader();

            InputStream appStream = loader.getResourceAsStream("RULES.siddhi");
            System.out.println("Created inputstream from : RULES.siddhi");
            StringWriter wr = new StringWriter();
            IOUtils.copy(appStream, wr, "UTF-8");
            System.out.println("copy ok");
            siddhiApp = wr.toString();
            System.out.println("LOADED RULES.siddhi .");

        } catch (Throwable t) {
            System.out.println("FAILED TO LOAD RULES.siddhi !");
            t.printStackTrace();
        }

        SiddhiManager siddhiManager = new SiddhiManager();

        SiddhiAppRuntime siddhiAppRuntime = siddhiManager.createSiddhiAppRuntime(siddhiApp);
        siddhiAppRuntime.addCallback("outputStream", new StreamCallback() {
            public int eventCount = 0;
            public int timeSpent = 0;
            long startTime = System.currentTimeMillis();

            @Override
            public void receive(Event[] events) {
                for (Event event : events) {
                    System.out.println("outputStream : " + event.toString());
                    /*
                     * eventCount++; timeSpent += (System.currentTimeMillis() - (Long)
                     * event.getData(3)); if (eventCount % 1000000 == 0) { System.out.println(
                     * "Throughput : " + (eventCount * 1000) / ((System.currentTimeMillis()) -
                     * startTime)); System.out.println("Time spent :  " + (timeSpent * 1.0 /
                     * eventCount)); startTime = System.currentTimeMillis(); eventCount = 0;
                     * timeSpent = 0; }
                     */
                }
            }
        });
/*		siddhiAppRuntime.addCallback("msgStream", new StreamCallback() {
			@Override
			public void receive(Event[] events) {
				for (Event event : events) {
					// System.out.println("msgStream : " + event.toString());
					long ts = event.getTimestamp();
					String edate = new SimpleDateFormat("HH:mm:ss").format(new Date(ts));
					String msg = event.getData(0).toString();
					System.out.println(edate + " : " + msg);
				}
			}
		});*/

        InputHandler inputHandler =
                siddhiAppRuntime.getInputHandler(local_monitoring_data_stream_name_prefix);

        siddhiAppRuntime.start();

        while (true) {
			/*Object [] object_to_send = new Object[]{
					"disk_available",new Double(50.0),"res_inst","1cc5:1cc5:1cc5:1cc5:1cc5:1cc5:1cc5:1cc5","net_sent",new Double(50.0),"cpu_user",new Double(50.0),"cpu_io_wait",new Double(50.0),"fragid","1cc5Fragment","cpu_sys",new Double(50.0),"mem_perc",new Double(50.0),"packets_sent",new Double(50.0),"packets_received",new Double(50.0),"mem_free",new Double(21.0),"net_rec",new Double(50.0),"cpu_perc",new Double(50.0),"timestamp",System.currentTimeMillis()/1000L
			};
			*/




            //Object [] object_to_send = new Object[]{50.0,50.0,50.0,50.0,50.0, ThreadLocalRandom.current().nextDouble(0,20),50.0,50.0,50.0,50.0,50.0,50.0,50.0,System.currentTimeMillis()/1000L};
            //inputHandler.send(object_to_send);


            Thread.sleep(1000);
        }
        /*
         * while (true) { inputHandler.send(new Object[] { "WSO2", 55.6f, 100,
         * System.currentTimeMillis() }); inputHandler.send(new Object[] { "IBM", 75.6f,
         * 100, System.currentTimeMillis() }); inputHandler.send(new Object[] { "WSO2",
         * 100f, 80, System.currentTimeMillis() }); inputHandler.send(new Object[] {
         * "IBM", 75.6f, 100, System.currentTimeMillis() }); inputHandler.send(new
         * Object[] { "WSO2", 55.6f, 100, System.currentTimeMillis() });
         * inputHandler.send(new Object[] { "IBM", 75.6f, 100,
         * System.currentTimeMillis() }); inputHandler.send(new Object[] { "WSO2", 100f,
         * 80, System.currentTimeMillis() }); inputHandler.send(new Object[] { "IBM",
         * 75.6f, 100, System.currentTimeMillis() }); }
         */

    }
}
