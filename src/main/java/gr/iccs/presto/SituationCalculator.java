package gr.iccs.presto;

import gr.iccs.presto.utility_classes.MonitoringMetric;
import gr.iccs.presto.utility_classes.Operand;
import gr.iccs.presto.utility_classes.SubRule;

import java.util.ArrayList;

import static gr.iccs.presto.Globals.monitoring_metrics;
import static gr.iccs.presto.Globals.number_of_severity_zones;

public class SituationCalculator {

    public static Double[][][] severity_zone_lookup_table = new Double[][][]{

            //There are as many rows as metrics, and as many columns as the number of zones
            // 2 dimensions - metrics e.g cpu_perc and mem_perc
            {
                    {0.0, 100.0},
                    {0.0, 79.78893470734675, 141.4213562373095},
                    {0.0, 65.14511096846039, 92.13008997110197, 141.4213562373095},
                    {0.0, 56.41967557128023, 79.79569380390589, 97.72361306924725, 141.4213562373095},
                    {0.0, 50.452745019242414, 71.36023570205009, 87.40194176023674, 101.05152659573115, 141.4213562373095},
                    {0.0, 46.05848150306034, 65.14607788382087, 79.78478257388772, 92.13118729098893, 103.9219928244817, 141.4213562373095},
                    {0.0, 42.65145437046887, 60.31682498854269, 73.86745020783829, 85.29556467181293, 95.36390590819694, 106.31012548640982, 141.4213562373095},
                    {0.0, 39.89200636090541, 56.422109121241405, 69.10266249939119, 79.78513588048064, 89.20065960903113, 97.71816225445032, 108.32036513498983, 141.4213562373095},
            },
            //3 dimensions
            {
                    {0.0, 100.0},
                    {0.0, 98.47168833746436, 173.20508075688775},
                    {0.0, 86.02172867628812, 109.63117570025054, 173.20508075688775},
                    {0.0, 78.15795936741401, 98.47333300947368, 115.95529254027433, 173.20508075688775},
                    {0.0, 72.55860760836637, 91.41908163324635, 104.9968976421018, 120.1774943775918, 173.20508075688775},
                    {0.0, 68.28336359380478, 86.02797443701346, 98.47552276062618, 109.62533203279733, 123.25584924000017, 173.20508075688775},
                    {0.0, 64.86137173739243, 81.72226856376865, 93.54484101316066, 103.09753862943671, 113.15529472708765, 125.65009597620588, 173.20508075688775},
                    {0.0, 62.037705525386556, 78.1654013983789, 89.47117990550068, 98.47516509108189, 106.69531285646276, 115.95000540454791, 127.56463798619366, 173.20508075688775}
            },
            //4 dimensions
            {
                    {0.0, 100.0},
                    {0.0, 113.92021620764538, 200.0},
                    {0.0, 101.96648779113343, 125.44395570540819, 200.0},
                    {0.0, 94.88801294618852, 113.9225316028577, 131.63772644461213, 200.0},
                    {0.0, 89.7310177177996, 106.92670067502061, 120.77516715005466, 135.71692855318668, 200.0},
                    {0.0, 85.74414799057679, 101.97129233665132, 113.91881608421996, 125.44703099613776, 138.71253324191497, 200.0},
                    {0.0, 82.49592653637322, 98.10723705544206, 108.96237299161082, 118.81527118693096, 128.91919200487507, 141.05480930699977, 200.0},
                    {0.0, 79.79078690805056, 94.89010226945166, 105.11338047730105, 113.92552911260448, 122.51532036757126, 131.64473854406552, 142.97484439217789, 200.0}
            },
            //5 dimensions
            {
                    {0.0, 100.0},
                    {0.0, 127.80914264781549, 223.60679774997897},
                    {0.0, 115.84500890540794, 139.13956468183437, 223.60679774997897},
                    {0.0, 108.87351426796911, 127.80394878848006, 145.2739346862758, 223.60679774997897},
                    {0.0, 104.00249249917628, 120.83220073067214, 134.55345568992988, 149.39569614368276, 223.60679774997897},
                    {0.0, 100.2607928020756, 115.85135354255814, 127.8092513729976, 139.1401307455232, 152.44739526382924, 223.60679774997897},
                    {0.0, 97.22559149727184, 111.99602416034847, 122.87404039696159, 132.62292081947453, 142.55840924551546, 154.83987545148312, 223.60679774997897},
                    {0.0, 94.65407055193805, 108.86697013945789, 119.00669209793378, 127.80758196968684, 136.25612000405872, 145.27007028399913, 156.78954773115498, 223.60679774997897}
            },
            //6 dimensions
            {
                    {0.0, 100.0},
                    {0.0, 140.24758510296243, 244.94897427831785},
                    {0.0, 128.44388603780632, 151.51980309115902, 244.94897427831785},
                    {0.0, 121.5217371494617, 140.2493696728169, 157.675573853199, 244.94897427831785},
                    {0.0, 116.68753825021057, 133.37793049290173, 146.93644633742267, 161.80232924079078, 244.94897427831785},
                    {0.0, 113.01732861415996, 128.4510418403914, 140.25399639188174, 151.5216860451028, 164.8551997982751, 244.94897427831785},
                    {0.0, 110.05854730482795, 124.6285882492997, 135.38860492146915, 145.01960188035753, 154.95470031059523, 167.24652271869502, 244.94897427831785},
                    {0.0, 107.5848169005438, 121.5208302153028, 131.56849651466868, 140.2483396160075, 148.63009527240024, 157.6715174628193, 169.20682214572793, 244.94897427831785}
            }
    };
    public static Double [] get_severity_zone_boundaries(int number_of_metrics, int number_of_severity_zones){

        if (number_of_metrics ==1 && number_of_severity_zones >=1){
            Double [] severity_zone_boundaries = new Double[number_of_severity_zones+1];

            severity_zone_boundaries[0]=0.0;
            for (int i=1; i<=number_of_severity_zones;i++){
                severity_zone_boundaries[i] = (1.0*i)/number_of_severity_zones;
            }
            return  severity_zone_boundaries;
        }
        else if (number_of_metrics>=2 && number_of_metrics<=6 && number_of_severity_zones>=1 && number_of_severity_zones<=8) {
            Double [] severity_zone_boundaries = new Double[severity_zone_lookup_table[number_of_metrics - 2][number_of_severity_zones - 1].length];
            System.arraycopy(severity_zone_lookup_table[number_of_metrics - 2][number_of_severity_zones - 1],0,severity_zone_boundaries,0,severity_zone_lookup_table[number_of_metrics - 2][number_of_severity_zones - 1].length);
            for (int i=0; i< severity_zone_boundaries.length;i++){
                severity_zone_boundaries[i]=severity_zone_boundaries[i]/100.0; //Normalization
            }
            return severity_zone_boundaries;
        }else{
            try{
                throw new Exception("Request out of range of lookup table");
            }catch (Exception e){
                e.printStackTrace();
                //TODO perhaps set the calculating function to calculate the Radii in a separate thread
            }
            return null;
        }
    }

    /**
     * The correct functionality of this method depends on the correct estimation of the structure which is to be received from Siddhi.
     * @param values_violating_rule The values violating the Scalability rule, as they come from Siddhi
     * @param sub_rules The sub rules which are involved in the Scalability rule which was triggered
     * @return The severity zone which is assigned to the situation
     */
    public static int get_severity_zone(Object[] values_violating_rule, ArrayList<SubRule> sub_rules){

        int severity_zone=0,number_of_offending_metrics  = values_violating_rule.length-1; //-1 is subtracted because the "values_violating_value" Object array also includes the extraneous res_inst field as its last element.
        double severity_value, sum = 0.0;
        Double [] normalized_metric_values = new Double[values_violating_rule.length-1];

        for (int i=0;i<values_violating_rule.length-1;i++){//TODO assumption that res_inst will always be last
            if (sub_rules.get(i).getOperand().equals(Operand.GREATER) || sub_rules.get(i).getOperand().equals(Operand.GREATEROREQUAL)){
                double metric_maximum = get_metric_maximum(sub_rules.get(i).getMetric_name());
                if (!Double.isNaN(metric_maximum)) {
                    normalized_metric_values[i] = normalize_metric_greater_than_rule((Double) values_violating_rule[i], metric_maximum, sub_rules.get(i).getThreshold());
                }else{
                    return 1;//do a mild adaptation in the case that not enough data is available.
                }
            }else{
                double metric_maximum  = get_metric_maximum(sub_rules.get(i).getMetric_name());
                if (!Double.isNaN(metric_maximum)) {
                    normalized_metric_values[i]= normalize_metric_less_than_rule((Double)values_violating_rule[i],metric_maximum,sub_rules.get(i).getThreshold());
                }else{
                    return 1;//do a mild adaptation in the case that not enough data is available.
                }
            }
        }


        for (int i=0;i<normalized_metric_values.length;i++){
            sum = sum + normalized_metric_values[i]*normalized_metric_values[i];
        }

        severity_value =  Math.sqrt(sum);
        Double [] severity_zone_boundaries = get_severity_zone_boundaries(number_of_offending_metrics, number_of_severity_zones);

        for (int i=0;i<severity_zone_boundaries.length-1;i++){ //-1 subtracted as the list also contains the supposed maximum value. Supposed because in the case of unbounded values there is the possibility that severity values can become larger than this maximum value which is calculated based on the information gathered.
            if (severity_value>severity_zone_boundaries[i]){
                severity_zone = i+1;
            }else{
                break;
            }
        }
        return severity_zone;

    }


    public static double normalize_metric_greater_than_rule(double initial_observation, double variable_maximum, double threshold){
        return (Math.abs(initial_observation-threshold))/(Math.abs(variable_maximum-threshold));
    }

    public static double normalize_metric_less_than_rule(double initial_observation, double variable_minimum, double threshold){
        return (Math.abs(initial_observation-threshold))/(Math.abs(threshold - variable_minimum));
    }

    public static void set_metric_maximum(String metric_name, double maximum_value){
            MonitoringMetric former_monitoring_metric = monitoring_metrics.remove(metric_name);
            monitoring_metrics.put(metric_name,new MonitoringMetric(metric_name,String.valueOf(former_monitoring_metric.getCollection_mode()),maximum_value,former_monitoring_metric.getLower_bound()));
    }

    public static void set_metric_minimum(String metric_name, double minimum_value){
            MonitoringMetric former_monitoring_metric = monitoring_metrics.remove(metric_name);
            monitoring_metrics.put(metric_name,new MonitoringMetric(metric_name,String.valueOf(former_monitoring_metric.getCollection_mode()),former_monitoring_metric.getUpper_bound(),minimum_value));
    }

    public static double get_metric_maximum(String metric_name){
            return monitoring_metrics.get(metric_name).getUpper_bound();
    }

    public static double get_metric_minimum(String metric_name){
            return monitoring_metrics.get(
                    metric_name).getLower_bound();
    }
}
