package gr.iccs.presto;

import gr.iccs.presto.utility_classes.MonitoringMetric;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import static gr.iccs.presto.Globals.*;
import static gr.iccs.presto.messaging.SSLUtilities.backUpDirName;
import static gr.iccs.presto.utility_classes.Fragment.getFragments;

public class SiddhiStream {
    private String name;
    private String definition;
    private ArrayList<String> metrics = new ArrayList<>(); //the metrics which exist in a Siddhi stream

    public SiddhiStream(String name, String definition){
        this.name = name;
        this.definition = definition;
    }

    public SiddhiStream(String name, String definition, ArrayList<String> metrics){
        this.name = name;
        this.definition = definition;
        this.metrics = metrics;
    }

    /**
     * In order for the parsing of JSON input to work properly, the parameters should appear in the correct order, and the parameter types should be able to accommodate the input - e.g having only int is not enough when long values can be sent.
     * @return The definition of the input stream
     */
    public static HashMap<String,SiddhiStream> get_input_stream_definitions() {
        HashMap<String,SiddhiStream> input_stream_definitions = new HashMap<>();

        getFragments().values().forEach(fragment -> {
            input_stream_definitions.putAll(fragment.getStream_definitions());
        });

        String ssl_settings;
        if(enforce_ssl){
            File truststore_file = new File(backUpDirName+"truststore.jks");
            ssl_settings = "tls.enabled = \'true\',"+
                    "tls.truststore.Type = \'"+ truststore_type +"\',"+
                    "tls.truststore.path = \'"+truststore_file.getAbsolutePath()+"',"+
                    "tls.truststore.password = \'"+ truststore_password +"\',"+
                    "tls.version = \'TLSv"+supported_tls_version+"\',";
        }else{
            ssl_settings = EMPTY;
        }

        String monitoring_topic = EMPTY;
        monitoring_topic = mca_fragment_data_topic.replaceAll(".(#)?$",EMPTY);
        monitoring_topic = monitoring_topic +".#";

        //The stream should always exist, and there exists only one such stream per platform
        String context_output_stream_definition = "@sink(type = 'rabbitmq', " +
                "uri = \'" + broker_uri + "\', " +
                "exchange.name = \'" + exchange_name + "\', " +
                "delivery.mode = \'2\', "+ //This means that persistence is required from the RabbitMQ broker
                "exchange.type = \'topic\', " +
                "expiration = \'86400000\', " +
                ssl_settings +
                "heartbeat = \'300\', "+
                "routing.key = \'"+monitoring_topic+"\', "+
                "@map(type='json', enclosing.element=\"$\", \n" +
                "@attributes(\n" +
                "avg_response_time_ms= \"avg_response_time_ms\",\n" +
                "fragid= \"fragid\",\n" +
                "res_inst= \"res_inst\")))\n"+
                "define stream "+context_output_stream_name+" (avg_response_time double, fragid string, res_inst string);\n"; //the json field expected by the MCA is response_time, so the attribute name is harmonized with this expectation rather than being avg_response_time

        SiddhiStream context_stream = new SiddhiStream(context_output_stream_name,context_output_stream_definition);

        input_stream_definitions.put(context_stream.getName(),context_stream);
        return input_stream_definitions;
    }
    /**
     * Returns a definition for a new stream containing the upper and lower boundaries of a monitoring metric
     * @param metric_name The name of the metric
     * @return A string containing the Siddhi statement which defines the new stream
     */
    public static SiddhiStream create_bounds_stream(String metric_name) {

        String bounds_stream_name = "unbounded_metric_"+metric_name;
        String bounds_stream_definition = "@config(async = 'true') define stream "+bounds_stream_name+
                " (lower_bound double, upper_bound double);\n";

        SiddhiStream bounds_stream = new SiddhiStream(bounds_stream_name,bounds_stream_definition);

        return bounds_stream;
    }



    /**
     * Returns the definitions of all monitoring metrics which are not bounded. This method is required to have access to a central
     * repository of unbounded monitoring metrics, to avoid retrieving duplicate definitions of unbounded stream calculations
     * @return A HashSet containing the Siddhi streams which are going to hold the calculated boundaries of the metric
     */
    public static HashMap<String, SiddhiStream> get_unbounded_metric_definitions(){
        final HashMap<String, SiddhiStream> unbounded_metric_definitions = new HashMap();

        for (MonitoringMetric monitoring_metric: unbounded_monitoring_metrics.values()){
            SiddhiStream bound_stream = create_bounds_stream(monitoring_metric.getName());
            unbounded_metric_definitions.put(bound_stream.getName(),bound_stream);
        }
        return unbounded_metric_definitions;
    }


    private static HashSet<SiddhiStream> get_unbounded_stream_definitions(HashMap<String, MonitoringMetric> monitoring_metrics){
        final HashSet<SiddhiStream> unbounded_metric_definitions = new HashSet<>();

        monitoring_metrics.values().forEach( monitoring_metric -> {
            if (monitoring_metric.is_upwards_unbounded() || monitoring_metric.is_downwards_unbounded()) {
                unbounded_metric_definitions.add(create_bounds_stream(monitoring_metric.getName()));
            }
        });

        return unbounded_metric_definitions;

        /*
        for (MonitoringMetric monitoring_metric : monitoring_metrics) {
            if (monitoring_metric.is_upwards_unbounded() || monitoring_metric.is_downwards_unbounded()) {
                unbounded_metric_definitions = unbounded_metric_definitions + create_bounds_stream_definition(monitoring_metric.getMetric_name());
            }
        }
        */
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDefinition() {
        return definition;
    }

    public void setDefinition(String definition) {
        this.definition = definition;
    }

    public ArrayList<String> getMetrics() {
        return metrics;
    }

    public void setMetrics(ArrayList<String> metrics) {
        this.metrics = metrics;
    }

    public void add_metric(String metric){
        metrics.add(metric);
    }
}
