package gr.iccs.presto.messaging;

import producer.AmqpProducer;

import static gr.iccs.presto.Globals.*;

public class AMQP_Publisher {


    private AmqpProducer producer​;
    //private static char[] trustPassphrase = "Pr3570Cloud".toCharArray();

    public void publish(String message) {
        producer​ = new AmqpProducer(broker_ip_address,false);
        if (enforce_ssl) {
            //producer​.useSSL(keyStorePath, keyPassphrase, trustStorePath, trustPassphrase);
            producer​.useSSL(vault_address,vault_token);
        }
        try {
            producer​.publish(message, situations_detected_topic);
        }catch (Exception e){//(IOException e){
                System.out.println(e.getMessage());
                waitXminAndRetry(1,message);
        }
    }


    public void publish(String topic, String message) {
        producer​ = new AmqpProducer(broker_ip_address,false);
        if (enforce_ssl) {
            //producer​.useSSL(keyStorePath, keyPassphrase, trustStorePath, trustPassphrase);
            producer​.useSSL(vault_address,vault_token);
        }
        try {
            producer​.publish(message, topic);
        }catch(Exception e){//IOException e){
            System.out.println(e.getMessage());
            waitXminAndRetry(1,topic,message);
        }
    }

    private void waitXminAndRetry(int minutes,String topic,String message){
        try{
            Thread.sleep(minutes*60*1000);
        }catch (InterruptedException i){
            publish(topic, message);
        }
    }

    private void waitXminAndRetry(int minutes,String message){
        try{
            Thread.sleep(minutes*60*1000);
        }catch (InterruptedException i){
            publish(message);
        }
    }

}