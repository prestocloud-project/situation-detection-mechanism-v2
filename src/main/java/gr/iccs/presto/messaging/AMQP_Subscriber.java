package gr.iccs.presto.messaging;

import consumer.AmqpConsumer;
import exceptions.AlreadySubscribedException;
import exceptions.NoHostException;
import exceptions.SubscribingFailedException;
import gr.iccs.presto.ScalabilityRule;
import gr.iccs.presto.TransformationsEngine;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClients;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.logging.Logger;

import static gr.iccs.presto.App.*;
import static gr.iccs.presto.Globals.*;
import static gr.iccs.presto.utility_classes.Fragment.getFragments;
import static java.util.logging.Level.INFO;
import static java.util.logging.Level.WARNING;

public class AMQP_Subscriber {


    private java.util.function.BiConsumer<String, String> message_handling_function = this::digestScalabilityRuleDefinition;

    //consumer​.useSSL(keyStorePath, keyPassphrase, trustStorePath, trustPassphrase);
    public void subscribe() {
        AmqpConsumer scalability_rule_definition_consumer = new AmqpConsumer(broker_ip_address,false,
                message_handling_function);
        AmqpConsumer undeployment_message_consumer = new AmqpConsumer(broker_ip_address,false, message_handling_function);
        if(enforce_ssl) {
            scalability_rule_definition_consumer.useSSL(vault_address,vault_token);
            undeployment_message_consumer.useSSL(vault_address,vault_token);
        }
        if(broker_username!=null && broker_password!=null) {
            scalability_rule_definition_consumer.setUsernameAndPassword(broker_username, broker_password);
            undeployment_message_consumer.setUsernameAndPassword(broker_username,broker_password);
        }else{
            Logger.getAnonymousLogger().log(WARNING,"Using default credentials for broker subscription");
            scalability_rule_definition_consumer.setUsernameAndPassword("nissatech", "9r3570cl0ud!");
            undeployment_message_consumer.setUsernameAndPassword("nissatech", "9r3570cl0ud!");
        }
        //TODO Create a similar consumer if needed to retrieve information from monitoring data
        try {
            scalability_rule_definition_consumer.subscribe(scalability_rules_manipulation_topic);
            undeployment_message_consumer.subscribe(undeployment_topic);
        } catch (AlreadySubscribedException | IOException | NoHostException e){
            System.out.println(e.getMessage());
        } catch (SubscribingFailedException s){
            subscribe();
            //waitXminAndRetry(3);
        }
    }
    //digestScalabilityRuleDefinition is the method that gets executed on every received message
    private void digestScalabilityRuleDefinition(String message, String topic) {
        //processing_function(message,topic);
        //System.out.println("Uncomment official event after testing");
        //TestBrokerSubscription.stop_program = true;

        JSONParser parser = new JSONParser();
        JSONObject master_json_object;




        if (topic.equals(undeployment_topic)) {
            try {
                master_json_object = (JSONObject) parser.parse(message);
                String undeployed_graph_instance_hex_id = (String) master_json_object.get("graphInstanceHexID");
                remove_graphHexInstanceID(undeployed_graph_instance_hex_id);

            }catch (Exception e) {
                Logger.getAnonymousLogger().log(WARNING,"Could not parse json message \n"+message+" as it was not in the specified format");
                e.printStackTrace();
            }

        } else if (topic.equals(scalability_rules_manipulation_topic)) {
            try {
                master_json_object = (JSONObject) parser.parse(message);
                Object creation = master_json_object.get("creation");
                Object hex_id = master_json_object.get("policyHexID");
                String graphHexInstanceID = (String) master_json_object.get("graphInstanceHexID");

                if (hex_id != null && !hex_id.toString().equals(EMPTY) && !hex_id.toString().equalsIgnoreCase("null")) {
                    if (Boolean.parseBoolean(creation.toString())) {
                        ScalabilityRule new_rule = TransformationsEngine.process_ui_rule(message);
                        HttpClient httpclient = HttpClients.createDefault();
                        HttpPost httppost = new HttpPost(new_rule.getCallback_url() + "/SUCCESS");
                        HttpResponse response = httpclient.execute(httppost);
                    /*HttpEntity entity = response.getEntity();
                    BufferedReader br = new BufferedReader(new InputStreamReader(entity.getContent()));
                    String next_line = br.readLine();
                    while (next_line!=null){
                        System.out.println(next_line);
                        next_line = br.readLine();
                    }*/
                        synchronized (scalability_rules) {
                            //scalability_rules.clear();
                            scalability_rules.put(hex_id.toString(), new_rule);
                            synchronized (NEW_SCALABILITY_RULE_OPERATION_DETECTED) {
                                NEW_SCALABILITY_RULE_OPERATION_DETECTED.setTrue();
                                NEW_SCALABILITY_RULE_OPERATION_DETECTED.notifyAll();
                            }
                        }
                    } else {
                        if (scalability_rules.get(hex_id)!=null) {
                            remove_rule(scalability_rules.get(hex_id));
                        }
                        return;
                    }
                } else {
                    remove_graphHexInstanceID(graphHexInstanceID);
                }

            } catch(Exception e){
                e.printStackTrace();
                try {
                    master_json_object = (JSONObject) parser.parse(message);
                    String callback_url = master_json_object.get("callbackURL").toString();
                    HttpClient httpclient = HttpClients.createDefault();
                    HttpPost httppost = new HttpPost(callback_url + "/ERROR");
                    HttpResponse response = httpclient.execute(httppost);
                    HttpEntity entity = response.getEntity();
                    BufferedReader br = new BufferedReader(new InputStreamReader(entity.getContent()));
                    String next_line = br.readLine();
                    while (next_line != null) {
                        Logger.getAnonymousLogger().log(INFO, next_line);
                        next_line = br.readLine();
                    }
                } catch (Exception f) {
                    f.printStackTrace();
                }
            }
        }
    }

    private void remove_all_state_for_rule(ScalabilityRule rule){

        synchronized (previous_adaptations) {
            if (previous_adaptations.containsKey(rule.getHex_id())) {
                previous_adaptations.remove(rule.getHex_id());
            }
        }

        synchronized (queries){
            if (queries.containsKey(rule.getAggregator_siddhi_query().getOutput_stream_name())) {
                queries.remove(rule.getAggregator_siddhi_query().getOutput_stream_name());
            }
            if (queries.containsKey(rule.getRule_siddhi_query().getOutput_stream_name())){
                queries.remove(rule.getRule_siddhi_query().getOutput_stream_name());
            }

            //Commented out because the knowledge about custom metrics might be needed in a future rule and thus it should be kept
            // unbounded_metrics_queries.remove(rule.getAggregator_siddhi_query().getOutput_stream_name());
        }
        synchronized (stream_definitions){
            if(stream_definitions.containsKey(rule.getRule_output_stream().getName())){
                stream_definitions.remove(rule.getRule_output_stream().getName());
            }
            if(stream_definitions.containsKey(rule.get_siddhi_query_output_stream().getName())) {
                stream_definitions.remove(rule.get_siddhi_query_output_stream().getName());
            }
        }
    }

    private void remove_rule (ScalabilityRule rule) {
        remove_all_state_for_rule(rule);

        synchronized (scalability_rules) {
            scalability_rules.remove(rule.getHex_id());
            synchronized (NEW_SCALABILITY_RULE_OPERATION_DETECTED) {
                NEW_SCALABILITY_RULE_OPERATION_DETECTED.setTrue();
                NEW_SCALABILITY_RULE_OPERATION_DETECTED.notifyAll();
            }
        }
    }


    private void remove_graphHexInstanceID(String graphHexInstanceID) {
        ArrayList<String> rule_ids_to_remove = new ArrayList<>();
        synchronized (scalability_rules) {
            scalability_rules.values().forEach(rule -> {
                if (rule.getGraphInstanceHexID().equals(graphHexInstanceID))
                    rule_ids_to_remove.add(rule.getHex_id());
                remove_all_state_for_rule(rule);
            });
            rule_ids_to_remove.forEach(x -> {
                scalability_rules.remove(x);
            });
        }

        synchronized (getFragments()){
            ArrayList<String> fragments_to_remove = new ArrayList<>();
            getFragments().values().forEach(fragment -> {

                //Add the fragment to the list of fragments for deletion, if only one scalability rule is contained in the scalability rules associated with the fragment
                HashSet<String> rules_set = fragment.getScalability_rules_associated_with_fragment();
                if (rule_ids_to_remove.containsAll(rules_set)){
                    fragments_to_remove.add(fragment.getFragment_name());
                }
            });
            fragments_to_remove.forEach(fragment -> {
                getFragments().remove(fragment);
            });
        }

        synchronized (NEW_SCALABILITY_RULE_OPERATION_DETECTED) {
            NEW_SCALABILITY_RULE_OPERATION_DETECTED.setTrue();
            NEW_SCALABILITY_RULE_OPERATION_DETECTED.notifyAll();
        }
    }

    private void  processing_function(String message, String topic){
        System.out.println("A message was received in topic "+topic);
        System.out.println("MESSAGE "+message);
    }

    private void waitXminAndRetry(int minutes){
        try{
            Thread.sleep(minutes*60*1000);
        }catch (InterruptedException i){
            subscribe();
        }
    }
}
