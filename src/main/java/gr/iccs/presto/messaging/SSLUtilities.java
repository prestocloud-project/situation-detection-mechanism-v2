package gr.iccs.presto.messaging;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import sun.security.util.DerInputStream;
import sun.security.util.DerValue;

import java.io.*;
import java.math.BigInteger;
import java.nio.charset.Charset;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.spec.RSAPrivateCrtKeySpec;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import static gr.iccs.presto.App.TRUSTSTORE_AVAILABLE;
import static gr.iccs.presto.Globals.*;

public class SSLUtilities {

    private static String alias = "client.nissatech.node";
    public static String backUpDirName = "backup/";
    private static String role = "nissatech-dot-node";

    public static void prepareSSL() throws GeneralSecurityException, IOException, JSONException {

        KeyStore trustStore = KeyStore.getInstance(truststore_type.toLowerCase());

        boolean trustStoreLoad = false;
        while (!trustStoreLoad) {
            try {
                File truststore_file = new File (backUpDirName+"truststore.jks");
                trustStore.load(new FileInputStream(truststore_file.getAbsolutePath()), truststore_password.toCharArray());
                Date certExpiryDate = ((X509Certificate) trustStore.getCertificate(alias)).getNotAfter();
                Date today = new Date();
                long dateDiff = certExpiryDate.getTime() - today.getTime();
                long expiresIn = dateDiff / (24 * 60 * 60 /*30*/ * 1000);
                if (expiresIn < /*1*/7)
                    throw new CertificateExpiredException("[x] Certificate expired or expires in less than " + "7 days"/*"30 s"*/ + ", so it will be renewed");
                trustStoreLoad = true;
            } catch (FileNotFoundException | CertificateExpiredException e) {
                if (e instanceof FileNotFoundException) {
                    Logger.getAnonymousLogger().log(Level.INFO,"[x] There is no truststore, so new one will be initialized");
                } else {
                    Logger.getAnonymousLogger().log(Level.INFO,e.getMessage());
                }
                updateTrustStore();
            }
        }
        synchronized (TRUSTSTORE_AVAILABLE) {
            TRUSTSTORE_AVAILABLE.setTrue();
            TRUSTSTORE_AVAILABLE.notifyAll();
        }
    }

    private static void updateTrustStore() throws IOException, JSONException, GeneralSecurityException {
        KeyStore trustStore = KeyStore.getInstance(truststore_type);
        trustStore.load(null, null);

        JSONObject data = getVaultCerts();

        List<Certificate> certs = new ArrayList<>();
        CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509");

        String certificate = data.get("certificate").toString();
        certs.add(certificateFactory.generateCertificate(new ByteArrayInputStream(certificate.getBytes(Charset.forName("UTF-8")))));


        JSONArray chain = data.getJSONArray("ca_chain");
        for (int i = 0; i < chain.length(); i++) {
            Certificate ca = certificateFactory.generateCertificate(new ByteArrayInputStream(chain.getString(i).getBytes(Charset.forName("UTF-8"))));
            certs.add(ca);
            trustStore.setCertificateEntry("certificate-" + i, ca);
        }

        String key = data.get("private_key").toString().replace("\n", "")
                .replace("-----BEGIN RSA PRIVATE KEY-----", "")
                .replace("-----END RSA PRIVATE KEY-----", "");
        PrivateKey privateKeyPrivate = getPrivateKey(key);

        Certificate[] certificates = new Certificate[certs.size()];
        certificates = certs.toArray(certificates);
        trustStore.setKeyEntry(alias, privateKeyPrivate, null, certificates);

        File dir = new File(backUpDirName);
        dir.mkdir();
        File truststore_file = new File(backUpDirName+"truststore.jks");
        trustStore.store(new FileOutputStream(truststore_file.getAbsolutePath()), truststore_password.toCharArray());
//        return trustStore;
    }

    private static JSONObject getVaultCerts() throws IOException, JSONException {
        CloseableHttpClient client = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost("http://" + vault_address + ":8200/v1/pki_int/issue/" + role);

        String json = "{\"common_name\": \"" + alias + "\"," + /*"\"ttl\": \"1m\"," +*/ "\"exclude_cn_from_sans\":\"true\"}";
        StringEntity entity = new StringEntity(json);
        httpPost.setEntity(entity);
        httpPost.setHeader("X-Vault-Token", vault_token);
        httpPost.setHeader("Accept", "application/json");
        httpPost.setHeader("Content-type", "application/json");

        CloseableHttpResponse response = client.execute(httpPost);

        HttpEntity responseEntity = response.getEntity();
        String responseString = EntityUtils.toString(responseEntity, "UTF-8");

        return new JSONObject(responseString).getJSONObject("data");
    }

    private static PrivateKey getPrivateKey(String key) throws IOException, GeneralSecurityException {
        DerInputStream derReader = new DerInputStream(Base64.getDecoder().decode(key));

        DerValue[] seq = derReader.getSequence(0);

        if (seq.length < 9) {
            throw new GeneralSecurityException("Could not parse a PKCS1 private key.");
        }

        // skip version seq[0];
        BigInteger modulus = seq[1].getBigInteger();
        BigInteger publicExp = seq[2].getBigInteger();
        BigInteger privateExp = seq[3].getBigInteger();
        BigInteger prime1 = seq[4].getBigInteger();
        BigInteger prime2 = seq[5].getBigInteger();
        BigInteger exp1 = seq[6].getBigInteger();
        BigInteger exp2 = seq[7].getBigInteger();
        BigInteger crtCoef = seq[8].getBigInteger();

        RSAPrivateCrtKeySpec keySpec = new RSAPrivateCrtKeySpec(modulus, publicExp, privateExp, prime1, prime2, exp1, exp2, crtCoef);

        KeyFactory factory = KeyFactory.getInstance("RSA");

        return factory.generatePrivate(keySpec);
    }


}
