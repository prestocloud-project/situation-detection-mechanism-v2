package gr.iccs.presto.utility_classes;


import static gr.iccs.presto.Globals.unbounded_monitoring_metrics;

public class MonitoringMetric {

    private enum metric_bound_status {fully_bounded, upwards_unbounded, downwards_unbounded, fully_unbounded}
    private enum metric_collection_mode {local, aggregate}
    private boolean predicted_metric = false;
    private metric_collection_mode collection_mode;
    private double lower_bound = Double.NaN;
    private double upper_bound = Double.NaN;
    private metric_bound_status bound_status;
    private int unbounded_metric_counter = 1;
    private String name;


    public MonitoringMetric(MonitoringMetric monitoring_metric){
        this.upper_bound = monitoring_metric.upper_bound;
        this.lower_bound = monitoring_metric.lower_bound;
        this.bound_status = monitoring_metric.bound_status;
        this.unbounded_metric_counter = monitoring_metric.unbounded_metric_counter;
        this.name = monitoring_metric.name;
        this.predicted_metric = monitoring_metric.predicted_metric;
        this.collection_mode = monitoring_metric.collection_mode;
    }

    public MonitoringMetric(String name, String collection_mode, double upper_bound, double lower_bound){
        this.lower_bound = lower_bound;
        this.upper_bound = upper_bound;
        if (new Double(upper_bound).isNaN() && new Double(lower_bound).isNaN()){
            bound_status = metric_bound_status.fully_unbounded;
        }else if (new Double(upper_bound).isNaN()){
            bound_status = metric_bound_status.upwards_unbounded;
        }else if (new Double(lower_bound).isNaN()){
            bound_status = metric_bound_status.downwards_unbounded;
        }else{
            bound_status = metric_bound_status.fully_bounded;
        }
        this.name = name;
        this.collection_mode = metric_collection_mode.valueOf(collection_mode);
        if (!bound_status.equals(metric_bound_status.fully_bounded)){
            synchronized (unbounded_monitoring_metrics) {
                unbounded_monitoring_metrics.put(name, this);
            }
        }
    }


    public metric_collection_mode getCollection_mode() {
        return collection_mode;
    }

    public boolean is_predicted_metric() {
        return predicted_metric;
    }

    public void set_predicted_metric(boolean predicted_metric){
        this.predicted_metric = predicted_metric;
    }

    public void setCollection_mode(metric_collection_mode collection_mode) {
        this.collection_mode = collection_mode;
    }

    public int getUnbounded_metric_counter() {
        return unbounded_metric_counter;
    }

    public void increaseUnbounded_metric_counter() {
        unbounded_metric_counter++;
    }

    public boolean is_upwards_unbounded(){
        return (bound_status== metric_bound_status.upwards_unbounded || bound_status == metric_bound_status.fully_unbounded);
    }

    public boolean is_downwards_unbounded(){
        return (bound_status== metric_bound_status.downwards_unbounded || bound_status == metric_bound_status.fully_unbounded);
    }

    public boolean is_locally_collected_metric (){
        return collection_mode == metric_collection_mode.local;
    }

    public boolean is_metric_collected_in_aggregate(){
        return collection_mode == metric_collection_mode.aggregate;
    }

    public double getLower_bound() {
        return lower_bound;
    }

    public void setLower_bound(double lower_bound) {
        this.lower_bound = lower_bound;
    }

    public double getUpper_bound() {
        return upper_bound;
    }

    public void setUpper_bound(double upper_bound) {
        this.upper_bound = upper_bound;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
