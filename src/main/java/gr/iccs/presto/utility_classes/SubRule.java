package gr.iccs.presto.utility_classes;

public class SubRule {
    private String metric_name;
    private String monitored_fragment_name;
    private String monitored_fragment_id;
    private String aggregation_function;
    private double threshold;
    private Operand operand;

    public SubRule(String aggregation_function, String metric_name, Operand operand, String monitored_fragment_id, String monitored_fragment_name, double threshold){
        this.metric_name = metric_name;
        this.monitored_fragment_name = monitored_fragment_name;
        this.monitored_fragment_id = monitored_fragment_id;
        this.aggregation_function = aggregation_function;
        this.threshold = threshold;
        this.operand = operand;
    }

    public String getMetric_name() {
        return metric_name;
    }

    public void setMetric_name(String metric_name) {
        this.metric_name = metric_name;
    }

    public String getMonitored_fragment_name() {
        return monitored_fragment_name;
    }

    public void setMonitored_fragment_name(String monitored_fragment_name) {
        this.monitored_fragment_name = monitored_fragment_name;
    }

    public String getMonitored_fragment_id() {
        return monitored_fragment_id;
    }

    public void setMonitored_fragment_id(String monitored_fragment_id) {
        this.monitored_fragment_id = monitored_fragment_id;
    }

    public String getAggregation_function() {
        return aggregation_function;
    }

    public void setAggregation_function(String aggregation_function) {
        this.aggregation_function = aggregation_function;
    }

    public double getThreshold() {
        return threshold;
    }

    public void setThreshold(double threshold) {
        this.threshold = threshold;
    }

    public Operand getOperand() {
        return operand;
    }

    public void setOperand(Operand operand) {
        this.operand = operand;
    }
}
