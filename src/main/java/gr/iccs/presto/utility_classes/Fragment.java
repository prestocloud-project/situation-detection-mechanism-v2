package gr.iccs.presto.utility_classes;

import gr.iccs.presto.SiddhiQuery;
import gr.iccs.presto.SiddhiStream;

import java.io.File;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static gr.iccs.presto.Globals.*;
import static gr.iccs.presto.SiddhiQuery.create_bounds_stream_query;
import static gr.iccs.presto.messaging.SSLUtilities.backUpDirName;

public class Fragment {
    private HashSet<String> scalability_rules_associated_with_fragment = new HashSet<>(); //This contains the ids of the scalability rules which correspond to this fragment

    public static final SynchronizedBoolean can_manipulate_fragments_map = new SynchronizedBoolean(true);
    public String predicted_monitoring_data_stream_name;
    public String local_monitoring_data_stream_name;
    public String edge_local_monitoring_data_stream_name;
    public String aggregate_monitoring_data_stream_name;
    public String combined_metrics_monitoring_stream_name;
    public String fragment_context_output_stream_name;
    private HashMap<String, MonitoringMetric> monitoring_metrics = new HashMap<>();
    private HashMap<String, MonitoringMetric> predicted_monitoring_metrics = new HashMap<>();
    private HashMap<String,SiddhiQuery> monitoring_metric_queries = new HashMap<>();
    private HashMap<String,SiddhiStream> stream_definitions = new HashMap<>();
    private String fragment_name;
    private static Map<String, Fragment> fragments = new ConcurrentHashMap<>();

    public Fragment(String fragment_name, ConcurrentHashMap<String, MonitoringMetric> realtime_monitoring_metrics) {

        this.fragment_name = fragment_name;
        predicted_monitoring_data_stream_name = predicted_monitoring_data_stream_name_prefix + fragment_name;
        local_monitoring_data_stream_name = local_monitoring_data_stream_name_prefix + fragment_name;
        edge_local_monitoring_data_stream_name = edge_local_monitoring_data_stream_name_prefix + fragment_name;
        aggregate_monitoring_data_stream_name = aggregate_monitoring_data_stream_name_prefix + fragment_name;
        combined_metrics_monitoring_stream_name = combined_metrics_monitoring_stream_prefix + fragment_name;
        fragment_context_output_stream_name = context_output_stream_name + fragment_name;

        monitoring_metrics.putAll(realtime_monitoring_metrics);
        /*
        for (MonitoringMetric realtime_monitoring_metric: realtime_monitoring_metrics) {
            monitoring_metrics.put(realtime_monitoring_metric.getMetric_name(),realtime_monitoring_metric);
        }
        */
        monitoring_metrics.values().forEach(monitoring_metric -> {
            if (monitoring_metric.is_downwards_unbounded() || monitoring_metric.is_upwards_unbounded()) {
                synchronized (unbounded_monitoring_metrics) {
                    unbounded_monitoring_metrics.put(monitoring_metric.getName(), monitoring_metric);
                }
            }
        });
        /*
        for (HashMap.Entry<String,MonitoringMetric> monitoring_metric_entry : monitoring_metrics.entrySet()) {
            MonitoringMetric monitoring_metric = monitoring_metric_entry.getValue();
            if(monitoring_metric.is_downwards_unbounded() || monitoring_metric.is_upwards_unbounded()){
                unbounded_monitoring_metrics.put(monitoring_metric.getMetric_name(),monitoring_metric);
            }
        }
        */
        initialize_stream_definitions();
        initialize_queries();
        addFragment(this);
    }

    /**
     * This method initialized the join, and mobile context analyzer queries. The join query joins metrics from the local and aggregate metric streams.
     */
    private void initialize_queries() {

        String select_monitoring_metrics = EMPTY;
        for (MonitoringMetric metric : monitoring_metrics.values()) {
            String stream_name_containing_metric =
                    (metric.is_predicted_metric() ? EMPTY :
                    metric.is_locally_collected_metric() ? local_monitoring_data_stream_name :
                    metric.is_metric_collected_in_aggregate() ? aggregate_monitoring_data_stream_name :
                    EMPTY);
            if (!stream_name_containing_metric.equals(EMPTY)) {
                select_monitoring_metrics = select_monitoring_metrics +
                        stream_name_containing_metric + "." + metric.getName() + ", ";
            }
        }

        select_monitoring_metrics = select_monitoring_metrics
                + local_monitoring_data_stream_name + ".fragid, "
                + local_monitoring_data_stream_name + ".res_inst, "
                + local_monitoring_data_stream_name + ".timestamp";


        String monitoring_stream_join_query_definition =
                "from " + local_monitoring_data_stream_name + "#window.time(" + monitoring_events_frequency_msec + " milliseconds) unidirectional \n" +
                        "join " + aggregate_monitoring_data_stream_name + "#window.time(" + monitoring_events_frequency_msec + " milliseconds)\n" +
                        "on " + local_monitoring_data_stream_name + ".fragid == " + aggregate_monitoring_data_stream_name + ".fragid\n" +
                        "select " + select_monitoring_metrics + "\n" +
                        "insert into " + combined_metrics_monitoring_stream_name + ";\n";
        /*
        monitoring_stream_join_query = "from "+ local_monitoring_data_stream_name + "\n"+
                                        "select disk_available, net_sent, cpu_user, cpu_io_wait, 0.0 as linvocations_sec, 0.0 as linvocations_responsetime, throughput_rate_fps, latency_ms, cpu_sys, mem_perc, packets_sent, mem_free, packets_received, net_rec, cpu_perc, response_time_ms, fragid, res_inst, timestamp\n" +
                                        "insert into "+combined_metrics_monitoring_stream+";\n";
        */

        SiddhiQuery monitoring_stream_join_query = new SiddhiQuery(monitoring_stream_join_query_definition, combined_metrics_monitoring_stream_name); //This is added at application level:  get_unbounded_metric_queries(monitoring_metrics);

        monitoring_metric_queries.put(monitoring_stream_join_query.getOutput_stream_name(),monitoring_stream_join_query);

        String mobile_context_analyzer_query_definition;

        if (send_cloud_device_context) {
            mobile_context_analyzer_query_definition =
                    "from " + local_monitoring_data_stream_name + " #window.time(" + number_of_events_for_context * monitoring_events_frequency_msec + " milliseconds) [fragid==\'" + fragment_name + "\']\n" + "select avg(response_time_ms) as avg_response_time,fragid,res_inst\n" + "group by fragid, res_inst\n" + "insert into " + context_output_stream_name + ";\n";
        }else {
            mobile_context_analyzer_query_definition =
                    "from " + edge_local_monitoring_data_stream_name + " #window.time(" + number_of_events_for_context * monitoring_events_frequency_msec + " milliseconds) [fragid==\'" + fragment_name + "\']\n" + "select avg(response_time_ms) as avg_response_time,fragid,res_inst\n" + "group by fragid, res_inst\n" + "insert into " + context_output_stream_name + ";\n";
        }

        SiddhiQuery mobile_context_analyzer_query = new SiddhiQuery(mobile_context_analyzer_query_definition, context_output_stream_name);
        monitoring_metric_queries.put(fragment_context_output_stream_name,mobile_context_analyzer_query); //Although all context events are sent using the same context_stream, their identifiers in the monitoring_metric_queries hashmap use the fragment_context_output_stream_name which is different for each fragment, to allow multiple context streams belonging to many fragments to coexist.
    }

    /**
     * This method initializes the definitions of the input streams, the local monitoring stream containing solely metrics which are collected locally, the aggregate stream which contains metrics which are collected in aggregate, the combined monitoring stream which joins the previous two streams, and the predicted stream containing metric predictions.
     */
    private void initialize_stream_definitions() {


/*        String local_metrics_monitoring_stream = "@source(type = 'rabbitmq', uri = 'amqp://guest:guest@3.120.91.124:5672', exchange.name = 'presto.cloud', routing.key = '"+realtime_monitoring_data_topic+"', @map(type='json', enclosing.element=\"$\", \n" +
                "     @attributes(\n" +
                "     disk_available = \"disk_available\", \n" +
                "     cpu_sys = \"cpu_sys\", \n" +
                "     cpu_user = \"cpu_user\", \n" +
                "     cpu_io_wait = \"cpu_io_wait\", \n" +
                "     cpu_perc = \"cpu_perc\", \n" +
                "     mem_free = \"mem_free\", \n" +
                "     mem_perc = \"mem_perc\", \n" +
                "     net_rec = \"net_rec\", \n" +
                "     net_sent = \"net_sent\", \n" +
                "     packets_sent = \"packets_sent\", \n" +
                "     packets_received = \"packets_received\", \n" +
                "     throughput_rate_fps = \"throughput_rate_fps\", \n" +
                "     response_time_ms = \"response_time_ms\", \n" +
                "     latency_ms = \"latency_ms\", \n" +
                "     fragid= \"fragid\", \n" +
                "     res_inst = \"res_inst\", \n" +
                "     timestamp = \"timestamp\")\n" +
                "     ))\n" +
                "define stream "+ local_monitoring_data_stream_name +" (disk_available double, cpu_sys double, cpu_user double, cpu_io_wait double, cpu_perc double, mem_free double, mem_perc double, net_rec double, net_sent double, packets_sent double, packets_received double, throughput_rate_fps double, response_time_ms double, latency_ms double, fragid string, res_inst string, timestamp long);\n";*/

        // -------------- Local metrics monitoring stream calculation


        String local_monitoring_fragment_topic;
        local_monitoring_fragment_topic = realtime_monitoring_data_topic.replaceAll(".(#)?$",EMPTY);
        local_monitoring_fragment_topic = local_monitoring_fragment_topic +".*.*.*."+fragment_name+".#";


        String fragment_queue_name =broker_amqp_queue_name_prefix+System.nanoTime();
        String ssl_settings;
        if(enforce_ssl){
            File truststore_file = new File(backUpDirName+"truststore.jks");
            ssl_settings = "tls.enabled = \'true\',"+
                    "tls.truststore.Type = \'"+ truststore_type +"\',"+
                    "tls.truststore.path = \'"+truststore_file.getAbsolutePath()+"',"+
                    "tls.truststore.password = \'"+ truststore_password +"\',"+
                    "tls.version = \'TLSv"+supported_tls_version+"\',";
        }else{
            ssl_settings = EMPTY;
        }




                String local_monitoring_data_stream_input_type_details =
                "@source(type = 'rabbitmq', " +
                        "uri = \'" + broker_uri + "\', " +
                        "exchange.name = \'" + exchange_name + "\', " +
                        "exchange.type = \'topic\', " +
                        "expiration = \'86400000\', " +
                        "routing.key = \'"+local_monitoring_fragment_topic+"\', "+
                        ssl_settings+
                        "heartbeat = \'300\', "+
                        "queue.name=\'"+fragment_queue_name+"\',"+
                        "queue.durable.enabled=\'true\',"+
                        "queue.exclusive.enabled=\'false\',"+
                        "queue.autodelete.enabled=\'false\',";

        String local_monitoring_data_stream_attribute_mapping =
                "@map(type='json', enclosing.element=\"$\", \n"
                        + "@attributes(\n";

        String local_monitoring_data_stream_definitions = "define stream " + local_monitoring_data_stream_name + " (";
        for (MonitoringMetric metric : monitoring_metrics.values()) {
            if (metric.is_locally_collected_metric() && !metric.is_predicted_metric()) {
                local_monitoring_data_stream_attribute_mapping = local_monitoring_data_stream_attribute_mapping + metric.getName() + "= \"" + metric.getName() + "\",\n";
                local_monitoring_data_stream_definitions = local_monitoring_data_stream_definitions + metric.getName() + " double, ";
            }
        }

        local_monitoring_data_stream_attribute_mapping = local_monitoring_data_stream_attribute_mapping +
                "fragid= \"fragid\", \n" +
                "res_inst = \"res_inst\", \n" +
                "timestamp = \"timestamp\"\n" +
                ")))\n";
        local_monitoring_data_stream_definitions = local_monitoring_data_stream_definitions +
                "fragid string, res_inst string, timestamp long);\n";

        String local_metrics_monitoring_stream = local_monitoring_data_stream_input_type_details + local_monitoring_data_stream_attribute_mapping + local_monitoring_data_stream_definitions;

        // -------------- Local edge metrics monitoring stream calculation


        String edge_local_monitoring_fragment_topic;
        edge_local_monitoring_fragment_topic = realtime_monitoring_data_topic.replaceAll(".(#)?$",EMPTY);
        edge_local_monitoring_fragment_topic = edge_local_monitoring_fragment_topic +".*.*.edge."+fragment_name+".#";


        String edge_fragment_queue_name =broker_amqp_queue_name_prefix+System.nanoTime();

        String edge_local_monitoring_data_stream_input_type_details =
                "@source(type = 'rabbitmq', " +
                        "uri = \'" + broker_uri + "\', " +
                        "exchange.name = \'" + exchange_name + "\', " +
                        "exchange.type = \'topic\', " +
                        "expiration = \'86400000\', " +
                        "routing.key = \'"+edge_local_monitoring_fragment_topic+"\', "+
                        ssl_settings+
                        "heartbeat = \'300\', "+
                        "queue.name=\'"+edge_fragment_queue_name+"\',"+
                        "queue.durable.enabled=\'true\',"+
                        "queue.exclusive.enabled=\'false\',"+
                        "queue.autodelete.enabled=\'false\',";

        String edge_local_monitoring_data_stream_attribute_mapping =
                "@map(type='json', enclosing.element=\"$\", \n"
                        + "@attributes(\n";

        String edge_local_monitoring_data_stream_definitions = "define stream " + edge_local_monitoring_data_stream_name + " (";
        for (MonitoringMetric metric : monitoring_metrics.values()) {
            if (metric.is_locally_collected_metric() && !metric.is_predicted_metric()) {
                edge_local_monitoring_data_stream_attribute_mapping = edge_local_monitoring_data_stream_attribute_mapping + metric.getName() + "= \"" + metric.getName() + "\",\n";
                edge_local_monitoring_data_stream_definitions = edge_local_monitoring_data_stream_definitions + metric.getName() + " double, ";
            }
        }

        edge_local_monitoring_data_stream_attribute_mapping = edge_local_monitoring_data_stream_attribute_mapping +
                "fragid= \"fragid\", \n" +
                "res_inst = \"res_inst\", \n" +
                "timestamp = \"timestamp\"\n" +
                ")))\n";
        edge_local_monitoring_data_stream_definitions = edge_local_monitoring_data_stream_definitions +
                "fragid string, res_inst string, timestamp long);\n";

        String edge_local_metrics_monitoring_stream = edge_local_monitoring_data_stream_input_type_details + edge_local_monitoring_data_stream_attribute_mapping + edge_local_monitoring_data_stream_definitions;





        // -------------- Predicted metrics monitoring stream calculation

        //predicted topics have the structure :
        //predicted_monitoring.graphID.graphInstanceID.architectureType.componentID.componentInstanceID

        String predicted_monitoring_fragment_topic;
        predicted_monitoring_fragment_topic = predicted_monitoring_data_topic.replaceAll(".(#)?$",EMPTY);
        predicted_monitoring_fragment_topic = predicted_monitoring_fragment_topic +".*.*.*."+fragment_name+".#";

        fragment_queue_name =broker_amqp_queue_name_prefix+System.nanoTime();

        String predicted_monitoring_data_stream_input_type_details =
                        "@source(type = 'rabbitmq', " +
                        "uri = \'" + broker_uri + "\', " +
                        "exchange.name = \'" + exchange_name + "\', " +
                        "exchange.type = \'topic\', " +
                        "expiration = \'86400000\', " +
                        "routing.key = \'"+predicted_monitoring_fragment_topic+"\', "+
                        ssl_settings +
                        "heartbeat = \'300\', "+
                        "queue.name=\'"+fragment_queue_name+"\',"+
                        "queue.durable.enabled=\'true\',"+
                        "queue.exclusive.enabled=\'false\',"+
                        "queue.autodelete.enabled=\'false\',";
        String predicted_monitoring_data_stream_attribute_mapping =
                "@map(type='json', enclosing.element=\"$\", \n"
                        + "@attributes(\n";
        String predicted_monitoring_data_stream_definitions = "define stream " + predicted_monitoring_data_stream_name  + " (";

        for (MonitoringMetric metric : monitoring_metrics.values()) {

            if (metric.is_predicted_metric()) {
                predicted_monitoring_data_stream_attribute_mapping = predicted_monitoring_data_stream_attribute_mapping + metric.getName() + "= \"" + metric.getName() + "\",\n";
                predicted_monitoring_data_stream_definitions = predicted_monitoring_data_stream_definitions + metric.getName() + " double, ";
            }
        }

        predicted_monitoring_data_stream_attribute_mapping = predicted_monitoring_data_stream_attribute_mapping +
                "fragid= \"fragid\", \n" +
                "res_inst = \"res_inst\", \n" +
                "prediction_timestamp = \"prediction_timestamp\"\n" +
                ")))\n";
        predicted_monitoring_data_stream_definitions = predicted_monitoring_data_stream_definitions +
                "fragid string, res_inst string, prediction_timestamp long);\n";


        String predicted_metrics_monitoring_stream = predicted_monitoring_data_stream_input_type_details + predicted_monitoring_data_stream_attribute_mapping + predicted_monitoring_data_stream_definitions;

        // -------------- Aggregated metrics monitoring stream calculation

        String aggregated_monitoring_fragment_topic;
        aggregated_monitoring_fragment_topic = realtime_monitoring_data_topic.replaceAll(".(#)?$",EMPTY);
        aggregated_monitoring_fragment_topic = aggregated_monitoring_fragment_topic +".*.*.*."+fragment_name+".#";

        fragment_queue_name =broker_amqp_queue_name_prefix+System.nanoTime();

        String aggregate_monitoring_data_stream_input_type_details =
                "@source(type = 'rabbitmq', " +
                        "uri = \'" + broker_uri + "\', " +
                        "exchange.name = \'" + exchange_name + "\', " +
                        "exchange.type = \'topic\', " +
                        "expiration = \'86400000\', " +
                        "routing.key = '" + aggregated_monitoring_fragment_topic +"',"+
                        ssl_settings +
                        "heartbeat = \'300\', "+
                        "queue.name=\'"+fragment_queue_name+"\',"+
                        "queue.durable.enabled=\'true\',"+
                        "queue.exclusive.enabled=\'false\',"+
                        "queue.autodelete.enabled=\'false\',";



        String aggregate_monitoring_data_stream_attribute_mapping =
                "@map(type='json', enclosing.element=\"$.loadBalancerMonitoringModels\", \n"
                        + "@attributes(\n";

        String aggregate_monitoring_data_stream_definitions = "define stream " + aggregate_monitoring_data_stream_name + " (";
        for (MonitoringMetric metric_entry : monitoring_metrics.values()) {
            MonitoringMetric metric = metric_entry;
            if (metric.is_metric_collected_in_aggregate()) {
                aggregate_monitoring_data_stream_attribute_mapping = aggregate_monitoring_data_stream_attribute_mapping + metric.getName() + "= \"" + metric.getName() + "\",\n";
                aggregate_monitoring_data_stream_definitions = aggregate_monitoring_data_stream_definitions + metric.getName() + " double, ";
            }
        }

        aggregate_monitoring_data_stream_attribute_mapping = aggregate_monitoring_data_stream_attribute_mapping +
                "fragid= \"loadbalancer_for_fragid\"\n" +
                ")))\n";
        aggregate_monitoring_data_stream_definitions = aggregate_monitoring_data_stream_definitions +
                "fragid string);\n";

        String aggregated_metrics_monitoring_stream = aggregate_monitoring_data_stream_input_type_details + aggregate_monitoring_data_stream_attribute_mapping + aggregate_monitoring_data_stream_definitions;

        /*String aggregated_metrics_monitoring_stream = "\t@source(type = 'rabbitmq', uri = 'amqp://guest:guest@3.120.91.124:5672', exchange.name = 'presto.cloud', routing.key = '"+realtime_monitoring_data_topic+"', @map(type='json', enclosing.element=\"$.loadBalancerMonitoringModels\", \n" +
                "     @attributes(\n" +
                "     linvocations_sec = \"linvocations_sec\", \n" +
                "     fragid = \"loadbalancer_for_fragid\", \n" +
                "     linvocations_response_time = \"linvocations_response_time\"\n" +
                "     )))\n" +
                "define stream "+ aggregate_monitoring_data_stream_name +" (linvocations_sec double, fragid string, linvocations_response_time double);\n";
       */

        // -------------- Combined monitoring stream calculation


        String combined_metrics_monitoring_stream = "define stream " + combined_metrics_monitoring_stream_name + " (";

        for (MonitoringMetric metric : monitoring_metrics.values()) {
            if(!metric.is_predicted_metric()) {
                combined_metrics_monitoring_stream = combined_metrics_monitoring_stream + metric.getName() + " double, ";
            }
        }
        combined_metrics_monitoring_stream = combined_metrics_monitoring_stream + "fragid string, res_inst string, timestamp long);\n";
       /*String combined_metrics_monitoring_stream = "define stream monitoringStream (disk_available double, cpu_sys double, cpu_user double, cpu_io_wait double, cpu_perc double, mem_free double, mem_perc double, net_rec double, net_sent double, packets_sent double, packets_received double, throughput_rate_fps double, response_time_ms double, latency_ms double, fragid string, res_inst string, timestamp long, linvocations_sec double, linvocations_response_time double);\n";

        String old_stream =  "@source(\n"+
                "\ttype ='rabbitmq',\n" +
                "\turi = 'amqp://guest:guest@3.120.91.124:5672',\n" +
                "\texchange.name = 'presto.cloud',\n" +
                "\trouting.key= 'monitoring.#',\n" +
                "\t@map(\n" +
                "\t\ttype='json'\t\t\n" +
                "\t)\n" +
                ")\n"+
                "define stream serverStream (disk_available double, cpu_sys double, cpu_user double, cpu_io_wait double, cpu_perc double, mem_free double, mem_perc double, net_rec double, net_sent double, packets_sent double, packets_received double, fragid string, res_inst string, timestamp long);\n";

       */
       SiddhiStream local_monitoring_stream,edge_local_monitoring_stream, aggregated_monitoring_stream, combined_monitoring_stream, predicted_monitoring_stream;
       local_monitoring_stream = new SiddhiStream(local_monitoring_data_stream_name,local_metrics_monitoring_stream);
       edge_local_monitoring_stream = new SiddhiStream(edge_local_monitoring_data_stream_name,edge_local_metrics_monitoring_stream);
       aggregated_monitoring_stream = new SiddhiStream(aggregate_monitoring_data_stream_name,aggregated_metrics_monitoring_stream);
       combined_monitoring_stream = new SiddhiStream(combined_metrics_monitoring_stream_name,combined_metrics_monitoring_stream);
       predicted_monitoring_stream = new SiddhiStream(predicted_monitoring_data_stream_name,predicted_metrics_monitoring_stream);

        stream_definitions.put(local_monitoring_stream.getName(),local_monitoring_stream);
        stream_definitions.put(edge_local_monitoring_stream.getName(),edge_local_monitoring_stream);
        stream_definitions.put(aggregated_monitoring_stream.getName(),aggregated_monitoring_stream);
        stream_definitions.put(combined_monitoring_stream.getName(),combined_monitoring_stream);
        stream_definitions.put(predicted_monitoring_stream.getName(),predicted_monitoring_stream);

    }


    /**
     * Returns the monitoring_metric_queries which are necessary to calculate the upper and lower boundaries of an unbounded stream, with 96% precision according to the equation of Chebyshev
     *
     * @return A string containing the Siddhi monitoring_metric_queries required to calculate the boundaries of the stream
     */
    public HashMap<String, SiddhiQuery> get_unbounded_metric_queries() {
        HashMap<String, SiddhiQuery> unbounded_metric_queries = new HashMap<>();


        for (MonitoringMetric monitoring_metric : monitoring_metrics.values()) {
            String input_stream_for_bounds_calculation; //precalculated, in case it is needed

            if (monitoring_metric.is_predicted_metric()){
                input_stream_for_bounds_calculation = predicted_monitoring_data_stream_name;
            }
            else if (monitoring_metric.is_locally_collected_metric()){
                input_stream_for_bounds_calculation = local_monitoring_data_stream_name;
            }else if (monitoring_metric.is_metric_collected_in_aggregate()){
                input_stream_for_bounds_calculation = aggregate_monitoring_data_stream_name;
            }else{//should never enter here, but inserted as a catch-all case
                input_stream_for_bounds_calculation = combined_metrics_monitoring_stream_name;
            }

            if (monitoring_metric.is_downwards_unbounded() && monitoring_metric.is_upwards_unbounded()) {
                unbounded_metric_queries.put(monitoring_metric.getName(), create_bounds_stream_query(input_stream_for_bounds_calculation, monitoring_metric.getName()));
            } else if (monitoring_metric.is_upwards_unbounded()) {
                unbounded_metric_queries.put(monitoring_metric.getName(), create_bounds_stream_query(input_stream_for_bounds_calculation, monitoring_metric.getName(), true, false, monitoring_metric.getLower_bound()));
            } else if (monitoring_metric.is_downwards_unbounded()) {
                unbounded_metric_queries.put(monitoring_metric.getName(), create_bounds_stream_query(input_stream_for_bounds_calculation, monitoring_metric.getName(), false, true, monitoring_metric.getUpper_bound()));
            }
        }

        return unbounded_metric_queries;
    }


    public HashMap<String, MonitoringMetric> getMonitoring_metrics() {
        return monitoring_metrics;
    }


    public void setMonitoring_metrics(HashMap<String, MonitoringMetric> monitoring_metrics) {
        this.monitoring_metrics = monitoring_metrics;
    }

    public void add_monitoring_metric(MonitoringMetric monitoring_metric) {

        if (!monitoring_metrics.containsKey(monitoring_metric.getName())) {
            if (!monitoring_metric.getName().startsWith(predicted_monitoring_metrics_prefix)) {
                monitoring_metrics.put(monitoring_metric.getName(), monitoring_metric);
                String predicted_metric_name = predicted_monitoring_metrics_prefix + monitoring_metric.getName();

                MonitoringMetric predicted_metric = new MonitoringMetric(monitoring_metric);
                if (monitoring_metric.is_locally_collected_metric() ||
                        (monitoring_metric.is_metric_collected_in_aggregate() && aggregate_metrics_predicted)) { //aggregate metrics are not predicted in general
                    predicted_metric.setName(predicted_metric_name);
                    predicted_metric.set_predicted_metric(true);
                    monitoring_metrics.put(predicted_metric.getName(), predicted_metric);
                }

                unbounded_monitoring_metrics.put(monitoring_metric.getName(), monitoring_metric);
                unbounded_monitoring_metrics.put(predicted_metric.getName(), predicted_metric);


            } else { //The user has dynamically sent a new predicted custom metric request, but the normal metric is not used in any rule
                predicted_monitoring_metrics.put(monitoring_metric.getName(), monitoring_metric);
                MonitoringMetric normal_metric = new MonitoringMetric(monitoring_metric);

                String normal_metric_name = monitoring_metric.getName().replaceFirst(predicted_monitoring_metrics_prefix, EMPTY);
                normal_metric.setName(normal_metric_name);
                monitoring_metrics.put(normal_metric.getName(), normal_metric);

                unbounded_monitoring_metrics.put(monitoring_metric.getName(), monitoring_metric);
                unbounded_monitoring_metrics.put(normal_metric_name, normal_metric);

            }
        }

    }

    public static Map<String, Fragment> getFragments() {
        return fragments;
    }

    public static void setFragments(HashMap<String, Fragment> fragments) {
        Fragment.fragments = fragments;
    }

    public static void addFragment(Fragment fragment) {
        synchronized (can_manipulate_fragments_map) {
            if (!can_manipulate_fragments_map.getValue()) {
                try {
                    can_manipulate_fragments_map.wait();
                }catch (Exception e){
                    e.printStackTrace();
                }
                can_manipulate_fragments_map.setFalse();
            }
            fragments.put(fragment.fragment_name, fragment);
        }
    }

    public HashMap<String,SiddhiStream> getStream_definitions() {
        return stream_definitions;
    }

    public void setStream_definitions(HashMap<String,SiddhiStream> stream_definitions) {
        this.stream_definitions = stream_definitions;
    }

    public HashMap <String,SiddhiQuery> getMonitoring_metric_queries() {
        return monitoring_metric_queries;
    }

    public void setMonitoring_metric_queries(HashMap <String,SiddhiQuery> monitoring_metric_queries) {
        this.monitoring_metric_queries = monitoring_metric_queries;
    }

    public String getFragment_name() {
        return fragment_name;
    }

    public void setFragment_name(String fragment_name) {
        this.fragment_name = fragment_name;
    }

    public String getPredicted_monitoring_data_stream_name() {
        return predicted_monitoring_data_stream_name;
    }

    public void setPredicted_monitoring_data_stream_name(String predicted_monitoring_data_stream_name) {
        this.predicted_monitoring_data_stream_name = predicted_monitoring_data_stream_name;
    }

    public String getLocal_monitoring_data_stream_name() {
        return local_monitoring_data_stream_name;
    }

    public void setLocal_monitoring_data_stream_name(String local_monitoring_data_stream_name) {
        this.local_monitoring_data_stream_name = local_monitoring_data_stream_name;
    }

    public String getAggregate_monitoring_data_stream_name() {
        return aggregate_monitoring_data_stream_name;
    }

    public void setAggregate_monitoring_data_stream_name(String aggregate_monitoring_data_stream_name) {
        this.aggregate_monitoring_data_stream_name = aggregate_monitoring_data_stream_name;
    }

    public String getCombined_metrics_monitoring_stream_name() {
        return combined_metrics_monitoring_stream_name;
    }

    public void setCombined_metrics_monitoring_stream_name(String combined_metrics_monitoring_stream_name) {
        this.combined_metrics_monitoring_stream_name = combined_metrics_monitoring_stream_name;
    }

    public HashMap<String, MonitoringMetric> getPredicted_monitoring_metrics() {
        return predicted_monitoring_metrics;
    }

    public void setPredicted_monitoring_metrics(HashMap<String, MonitoringMetric> predicted_monitoring_metrics) {
        this.predicted_monitoring_metrics = predicted_monitoring_metrics;
    }

    public void update_queries() {
        initialize_stream_definitions();
        initialize_queries();
    }

    public HashSet<String> getScalability_rules_associated_with_fragment() {
        return scalability_rules_associated_with_fragment;
    }

    public void setScalability_rules_associated_with_fragment(HashSet<String> scalability_rules_associated_with_fragment) {
        this.scalability_rules_associated_with_fragment = scalability_rules_associated_with_fragment;
    }
}
