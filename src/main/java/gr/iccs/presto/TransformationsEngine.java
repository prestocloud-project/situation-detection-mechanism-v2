package gr.iccs.presto;

import gr.iccs.presto.utility_classes.MonitoringMetric;
import gr.iccs.presto.utility_classes.Operand;
import gr.iccs.presto.utility_classes.SubRule;

import static gr.iccs.presto.Globals.EMPTY;
import static gr.iccs.presto.utility_classes.Fragment.getFragments;

public class TransformationsEngine {
    public static ScalabilityRule process_ui_rule(String ui_rule_json) {
        ScalabilityRule new_rule =  new ScalabilityRule(ui_rule_json);
        new_rule.getMonitored_fragment_names().forEach(fragment_name -> {
            getFragments().get(fragment_name).getScalability_rules_associated_with_fragment().add(new_rule.getHex_id());
        });
        return new_rule;
    }


    public static String get_aggregation_alias_of_metric(String aggregation_operand, String metric) {

        if (aggregation_operand!=null && !aggregation_operand.equals(EMPTY)){
            return aggregation_operand + "_" + metric;
        }else{
            return metric;
        }
    }

    public static String get_aggregation_function(SubRule sub_rule) {

        String aggregation_function = sub_rule.getAggregation_function();
        if (aggregation_function!=null && !aggregation_function.equals("NONE")) {


            if (aggregation_function.equals("AVG")){
                return "avg";
            }else if (aggregation_function.equals("SUM")) {
                return "sum"; //TODO verify
            }
        }else {
            return EMPTY;
        }
        return EMPTY;
    }


    public static String get_conditional_operand(Operand operand) {

        if (operand!=null) {
            switch (operand) {
                case NONE:
                    return EMPTY;
                case GREATER:
                    return ">";
                case LESS:
                    return "<";
                case GREATEROREQUAL:
                    return ">=";
                case LESSOREQUAL:
                    return "<=";
                default:
                    return EMPTY;
            }
        }else {
            return EMPTY;
        }
    }




    public static boolean is_aggregated_metric(MonitoringMetric metric){
        if(metric.is_metric_collected_in_aggregate()){
            return true;
        }else{
            return false;
        }
    }

    public static boolean is_local_metric(MonitoringMetric metric){
        if(metric.is_locally_collected_metric()){
            return true;
        }else{
            return false;
        }
    }




}