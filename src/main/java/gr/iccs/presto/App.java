package gr.iccs.presto;

import gr.iccs.presto.messaging.AMQP_Publisher;
import gr.iccs.presto.messaging.AMQP_Subscriber;
import gr.iccs.presto.utility_classes.Fragment;
import gr.iccs.presto.utility_classes.MonitoringMetric;
import gr.iccs.presto.utility_classes.SynchronizedBoolean;
import io.siddhi.core.event.Event;
import io.siddhi.core.stream.output.StreamCallback;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.log4j.LogManager;
import org.json.simple.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import static gr.iccs.presto.Globals.*;
import static gr.iccs.presto.SiddhiStream.get_input_stream_definitions;
import static gr.iccs.presto.SiddhiStream.get_unbounded_metric_definitions;
import static gr.iccs.presto.SituationCalculator.get_severity_zone;
import static gr.iccs.presto.messaging.SSLUtilities.backUpDirName;
import static gr.iccs.presto.messaging.SSLUtilities.prepareSSL;
import static gr.iccs.presto.utility_classes.Fragment.can_manipulate_fragments_map;
import static gr.iccs.presto.utility_classes.Fragment.getFragments;
import static java.lang.Math.max;
import static java.util.logging.Level.*;

public class App {


    public static final SynchronizedBoolean NEW_SCALABILITY_RULE_OPERATION_DETECTED = new SynchronizedBoolean(false);
    public static final SynchronizedBoolean TRUSTSTORE_AVAILABLE = new SynchronizedBoolean(false);
    public static final Map<String, ScalabilityRule> scalability_rules = new ConcurrentHashMap<>();

    public static final Map<String,SiddhiStream> stream_definitions = Collections.synchronizedMap(new LinkedHashMap<>());//The names of the streams are the keys
    private static String unbounded_metric_definitions = EMPTY;
    public static final Map<String, SiddhiQuery> queries = Collections.synchronizedMap(new LinkedHashMap<>());
    public static final Map<String, SiddhiQuery> unbounded_metrics_queries = new ConcurrentHashMap<>(); // A different data structure is created because unbounded metrics may be used by different fragments, and it is desirable to only maintain a calculation for their bounds only once
    private static final AMQP_Publisher publisher = new AMQP_Publisher();
    //static ExecutorService executor = Executors.newFixedThreadPool(4);
    public static final Map<String,Integer> previous_adaptations = new ConcurrentHashMap<>();
    private static boolean REPLAY_DATA_DETECTED;

    /**
     * The steps carried out in the main method are the following:
     * 1. Retrieval of configuration values from a properties file
     * 2. Addition of definition Siddhi streams based on the monitoring metrics which are inside the properties file
     * 3. Creation of subscriber to listen to new scalability events
     * 4. Retrieval of new ui rule and addition to a new Siddhi instance, replacing the old one
     */
    public static void main(String[] args) throws InterruptedException {

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss.SSS");
        boolean DEBUG_EVENTS = false;
        Properties prop = new Properties();
        InputStream preferences_file;

        try {
            if (args == null || args.length < 1) {
                Logger.getAnonymousLogger().log(WARNING, "No preferences file has been specified manually, will attempt to find one in the default path");
                preferences_file = new FileInputStream("/home/prestocloud/sdm/preferences.properties");
            } else {
                Logger.getAnonymousLogger().log(INFO, "A preferences file has been manually specified");
                preferences_file = new FileInputStream(args[0]);
            }
            prop.load(preferences_file);

            number_of_severity_zones = Integer.parseInt(prop.getProperty("number_of_severity_zones"));
            number_of_events_for_context = Integer.parseInt(prop.getProperty("number_of_events_for_context"));
            broker_ip_address = prop.getProperty("broker_ip_address");
            broker_uri = prop.getProperty("broker_uri");
            truststore_password = prop.getProperty("truststore_password");
            if(broker_uri.endsWith(":5671")){
                enforce_ssl = true;
            }
            broker_username = get_broker_username(new String(broker_uri));
            broker_password = get_broker_password(new String(broker_uri));
            exchange_name = prop.getProperty("exchange_name");
            vault_address = prop.getProperty("vault_address");
            vault_token = prop.getProperty("vault_token");
            mca_fragment_data_topic = prop.getProperty("mca_fragment_data_topic");
            undeployment_topic = prop.getProperty("undeployment_topic");
            realtime_monitoring_data_topic = prop.getProperty("realtime_monitoring_data_topic");
            predicted_monitoring_data_topic = prop.getProperty("predicted_monitoring_data_topic");
            situations_detected_topic = prop.getProperty("situations_detected_topic");
            ui_information_topic = prop.getProperty("ui_information_topic");
            ui_situations_detected_topic = prop.getProperty("ui_situations_detected_topic");
            create_monitoring_metrics(prop.getProperty("realtime_monitoring_metrics").split(","));
            create_predicted_monitoring_metrics(prop.getProperty("predicted_monitoring_metrics").split(","));
            scalability_rules_manipulation_topic = prop.getProperty("scalability_rules_manipulation_topic");
            unbounded_metric_samples = Integer.parseInt(prop.getProperty("unbounded_metric_samples"));
            monitoring_events_frequency_msec = Integer.parseInt(prop.getProperty("monitoring_metric_frequency_msec"));

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (situations_detected_topic==null || situations_detected_topic.equals(EMPTY)){
            situations_detected_topic="situations";
        }
        if (ui_information_topic==null || ui_information_topic.equals(EMPTY)){
            ui_information_topic = "graph-triggered-info";
        }
        if (ui_situations_detected_topic==null || ui_situations_detected_topic.equals(EMPTY)){
            ui_situations_detected_topic = "graph-triggered-actions";
        }

        SiddhiRunner running_siddhi_thread = new SiddhiRunner();


        AMQP_Subscriber ui_rule_subscriber = new AMQP_Subscriber();
        ui_rule_subscriber.subscribe();
        if (enforce_ssl) {
            try {
                prepareSSL();
            } catch (Exception e) {
                Logger.getAnonymousLogger().log(SEVERE,"Problem while trying to retrieve SSL certificates to securely subscribe to the broker");
                e.printStackTrace();
            }
        }
        //Wait here for the transmission of newly defined scalability rules. In parallel, processing is carried out according to the rules which have already been provided

        do {
            synchronized (NEW_SCALABILITY_RULE_OPERATION_DETECTED) {
                if (!NEW_SCALABILITY_RULE_OPERATION_DETECTED.getValue()) {
                    try {
                        NEW_SCALABILITY_RULE_OPERATION_DETECTED.wait();
                    } catch (InterruptedException i) {
                        Logger.getAnonymousLogger().log(SEVERE, "The thread expecting a notification from the AMQP service subcribing to the Broker was interrupted from waiting. The value of NEW_SCALABILITY_RULE_OPERATION_DETECTED was " + NEW_SCALABILITY_RULE_OPERATION_DETECTED + " and the stacktrace follows");
                        i.printStackTrace();
                    }
                }
                NEW_SCALABILITY_RULE_OPERATION_DETECTED.setFalse();
            }
            //This first level of initialization ensures that the basic monitoring queries which are required
            //to receive monitoring events are properly handled. These queries are retrieved from the fragment
            //objects.
            synchronized (can_manipulate_fragments_map) {
                can_manipulate_fragments_map.setFalse();
                initialize_stream_definitions();
                initialize_queries();
                can_manipulate_fragments_map.setTrue();
                can_manipulate_fragments_map.notify();
            }

            synchronized (scalability_rules) {
                // ----------------------- Creation of replacement siddhi file containing the updated rules

                //This second level of initialization relies on the abstraction of scalability_rules to retrieve queries
                //associated with the Scalability rules which are dynamically created.

                for (Map.Entry<String, ScalabilityRule> scalability_rule_entry : scalability_rules.entrySet()) {
                    ScalabilityRule scalability_rule = scalability_rule_entry.getValue();
                    synchronized (queries) {
                        queries.put(scalability_rule.getAggregator_siddhi_query().getOutput_stream_name(), scalability_rule.getAggregator_siddhi_query());
                        queries.put(scalability_rule.getRule_siddhi_query().getOutput_stream_name(), scalability_rule.getRule_siddhi_query());
                    }
                    synchronized (stream_definitions) {
                        stream_definitions.put(scalability_rule.getRule_output_stream().getName(), scalability_rule.getRule_output_stream());
                    }
                }
                SiddhiRunner replacement_siddhi_thread;
                synchronized (stream_definitions) {
                    synchronized (queries) {
                        replacement_siddhi_thread = new SiddhiRunner(stream_definitions,queries);
                        replacement_siddhi_thread.start();
                    }
                }

                if (running_siddhi_thread.has_started_execution()){
                    Enumeration<org.apache.log4j.Logger> loggers = LogManager.getCurrentLoggers();
                    while (loggers.hasMoreElements()){
                        loggers.nextElement().setLevel(org.apache.log4j.Level.OFF); //Stopping logging to prevent error output messages concerning already closed channels
                    }
                    //Uncomment to turn off all logging
                    //LogManager.getRootLogger().setLevel(org.apache.log4j.Level.OFF);
                    running_siddhi_thread.getSiddhi_app_runtime().shutdown();
                    running_siddhi_thread.getSiddhi_manager().shutdown();
                    running_siddhi_thread.join();
                }

                running_siddhi_thread = replacement_siddhi_thread;


                // ----------------------- Unbounded attribute bound(s) calculation

                for (HashMap.Entry<String,MonitoringMetric> unbounded_metric_entry : unbounded_monitoring_metrics.entrySet()) {
                    MonitoringMetric unbounded_metric = unbounded_metric_entry.getValue();
                    synchronized (running_siddhi_thread) {
                        if (!running_siddhi_thread.has_started_execution()) {
                            try {
                                running_siddhi_thread.wait();
                            } catch (InterruptedException i) {
                                i.printStackTrace();
                            }
                        }
                    }
                    running_siddhi_thread.getSiddhi_app_runtime().addCallback("unbounded_metric_" + unbounded_metric.getName(), new StreamCallback() {
                        @Override
                        public void receive(Event[] events) {
                            for (Event event : events) {
                                if (unbounded_monitoring_metrics.get(unbounded_metric.getName()).getUnbounded_metric_counter() % (max(1, unbounded_metric_samples / 100)) == 0) {
                                    unbounded_monitoring_metrics.get(unbounded_metric.getName()).setLower_bound((Double) event.getData()[0]); //the first metric in the definition of the bounds stream corresponds to the lower bound
                                    unbounded_monitoring_metrics.get(unbounded_metric.getName()).setUpper_bound((Double) event.getData()[1]); //the second metric in the definition of the bounds stream corresponds to the upper bound
                                    //Logger.getAnonymousLogger().log(INFO,"Lower bound "+event.getData()[0]+" Upper bound "+event.getData()[1]);
                                }
                                unbounded_monitoring_metrics.get(unbounded_metric.getName()).increaseUnbounded_metric_counter();
                            }
                        }
                    });
                }
                if (DEBUG_EVENTS) {
                    running_siddhi_thread.getSiddhi_app_runtime().addCallback("combinedMonitoringStream", new StreamCallback() {
                        @Override
                        public void receive(Event[] events) {
                            String uuid_of_receiver = UUID.randomUUID().toString().replaceAll("-", EMPTY).substring(0, 10);
                            for (Event event : events) {
                                Date dt = new Date();
                                String S = sdf.format(dt);
                                System.out.println(S + ", " + uuid_of_receiver + ", " + event.getData()[16] + ", " + event.getData()[9] + ", linvocations_sec, " + event.getData()[4] + ", linvocations resp_time, " + event.getData()[5]);
                            }
                        }
                    });
                }


                // ---------------------- Severity zone calculation & publishing of situation event

                for (Map.Entry<String, ScalabilityRule> scalability_rule_entry : scalability_rules.entrySet()) {

                    ScalabilityRule scalability_rule = scalability_rule_entry.getValue();
                    SiddhiStream output_stream = scalability_rule.getRule_output_stream();
                    String output_stream_name = output_stream.getName();
                    ArrayList<String> output_stream_metrics = output_stream.getMetrics();

                    Logger.getAnonymousLogger().log(INFO, "Adding callback for " + output_stream_name);
                    try {
                        synchronized (stream_definitions){
                            running_siddhi_thread.getSiddhi_app_runtime().addCallback(output_stream_name, new StreamCallback() {
                                @Override
                                public void receive(Event[] events) {

                                    for (Event event : events) {

                                        int calculated_severity_zone = get_severity_zone(event.getData(), scalability_rule.getSub_rules());

                                        JSONObject situation_json_object = new JSONObject();
                                        JSONObject event_json_object = new JSONObject();

                                        event_json_object.put("rule_id", scalability_rule_entry.getKey());
                                        event_json_object.put("zone", calculated_severity_zone);
                                        event_json_object.put("timestamp", event.getTimestamp() / 1000);
                                        event_json_object.put("res_inst", toMap(event).get("res_inst"));
                                        event_json_object.put("fragid", scalability_rule.getScaled_component_name()

                                        );
                                        event_json_object.put("action", scalability_rule.getSituation_action().toString());

                                        JSONObject attributes_over_threshold = new JSONObject();
                                        for (int i = 0; i < event.getData().length - 1; i++) { //length-1 as the last item is the res_inst
                                            attributes_over_threshold.put(output_stream_metrics.get(i), event.getData()[i]);

                                            String original_metric_name = output_stream_metrics.get(i).replaceAll("^(avg_)|^(sum_)", EMPTY);

                                            MonitoringMetric unbounded_monitoring_metric = unbounded_monitoring_metrics.get(original_metric_name);
                                            if (unbounded_monitoring_metric != null) {
                                                double upper_bound = unbounded_monitoring_metric.getUpper_bound(); //the second metric in the definition of the bounds stream corresponds to the upper bound
                                                double lower_bound = unbounded_monitoring_metric.getLower_bound();
                                                Logger.getAnonymousLogger().log(INFO, "The lower bound for the " + unbounded_monitoring_metric.getName() + " is " + lower_bound + " and the upper bound is " + upper_bound);
                                            }
                                        }
                                        event_json_object.put("attributes",attributes_over_threshold);
                                        situation_json_object.put("event", event_json_object);
                                        Logger.getAnonymousLogger().log(INFO, "A situation object was created\n" + situation_json_object.toJSONString());
                                        boolean situation_sent = handle_situation_publishing(scalability_rule, calculated_severity_zone, situation_json_object);
                                        if (situation_sent) {
                                            previous_adaptations.put(scalability_rule.getHex_id(), (new Long(System.currentTimeMillis() / 1000)).intValue());
                                        }


                                    }

                                }
                            });
                        }
                    }catch (Exception e){
                        Logger.getAnonymousLogger().log(WARNING,"Could not register output stream "+output_stream_name);
                        if (!stream_definitions.containsKey(output_stream_name)){
                            Logger.getAnonymousLogger().log(WARNING,"The output stream does not exist in the stream definitions");
                        }
                    }
                }
                //}
            }

        }while (true);

    }

    private static boolean handle_situation_publishing(ScalabilityRule scalability_rule, int calculated_severity_zone, JSONObject situation_json_object){
        String rule_id = scalability_rule.getHex_id();
        int inertia_time = scalability_rule.getInertia_time();
        Integer last_adaptation_time = previous_adaptations.get(rule_id);
        int delta_time;
        if (last_adaptation_time!=null) {
            delta_time = (new Long(System.currentTimeMillis() / 1000)).intValue() - last_adaptation_time;
        }else{
            if (scalability_rules.get(rule_id)!=null) {
                delta_time = Integer.MAX_VALUE;
                Logger.getAnonymousLogger().log(INFO, "This is the first encounter of a situation with policy hex id " + rule_id);
            }else{
                //the triggered rule does no more exist, as it was probably deleted
                Logger.getAnonymousLogger().log(INFO, "The rule with policy hex id " + rule_id+" was probably deleted, and as a result a detected situation will not be enforced");
                return false;
            }
        }
        if ((delta_time>=inertia_time) || !INERTIA_TIME_RESPECTED_SCALABILITY_RULE){

        publish_situation_to_ui_topics(scalability_rule,calculated_severity_zone);


        if (
                !scalability_rule.getSituation_action().equals(SituationAction.info) &&
                        !scalability_rule.getSituation_action().equals(SituationAction.pushIntoKafkaTopic))
        {
            //AMQP_Publisher publisher = new AMQP_Publisher();
            publisher.publish(situations_detected_topic, situation_json_object.toJSONString());

                                    /*
                                    Thread broker_pusher = new Thread(new Runnable() {
                                        @Override
                                        public void run() {
                                            publisher.publish(situations_detected_topic, situation_json_object.toJSONString());
                                        }
                                    });
                                    broker_pusher.setDaemon(true);
                                    broker_pusher.start();
                                    */



            //executor.execute(new BrokerEventPublisher(situations_detected_topic,situation_json_object.toJSONString()));

            Logger.getAnonymousLogger().log(INFO,situations_detected_topic+situation_json_object.toJSONString());


        }else if (scalability_rule.getSituation_action().equals(SituationAction.pushIntoKafkaTopic)) {

            pushIntoKafkaTopic(scalability_rule.getKafka_topic(), scalability_rule.getContext_on_action(), scalability_rule.getKafka_url());

                  /*
                                    Thread kafka_pusher = new Thread(new Runnable() {

                                        @Override
                                        public void run() {
                                            pushIntoKafkaTopic(scalability_rule.getKafka_topic(),scalability_rule.getContext_on_action(),scalability_rule.getKafka_url());
                                        }
                                    });
                                    kafka_pusher.setDaemon(true);
                                    kafka_pusher.start();
                                    */

            //executor.execute(new KafkaEventProducer(scalability_rule.getKafka_topic(),scalability_rule.getContext_on_action(),scalability_rule.getKafka_url()));

            Logger.getAnonymousLogger().log(INFO, scalability_rule.getKafka_topic() + scalability_rule.getContext_on_action());

        }
        return true;
        }else{
            Logger.getAnonymousLogger().log(WARNING,"Did not send more situation events as this would violate the inertia time");
            return false;
        }
    }


    private static String get_broker_username(String broker_uri) {
        return broker_uri.replaceAll("amqp://?",EMPTY).replaceAll("@(.)*",EMPTY).split(":")[0];
    }

    private static String get_broker_password(String broker_uri) {
        return broker_uri.replaceAll("amqp://?",EMPTY).replaceAll("@(.)*",EMPTY).split(":")[1];
    }


    private static void create_predicted_monitoring_metrics(String[] predicted_monitoring_metric_definitions) {


        for (String predicted_monitoring_metric_definition : predicted_monitoring_metric_definitions){

            double lower_bound_double,higher_bound_double;

            String collection_mode = "local"; //if a metric is predicted, it is assumed to be a local metric

            String name = predicted_monitoring_metric_definition.split("\\[")[0];
            String lower_bound = predicted_monitoring_metric_definition.split("\\[")[1].split(",")[0].split(":")[0];

            if (lower_bound.equalsIgnoreCase("-")){
                lower_bound_double = Double.NaN;
            }else{
                lower_bound_double = Double.parseDouble(lower_bound);
            }

            String higher_bound = predicted_monitoring_metric_definition.split("\\[")[1].split(",")[0].split(":")[1].split("\\]")[0];

            if (higher_bound!=null && higher_bound.equalsIgnoreCase("-")){
                higher_bound_double = Double.NaN;
            }else{
                higher_bound_double = Double.parseDouble(higher_bound);
            }

            MonitoringMetric monitoring_metric = new MonitoringMetric(name,collection_mode,higher_bound_double,lower_bound_double);
            monitoring_metric.set_predicted_metric(true);
            HashSet <String> fragments_using_monitoring_metric = new HashSet<>();

            try {
                String[] fragments_using_particular_metric_string = predicted_monitoring_metric_definition.split("\\{")[1].split("\\}");
                if (fragments_using_particular_metric_string.length > 0) {
                    fragments_using_monitoring_metric = new HashSet<>(Arrays.asList(predicted_monitoring_metric_definition.split("\\{")[1].split("\\}")[0].split(",")));
                }else{
                    common_monitoring_metrics.put(monitoring_metric.getName(),monitoring_metric);
                }
            }catch (Exception e){
                try {
                    Logger.getAnonymousLogger().log(SEVERE, "Could not understand the input from the properties configuration file for monitoring metric " + monitoring_metric.getName()+". Inserting to common monitoring metrics");
                    common_monitoring_metrics.put(monitoring_metric.getName(),monitoring_metric);
                }catch (Exception f){
                    f.printStackTrace();
                }
            }

            for (String fragment_name : fragments_using_monitoring_metric) {
                if(getFragments().containsKey(fragment_name)) {
                    Fragment fragment = getFragments().get(fragment_name);
                    fragment.add_monitoring_metric(monitoring_metric);
                }else{
                    Fragment fragment = new Fragment(fragment_name,new ConcurrentHashMap<String,MonitoringMetric>(){{ putAll(common_monitoring_metrics);put(monitoring_metric.getName(),monitoring_metric);}});
                }
            }
            predicted_monitoring_metrics.put(monitoring_metric.getName(),monitoring_metric);
        }
        //add the monitoring variables which are used by all fragments
        for (Fragment fragment : getFragments().values()){
            common_monitoring_metrics.values().forEach(fragment::add_monitoring_metric);
        }

    }

    private static JSONObject publish_situation_to_ui_topics(ScalabilityRule scalability_rule, int calculated_severity_zone ) {
        JSONObject event_json_object = new JSONObject();

        SituationAction situation_action  = scalability_rule.getSituation_action();
        String reported_situation_action =
                situation_action.equals(SituationAction.scale_in)?"scaleIn":
                situation_action.equals(SituationAction.scale_out)?"scaleOut":
                situation_action.equals(SituationAction.pushIntoKafkaTopic)?"pushIntoKafkaTopic":
                situation_action.equals(SituationAction.info)?"info":EMPTY;


        event_json_object.put("graphHexID",scalability_rule.getGraphHexID());
        event_json_object.put("graphInstanceHexID",scalability_rule.getGraphInstanceHexID());
        event_json_object.put("componentNodeHexID",scalability_rule.getScaled_component_HexID()); // The component which will be scaled in/out
        event_json_object.put("action",reported_situation_action);
        event_json_object.put("context",scalability_rule.getContext_on_action());
        event_json_object.put("actionAmount", calculated_severity_zone); //bypassing the RARecom and issuing a number of adaptation instances directly related to the severity zone

        Logger.getAnonymousLogger().log(INFO, event_json_object.toJSONString());
        //AMQP_Publisher publisher = new AMQP_Publisher();

        if (scalability_rule.getSituation_action().equals(SituationAction.info)) {
            publisher.publish(ui_information_topic, event_json_object.toJSONString());
            //executor.execute(new BrokerEventPublisher(ui_information_topic,event_json_object.toJSONString()));

            Logger.getAnonymousLogger().log(INFO, ui_information_topic + ":" + event_json_object.toJSONString());

        }else if (scalability_rule.getSituation_action().equals(SituationAction.scale_in)||scalability_rule.getSituation_action().equals(SituationAction.scale_out)){
            publisher.publish(ui_situations_detected_topic, event_json_object.toJSONString());
            //executor.execute(new BrokerEventPublisher(ui_situations_detected_topic,event_json_object.toJSONString()));
            Logger.getAnonymousLogger().log(INFO, ui_situations_detected_topic+ ":" + event_json_object.toJSONString());
        }

        return event_json_object;
    }
    private static void create_monitoring_metrics(String[] monitoring_metric_definitions) {

        for (String monitoring_metric_definition : monitoring_metric_definitions){

            double lower_bound_double,higher_bound_double;

            String collection_mode = monitoring_metric_definition.startsWith("local_")?"local":monitoring_metric_definition.startsWith("aggregate_")?"aggregate":monitoring_metric_definition.startsWith("prediction_")?"local":EMPTY; //if a metric is predicted, it is assumed to be a local metric

            String name = monitoring_metric_definition.split("\\[")[0].replaceFirst("^(local_)|^(aggregate_)",EMPTY);
            String lower_bound = monitoring_metric_definition.split("\\[")[1].split(",")[0].split(":")[0];

            if (lower_bound.equalsIgnoreCase("-")){
                lower_bound_double = Double.NaN;
            }else{
                lower_bound_double = Double.parseDouble(lower_bound);
            }

            String higher_bound = monitoring_metric_definition.split("\\[")[1].split(",")[0].split(":")[1].split("\\]")[0];

            if (higher_bound!=null && higher_bound.equalsIgnoreCase("-")){
                higher_bound_double = Double.NaN;
            }else{
                higher_bound_double = Double.parseDouble(higher_bound);
            }

            MonitoringMetric monitoring_metric = new MonitoringMetric(name,collection_mode,higher_bound_double,lower_bound_double);
            HashSet <String> fragments_using_monitoring_metric = new HashSet<>();

            try {
                String[] fragments_using_particular_metric_string = monitoring_metric_definition.split("\\{")[1].split("\\}");
                if (fragments_using_particular_metric_string.length > 0) {
                    fragments_using_monitoring_metric = new HashSet<>(Arrays.asList(monitoring_metric_definition.split("\\{")[1].split("\\}")[0].split(",")));
                }else{
                    common_monitoring_metrics.put(monitoring_metric.getName(),monitoring_metric);
                }
            }catch (Exception e){
                try {
                    Logger.getAnonymousLogger().log(SEVERE, "Could not understand the input from the properties configuration file for monitoring metric " + monitoring_metric.getName()+". Inserting to common monitoring metrics");
                    common_monitoring_metrics.put(monitoring_metric.getName(),monitoring_metric);
                }catch (Exception f){
                    f.printStackTrace();
                }
            }

            for (String fragment_name : fragments_using_monitoring_metric) {
                if(getFragments().containsKey(fragment_name)) {
                    Fragment fragment = getFragments().get(fragment_name);
                    fragment.add_monitoring_metric(monitoring_metric);
                }else{
                    Fragment fragment = new Fragment(fragment_name,new ConcurrentHashMap<String,MonitoringMetric>(){{ putAll(common_monitoring_metrics);put(monitoring_metric.getName(),monitoring_metric);}});
                }
            }
            monitoring_metrics.put(monitoring_metric.getName(),monitoring_metric);
        }
        //add the monitoring variables which are used by all fragments
        for (Fragment fragment : getFragments().values()){
            common_monitoring_metrics.values().forEach(fragment::add_monitoring_metric);
        }

    }


    /**
     * This method pushes a message into a kafka topic
     * @author Panagiotis Parthenis, Andreas Tsagkaropoulos
     * @param topicName the name of the topic which will be used
     * @param message the message which will be sent to this topic
     */
    public static void pushIntoKafkaTopic(String topicName, String message, String kafkaServer) {
        // kafka producer configurations
        Properties props = new Properties();
        props.put("bootstrap.servers", kafkaServer);
        props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        props.put("transactional.id", "test-transactional-id");
        Producer<String, String> producer = new KafkaProducer<>(props);

        producer.initTransactions();

        try {
            producer.beginTransaction();
            producer.send(new ProducerRecord<String, String>(topicName, "1", message)).get();
            producer.commitTransaction();

        } catch (Exception ex) {
            Logger.getAnonymousLogger().log(Level.SEVERE, "Exception while trying to send message "+ message+"at topic"+topicName+" to Kafka server "+kafkaServer);
        } finally {
            producer.close();
        }
    }




    private static void initialize_stream_definitions() {

        SiddhiStream master_local_monitoring_stream = get_master_local_monitoring_stream();
        SiddhiStream master_aggregate_monitoring_stream = get_master_aggregate_monitoring_stream();
        SiddhiStream master_combined_monitoring_stream = get_master_combined_monitoring_stream();
        synchronized (stream_definitions) {
            stream_definitions.put(master_local_monitoring_stream.getName(), master_local_monitoring_stream);
            stream_definitions.put(master_aggregate_monitoring_stream.getName(), master_aggregate_monitoring_stream);
            stream_definitions.put(master_combined_monitoring_stream.getName(), master_combined_monitoring_stream);
            stream_definitions.putAll(get_input_stream_definitions());//    +/*+ monitoring_metric_stream_definitions*/
            stream_definitions.putAll(get_unbounded_metric_definitions());
        }


    }

    private static void initialize_queries(){
        synchronized (queries) {
            queries.clear();
            for (Fragment fragment : getFragments().values()) {
                unbounded_metrics_queries.putAll(fragment.get_unbounded_metric_queries());
                queries.putAll(fragment.getMonitoring_metric_queries());
            }
            queries.putAll(unbounded_metrics_queries);
        }
    }

    private static SiddhiStream get_master_local_monitoring_stream(){

/*        String local_metrics_monitoring_stream = "@source(type = 'rabbitmq', uri = 'amqp://guest:guest@3.120.91.124:5672', exchange.name = 'presto.cloud', routing.key = '"+realtime_monitoring_data_topic+"', @map(type='json', enclosing.element=\"$\", \n" +
                "     @attributes(\n" +
                "     disk_available = \"disk_available\", \n" +
                "     cpu_sys = \"cpu_sys\", \n" +
                "     cpu_user = \"cpu_user\", \n" +
                "     cpu_io_wait = \"cpu_io_wait\", \n" +
                "     cpu_perc = \"cpu_perc\", \n" +
                "     mem_free = \"mem_free\", \n" +
                "     mem_perc = \"mem_perc\", \n" +
                "     net_rec = \"net_rec\", \n" +
                "     net_sent = \"net_sent\", \n" +
                "     packets_sent = \"packets_sent\", \n" +
                "     packets_received = \"packets_received\", \n" +
                "     throughput_rate_fps = \"throughput_rate_fps\", \n" +
                "     response_time_ms = \"response_time_ms\", \n" +
                "     latency_ms = \"latency_ms\", \n" +
                "     fragid= \"fragid\", \n" +
                "     res_inst = \"res_inst\", \n" +
                "     timestamp = \"timestamp\")\n" +
                "     ))\n" +
                "define stream "+ local_monitoring_data_stream_name +" (disk_available double, cpu_sys double, cpu_user double, cpu_io_wait double, cpu_perc double, mem_free double, mem_perc double, net_rec double, net_sent double, packets_sent double, packets_received double, throughput_rate_fps double, response_time_ms double, latency_ms double, fragid string, res_inst string, timestamp long);\n";*/

        // -------------- Local metrics monitoring stream calculation

        String local_monitoring_topic;
        local_monitoring_topic = realtime_monitoring_data_topic.replaceAll(".(#)?$",EMPTY);
        local_monitoring_topic = local_monitoring_topic +".#";


        String queue_name =broker_amqp_queue_name_prefix+System.nanoTime();
        String ssl_settings;
        if(enforce_ssl){
            File truststore_file = new File(backUpDirName+"truststore.jks");
            ssl_settings = "tls.enabled = \'true\',"+
                    "tls.truststore.Type = \'"+ truststore_type +"\',"+
                    "tls.truststore.path = \'"+truststore_file.getAbsolutePath()+"',"+
                    "tls.truststore.password = \'"+ truststore_password +"\',"+
                    "tls.version = \'TLSv"+supported_tls_version+"\',";
        }else{
            ssl_settings = EMPTY;
        }


        String local_monitoring_data_stream_input_type_details =
                    "@source(type = 'rabbitmq', " +
                            "uri = \'" + broker_uri + "\', " +
                            "exchange.name = \'" + exchange_name + "\', " +
                            "exchange.type = \'topic\', " +
                            "routing.key = \'"+local_monitoring_topic+"\', "+
                            "expiration = \'86400000\', " +
                            ssl_settings +
                            "heartbeat = \'300\', "+
                            "queue.name=\'"+queue_name+"\',"+
                            "queue.durable.enabled=\'true\',"+
                            "queue.exclusive.enabled=\'false\',"+
                            "queue.autodelete.enabled=\'false\',";



        String local_monitoring_data_stream_attribute_mapping =
                "@map(type='json', enclosing.element=\"$\", fail.on.missing.attribute=\"false\",\n"
                        +"@attributes(\n";

        String local_monitoring_data_stream_definitions = "define stream master_local_monitoring_stream (";
        for (MonitoringMetric metric : monitoring_metrics.values()){
            if (metric.is_locally_collected_metric() && !metric.is_predicted_metric()) {
                local_monitoring_data_stream_attribute_mapping = local_monitoring_data_stream_attribute_mapping + metric.getName() + "= \"" + metric.getName() + "\",\n";
                local_monitoring_data_stream_definitions = local_monitoring_data_stream_definitions + metric.getName() + " double, ";
            }
        }

        local_monitoring_data_stream_attribute_mapping = local_monitoring_data_stream_attribute_mapping +
                "fragid= \"fragid\", \n" +
                "res_inst = \"res_inst\", \n" +
                "timestamp = \"timestamp\"\n" +
                ")))\n";
        local_monitoring_data_stream_definitions = local_monitoring_data_stream_definitions +
                "fragid string, res_inst string, timestamp long);\n";

        String local_metrics_monitoring_stream = local_monitoring_data_stream_input_type_details + local_monitoring_data_stream_attribute_mapping + local_monitoring_data_stream_definitions;

        return new SiddhiStream("master_local_monitoring_stream",local_metrics_monitoring_stream);
    }

    private static SiddhiStream get_master_aggregate_monitoring_stream(){
        // -------------- Aggregated metrics monitoring stream calculation

        String monitoring_topic;
        monitoring_topic = realtime_monitoring_data_topic.replaceAll(".(#)?$",EMPTY);
        monitoring_topic = monitoring_topic +".#";

        String fragment_queue_name =broker_amqp_queue_name_prefix+System.nanoTime();

        String ssl_settings;
        if(enforce_ssl){
            File truststore_file = new File(backUpDirName+"truststore.jks");
            ssl_settings = "tls.enabled = \'true\',"+
                    "tls.truststore.Type = \'"+ truststore_type +"\',"+
                    "tls.truststore.path = \'"+truststore_file.getAbsolutePath()+"',"+
                    "tls.truststore.password = \'"+ truststore_password +"\',"+
                    "tls.version = \'TLSv"+supported_tls_version+"\',";
        }else{
            ssl_settings = EMPTY;
        }

        String aggregate_monitoring_data_stream_input_type_details =
                "@source(type = 'rabbitmq', " +
                        "uri = \'" + broker_uri + "\', " +
                        "exchange.name = \'" + exchange_name + "\', " +
                        "exchange.type = \'topic\', " +
                        "expiration = \'86400000\', " +
                        "routing.key = \'"+monitoring_topic+"\', "+
                        ssl_settings +
                        "heartbeat = \'300\', "+
                        "queue.name=\'"+fragment_queue_name+"\',"+
                        "queue.durable.enabled=\'true\',"+
                        "queue.exclusive.enabled=\'false\',"+
                        "queue.autodelete.enabled=\'false\',";



        String aggregate_monitoring_data_stream_attribute_mapping =
                "@map(type='json', enclosing.element=\"$.loadBalancerMonitoringModels\", fail.on.missing.attribute=\"false\",\n"
                        +"@attributes(\n";

        String aggregate_monitoring_data_stream_definitions = "define stream master_aggregate_monitoring_stream (";
        for (MonitoringMetric metric : monitoring_metrics.values()){
            if (metric.is_metric_collected_in_aggregate()) {
                aggregate_monitoring_data_stream_attribute_mapping = aggregate_monitoring_data_stream_attribute_mapping + metric.getName() + "= \"" + metric.getName() + "\",\n";
                aggregate_monitoring_data_stream_definitions = aggregate_monitoring_data_stream_definitions + metric.getName() + " double, ";
            }
        }

        aggregate_monitoring_data_stream_attribute_mapping = aggregate_monitoring_data_stream_attribute_mapping +
                "fragid= \"loadbalancer_for_fragid\"\n" +
                ")))\n";
        aggregate_monitoring_data_stream_definitions = aggregate_monitoring_data_stream_definitions +
                "fragid string);\n";

        String aggregated_metrics_monitoring_stream = aggregate_monitoring_data_stream_input_type_details + aggregate_monitoring_data_stream_attribute_mapping + aggregate_monitoring_data_stream_definitions;

        return new SiddhiStream("master_aggregated_monitoring_stream",aggregated_metrics_monitoring_stream);
    }

    private static SiddhiStream get_master_combined_monitoring_stream(){

        // -------------- Combined monitoring stream calculation


        String combined_metrics_monitoring_stream = "define stream master_combined_monitoring_stream (";

        for (MonitoringMetric metric : monitoring_metrics.values()){
            combined_metrics_monitoring_stream = combined_metrics_monitoring_stream + metric.getName() + " double, ";
        }
        combined_metrics_monitoring_stream = combined_metrics_monitoring_stream + "fragid string, res_inst string, timestamp long);\n";

        return new SiddhiStream("master_combined_monitoring_stream",combined_metrics_monitoring_stream);
    }
}
