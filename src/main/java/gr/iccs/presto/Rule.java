package gr.iccs.presto;

import gr.iccs.presto.utility_classes.SubRule;

import java.util.ArrayList;
import java.util.UUID;

import static gr.iccs.presto.Globals.EMPTY;

public class Rule {
    private ArrayList<SubRule> subrules;
    private String scalability_action;
    private String fragment_id_to_scale;
    private String fragment_name_to_scale;

    public Rule (ArrayList<SubRule> subrules, String scalability_action){
        this.subrules = subrules;
        this.scalability_action = scalability_action;
        this.fragment_id_to_scale =  UUID.randomUUID().toString().replaceAll("-",EMPTY).substring(0,10);
        this.fragment_name_to_scale = "random_fragment_"+ UUID.randomUUID().toString().replaceAll("-",EMPTY).substring(0,5);
    }

    public ArrayList<SubRule> getSubrules() {
        return subrules;
    }

    public void setSubrules(ArrayList<SubRule> subrules) {
        this.subrules = subrules;
    }

    public String getScalability_action() {
        return scalability_action;
    }

    public void setScalability_action(String scalability_action) {
        this.scalability_action = scalability_action;
    }

    public String getFragment_id_to_scale() {
        return fragment_id_to_scale;
    }

    public void setFragment_id_to_scale(String fragment_id_to_scale) {
        this.fragment_id_to_scale = fragment_id_to_scale;
    }

    public String getFragment_name_to_scale() {
        return fragment_name_to_scale;
    }

    public void setFragment_name_to_scale(String fragment_name_to_scale) {
        this.fragment_name_to_scale = fragment_name_to_scale;
    }
}
