package gr.iccs.presto;

import gr.iccs.presto.utility_classes.MonitoringMetric;

import java.util.concurrent.ConcurrentHashMap;

public class Globals {

    public static Boolean enforce_ssl = false; // This variable is updated according to the port that the broker listens, which appears in the broker uri variable of the configuration file. If the entered port is 5671, then this variable is set to true, else it remains false.
    public static String truststore_type = "PKCS12"; //PKCS12 or JKS. This reflects the password which is retrieved from the Vault repository
    public static String supported_tls_version = "1.2";
    public static Boolean INERTIA_TIME_RESPECTED_SCALABILITY_RULE = true; //Whether or not the inertia declared in a scalability rule should be respected
    public static final boolean aggregate_metrics_predicted = false;
    public static final boolean send_cloud_device_context = false;

    //String constants
    public static String predicted_monitoring_data_stream_name_prefix = "predictedStream_";
    public static String local_monitoring_data_stream_name_prefix = "localStream_";
    public static String edge_local_monitoring_data_stream_name_prefix = "edgeLocalStream_";
    public static String aggregate_monitoring_data_stream_name_prefix = "aggregatedStream_";
    public static String combined_metrics_monitoring_stream_prefix = "combinedMonitoringStream_";
    public static String predicted_monitoring_metrics_prefix = "prediction_";
    public static String context_output_stream_name = "contextStream";
    public static String broker_amqp_queue_name_prefix = "amqp-subscription-siddhi";
    public static String EMPTY = "";

    //SDM properties
    public static String truststore_password;
    public static String broker_ip_address;
    public static String broker_uri;
    public static String broker_username;
    public static String broker_password;
    public static String exchange_name;
    public static String vault_address;
    public static String vault_token;
    public static String mca_fragment_data_topic;
    public static String undeployment_topic;
    public static String realtime_monitoring_data_topic;
    public static String predicted_monitoring_data_topic;
    public static String situations_detected_topic;
    public static String ui_information_topic;
    public static String ui_situations_detected_topic;
    public static int number_of_severity_zones;
    public static int number_of_events_for_context;
    public static String scalability_rules_manipulation_topic;
    public static int monitoring_events_frequency_msec;
    public static int unbounded_metric_samples;
    public static String keyStorePath;
    public static char [] keyPassphrase;
    public static String trustStorePath;
    public static char [] trustPassphrase;

    //SDM data structures
    public static final ConcurrentHashMap<String, MonitoringMetric> monitoring_metrics = new ConcurrentHashMap<>();
    public static final ConcurrentHashMap<String, MonitoringMetric> predicted_monitoring_metrics = new ConcurrentHashMap<>();
    public static final ConcurrentHashMap <String,MonitoringMetric> common_monitoring_metrics = new ConcurrentHashMap<>();
    public static final ConcurrentHashMap <String,MonitoringMetric> unbounded_monitoring_metrics = new ConcurrentHashMap<>();

}
