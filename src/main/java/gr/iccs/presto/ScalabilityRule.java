package gr.iccs.presto;

import gr.iccs.presto.utility_classes.Fragment;
import gr.iccs.presto.utility_classes.MonitoringMetric;
import gr.iccs.presto.utility_classes.Operand;
import gr.iccs.presto.utility_classes.SubRule;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import static gr.iccs.presto.Globals.*;
import static gr.iccs.presto.SiddhiQuery.query_counter;
import static gr.iccs.presto.TransformationsEngine.*;
import static gr.iccs.presto.utility_classes.Fragment.getFragments;
import static java.util.logging.Level.INFO;
import static java.util.logging.Level.SEVERE;


enum SituationAction {scale_in, scale_out, info, pushIntoKafkaTopic};

public class ScalabilityRule {
    private String json_representation;
    private SiddhiQuery aggregator_siddhi_query;
    private SiddhiQuery rule_siddhi_query;
    private ArrayList<SubRule> sub_rules = new ArrayList<>();
    private LinkedHashMap<String,String> metric_names_map = new LinkedHashMap<>(); //old and new metric names are connected with this hashmap
    private SiddhiStream siddhi_aggregator_stream, rule_output_stream;
    private String graphHexID;
    private String graphInstanceHexID;
    private ArrayList<String> monitored_fragment_names;
    private String scaled_component_HexID; // the HexID of the component which will scale up or down
    private String scaled_component_name;
    private String hex_id;
    private int inertia_time;
    private SituationAction situation_action;
    private String context_on_action;
    private String callback_url;
    private String kafka_url;
    private String kafka_port;
    private String kafka_topic;

    public ScalabilityRule(String json_representation) {
        this.json_representation = json_representation;

        try {
            JSONParser parser = new JSONParser();
            JSONObject master_json_object;

            master_json_object = (JSONObject) parser.parse(json_representation);
            callback_url = master_json_object.get("callbackURL").toString();

            JSONArray rule_metrics = (JSONArray) master_json_object.get("expressionModelList");
            hex_id = master_json_object.get("policyHexID").toString();
            int query_time_window = Integer.parseInt(master_json_object.get("policyPeriod").toString());
            inertia_time = Integer.parseInt(master_json_object.get("inertialPeriod").toString());
            ArrayList<JSONObject> scalability_rule_sub_rules_json =new ArrayList<>();

            for (Object current_rule: rule_metrics){
                JSONObject rule = (JSONObject) current_rule;
                //System.out.println(rule.get("metric"));
                //System.out.println(rule.get("componentName"));
                //System.out.println(rule.get("operand"));
                //System.out.println(rule.get("threshold"));
                scalability_rule_sub_rules_json.add(rule);
            }

            sub_rules = get_sub_rules(scalability_rule_sub_rules_json);
            monitored_fragment_names = new ArrayList<>(sub_rules.stream()
                            .map(SubRule::getMonitored_fragment_name)
                            .collect(Collectors.toList()));



            scaled_component_HexID =((JSONObject)((JSONArray) master_json_object.get("actionModelsList")).get(0)).get("componentHexID").toString();
            scaled_component_name =((JSONObject)((JSONArray) master_json_object.get("actionModelsList")).get(0)).get("componentName").toString();
            context_on_action = ((JSONObject)((JSONArray) master_json_object.get("actionModelsList")).get(0)).get("context").toString();
            kafka_port = ((JSONObject)((JSONArray) master_json_object.get("actionModelsList")).get(0)).get("topicPort").toString();
            kafka_topic = ((JSONObject)((JSONArray) master_json_object.get("actionModelsList")).get(0)).get("topicName").toString();
            kafka_url= ((JSONObject)((JSONArray) master_json_object.get("actionModelsList")).get(0)).get("topicUrl").toString();
            String json_scalability_action = ((JSONObject)((JSONArray) master_json_object.get("actionModelsList")).get(0)).get("ruleAction").toString();
            if (json_scalability_action.equalsIgnoreCase("scaleOut")) {
                situation_action = SituationAction.scale_out;
            }else if (json_scalability_action.equalsIgnoreCase("scaleIn")) {
                situation_action = SituationAction.scale_in;
            }else if (json_scalability_action.equalsIgnoreCase("info")) {
                situation_action = SituationAction.info;
            }else if (json_scalability_action.equalsIgnoreCase("pushIntoKafkaTopic")){
                situation_action = SituationAction.pushIntoKafkaTopic;
            }else{
                Logger.getAnonymousLogger().log(SEVERE,"Unknown situation action type "+json_scalability_action+" was requested");
            }

            ArrayList<String> new_fragment_names = create_any_new_fragments_from_sub_rules();
            insert_required_new_monitoring_metrics(scalability_rule_sub_rules_json);
            siddhi_aggregator_stream = get_aggregator_stream();
            aggregator_siddhi_query = get_aggregator_query();

            rule_output_stream = get_siddhi_query_output_stream(); //creates a new stream into which all metrics are inserted when a situation is detected

            rule_siddhi_query = get_rule_siddhi_query(aggregator_siddhi_query,sub_rules,query_time_window, rule_output_stream.getName());//new SiddhiQuery(sub_rules,fragment_names,query_time_window);


            graphHexID = master_json_object.get("graphHexID").toString();
            graphInstanceHexID = master_json_object.get("graphInstanceHexID").toString();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private static ArrayList<SubRule> get_sub_rules(ArrayList<JSONObject> scalability_rule_sub_rules) {

        ArrayList<SubRule> sub_rules = new ArrayList<>();
        for (JSONObject sub_rule:scalability_rule_sub_rules){
            sub_rules.add(new SubRule(sub_rule.get("function").toString(),sub_rule.get("metric").toString(), Operand.valueOf((String)sub_rule.get("operand")),sub_rule.get("componentHexID").toString(),sub_rule.get("componentName").toString(),Double.parseDouble(sub_rule.get("threshold").toString())));
        }
        return sub_rules;
    }

    public static SiddhiStream create_monitoring_metric_stream(String metric_name){
        String monitoring_metric_stream_name = metric_name+"_monitoring_stream";
        String definition = "@config(async = 'true') define stream "+monitoring_metric_stream_name+" ("+metric_name+" double);\n";
        SiddhiStream monitoring_metric_stream = new SiddhiStream(monitoring_metric_stream_name,definition);
        monitoring_metric_stream.add_metric(metric_name);
        return monitoring_metric_stream;
    }

    /**
     * This method returns a SiddhiStream object, which corresponds to the output stream of a Siddhi query
     * @return A SiddhiStream object with the description of the stream which will publish the output of the Siddhi query
     */
    public SiddhiStream get_siddhi_query_output_stream(){

        //define stream outputStream (avg_cpu double, avg_memory double);
        String siddhi_stream_name = "siddhi_query_output_stream_"+hex_id;
        String siddhi_stream_definition = "define stream "+siddhi_stream_name;

        ArrayList<String> alias_metric_names = new ArrayList<>(); //These are the names of the new variables which will be inserted in the output streams of the query. They factor in any aggregation operands which might be used, e.g avg or sum.


        for (int counter=0; counter<sub_rules.size();counter++) {
            Fragment monitored_fragment = getFragments().get(sub_rules.get(counter).getMonitored_fragment_name());
            String metric_name = sub_rules.get(counter).getMetric_name();
            String source_stream_name;
            if (monitored_fragment.getMonitoring_metrics().get(metric_name).is_predicted_metric()){
                source_stream_name = monitored_fragment.getPredicted_monitoring_data_stream_name();
            } else if (monitored_fragment.getMonitoring_metrics().get(metric_name).is_locally_collected_metric()){
                source_stream_name = monitored_fragment.getLocal_monitoring_data_stream_name();
            } else if (monitored_fragment.getMonitoring_metrics().get(metric_name).is_metric_collected_in_aggregate()){
                source_stream_name = monitored_fragment.getAggregate_monitoring_data_stream_name();
            }else {//should never enter here, but inserted as a catch-all
                source_stream_name = monitored_fragment.getCombined_metrics_monitoring_stream_name();
            }
            String distinctive_metric_name = metric_names_map.get(source_stream_name+"."+metric_name);
            String aggregation_operand = get_aggregation_function(sub_rules.get(counter));
            String new_metric_name = get_aggregation_alias_of_metric(aggregation_operand,distinctive_metric_name);
            alias_metric_names.add(new_metric_name); //the output stream name taking into account the aggregation operator

        }



        /*
        for (SubRule sub_rule : sub_rules) {

            if (!get_aggregation_function(sub_rule).equals(EMPTY)){
                alias_metric_names.add(get_aggregation_function(sub_rule)+"_"+sub_rule.getMetric_name());
            }else {
                alias_metric_names.add(sub_rule.getMetric_name());
            }

        }
        */

        siddhi_stream_definition = siddhi_stream_definition + " (";

        for (int i=0; i<alias_metric_names.size();i++) {
            String alias_metric_name = alias_metric_names.get(i)+" double";
            if (i < alias_metric_names.size() - 1) {
                siddhi_stream_definition = siddhi_stream_definition + alias_metric_name + ", ";
            } else{
                siddhi_stream_definition = siddhi_stream_definition + alias_metric_name;
            }
        }
        siddhi_stream_definition = siddhi_stream_definition + ", "/*+"fragid string,"*/+" res_inst string);\n";

        return new SiddhiStream(siddhi_stream_name,siddhi_stream_definition,alias_metric_names);
    }



    private static SiddhiQuery get_rule_siddhi_query(SiddhiQuery aggregator_siddhi_query, ArrayList<SubRule> sub_rules, int query_time_window, String output_stream_name) {
        HashMap<String,String> metric_names_map = aggregator_siddhi_query.getMetric_names_map();

        StringBuilder final_select_statement_string_builder = new StringBuilder("select ");

        for (int counter=0; counter<sub_rules.size();counter++){
            Fragment monitored_fragment = getFragments().get(sub_rules.get(counter).getMonitored_fragment_name());
            String metric_name = sub_rules.get(counter).getMetric_name();
            String source_stream_name;
            if (monitored_fragment.getMonitoring_metrics().get(metric_name).is_predicted_metric()){
                source_stream_name = monitored_fragment.getPredicted_monitoring_data_stream_name();
            }else if (monitored_fragment.getMonitoring_metrics().get(metric_name).is_locally_collected_metric()){
                source_stream_name = monitored_fragment.getLocal_monitoring_data_stream_name();
            } else if (monitored_fragment.getMonitoring_metrics().get(metric_name).is_metric_collected_in_aggregate()){
                source_stream_name = monitored_fragment.getAggregate_monitoring_data_stream_name();
            }else {//should never enter here, but inserted as a catch-all
                source_stream_name = monitored_fragment.getCombined_metrics_monitoring_stream_name();
            }
            String distinctive_metric_name = metric_names_map.get(source_stream_name+"."+metric_name);
            String aggregation_operand = get_aggregation_function(sub_rules.get(counter));

            if (counter<sub_rules.size()-1) {
                if (!aggregation_operand.equals(EMPTY)) {
                    String new_metric_name = get_aggregation_alias_of_metric(aggregation_operand,distinctive_metric_name);
                    final_select_statement_string_builder.append(aggregation_operand + "(" + distinctive_metric_name + ")" + " as " + new_metric_name +", ");
                    metric_names_map.put(source_stream_name+"."+metric_name,new_metric_name); //replacing the distinctive metric name
                }else{
                    final_select_statement_string_builder.append(aggregation_operand + distinctive_metric_name + ", ");
                }
            }else{
                if (!aggregation_operand.equals(EMPTY)) {
                    String new_metric_name = get_aggregation_alias_of_metric(aggregation_operand,distinctive_metric_name);
                    final_select_statement_string_builder.append(aggregation_operand + "(" + distinctive_metric_name + ")" + " as " + new_metric_name);
                    metric_names_map.put(source_stream_name+"."+metric_name,new_metric_name); //replacing the distinctive metric name
                }else{
                    final_select_statement_string_builder.append(aggregation_operand + distinctive_metric_name);
                }
            }
        }
        String final_select_statement = final_select_statement_string_builder.toString();


        StringBuilder having_statement_string_builder = new StringBuilder("having ");

        for (int counter=0; counter<sub_rules.size();counter++){


            Fragment monitored_fragment = getFragments().get(sub_rules.get(counter).getMonitored_fragment_name());
            String metric_name = sub_rules.get(counter).getMetric_name();
            String source_stream_name;
            if (monitored_fragment.getMonitoring_metrics().get(metric_name).is_predicted_metric()){
                source_stream_name = monitored_fragment.getPredicted_monitoring_data_stream_name();
            }else if (monitored_fragment.getMonitoring_metrics().get(metric_name).is_locally_collected_metric()){
                source_stream_name = monitored_fragment.getLocal_monitoring_data_stream_name();
            } else if (monitored_fragment.getMonitoring_metrics().get(metric_name).is_metric_collected_in_aggregate()){
                source_stream_name = monitored_fragment.getAggregate_monitoring_data_stream_name();
            }else {//should never enter here, but inserted as a catch-all
                source_stream_name = monitored_fragment.getCombined_metrics_monitoring_stream_name();
            }

            String conditional_operand = get_conditional_operand(sub_rules.get(counter).getOperand());
            Double threshold = sub_rules.get(counter).getThreshold();
            String new_metric_name = metric_names_map.get(source_stream_name+"."+metric_name);


            if (counter<sub_rules.size()-1) {
                having_statement_string_builder.append(new_metric_name + conditional_operand + threshold + " and ");
            }else{
                having_statement_string_builder.append(new_metric_name + conditional_operand + threshold);
            }
        }
        String having_statement = having_statement_string_builder.toString();

        String final_output_stream_name = output_stream_name/*+"_"+query_counter*/+";\n\n";

        String definition =
                "from "+aggregator_siddhi_query.getOutput_stream_name() + "#window.time(" + query_time_window + " sec )" + "\n" +
                        final_select_statement+", res_inst\n" +
                        having_statement+ "\n" +
                        "insert into " + final_output_stream_name;

        query_counter++;
        return new SiddhiQuery(definition,metric_names_map,final_output_stream_name);
    }


    /**
     * The purpose of this method is to create any new required Siddhi queries for each Fragment, as indicated by the attributes of a sub-rule
     * @param scalability_rule_id
     * @param sub_rules
     * @param time_window
     * @return
     */
    private static ArrayList<SiddhiQuery> create_any_new_siddhi_queries_from_sub_rules(String scalability_rule_id, ArrayList<SubRule> sub_rules,int time_window) {

        ArrayList <SiddhiQuery> sub_rules_siddhi_queries = new ArrayList<>();
        HashMap<String, ArrayList<SubRule>>fragment_names_sub_rules = new HashMap<>();
        List<String> fragment_names = sub_rules.stream().map(sub_rule -> {
            String monitored_fragment_name = sub_rule.getMonitored_fragment_name();
            fragment_names_sub_rules.put(monitored_fragment_name,new ArrayList<>());
            return monitored_fragment_name;
        }).collect(Collectors.toList());

        sub_rules.stream().forEach(sub_rule -> {
            String monitored_fragment_name = sub_rule.getMonitored_fragment_name();
            fragment_names_sub_rules.get(monitored_fragment_name).add(sub_rule);
        });

        fragment_names_sub_rules.entrySet().stream().forEach(fragment_name_sub_rules_entry->{
            String fragment_name = fragment_name_sub_rules_entry.getKey();
            ArrayList<SubRule> fragment_sub_rules = fragment_name_sub_rules_entry.getValue();

            SiddhiQuery new_siddhi_query = new SiddhiQuery(fragment_sub_rules,getFragments().get(fragment_name),time_window); //We assume that the relevant fragment has been created before, and that the necessary monitoring metrics have been added to its description.
            sub_rules_siddhi_queries.add(new_siddhi_query);
        });

        if (sub_rules_siddhi_queries.size()>0) {
            //SiddhiQuery master_query = new SiddhiQuery(sub_rules_siddhi_queries, time_window);  //add siddhi query which retrieves input from multiple streams (each getting input concerning a particular fragment) and outputs to a master output stream
        }
        return sub_rules_siddhi_queries;
    }

    private ArrayList<String> create_any_new_fragments_from_sub_rules() {

        ArrayList<String> new_fragment_names = new ArrayList<>();
        //for (SubRule sub_rule: sub_rules)

            sub_rules.forEach(sub_rule -> {
                MonitoringMetric new_monitoring_metric;
                if (monitoring_metrics.containsKey(sub_rule.getMetric_name())){
                    new_monitoring_metric = monitoring_metrics.get(sub_rule.getMetric_name());
                }else{
                    new_monitoring_metric = new MonitoringMetric(sub_rule.getMetric_name(),"local",Double.NaN,Double.NaN);
                    if (sub_rule.getMetric_name().startsWith("prediction_")){
                        new_monitoring_metric.set_predicted_metric(true);
                    }
                }
                if (getFragments().containsKey(sub_rule.getMonitored_fragment_name())){
                    Fragment fragment = getFragments().get(sub_rule.getMonitored_fragment_name());
                    common_monitoring_metrics.values().forEach(monitoring_metric -> {
                        fragment.getMonitoring_metrics().put(monitoring_metric.getName(),monitoring_metric);
                    });
                    fragment.add_monitoring_metric(new_monitoring_metric);
                    fragment.update_queries();
                }else{
                    Fragment fragment = new Fragment(sub_rule.getMonitored_fragment_name(),common_monitoring_metrics);
                    common_monitoring_metrics.values().forEach(monitoring_metric -> {
                        fragment.getMonitoring_metrics().put(monitoring_metric.getName(),monitoring_metric);
                    });
                    fragment.add_monitoring_metric(new_monitoring_metric);
                    new_fragment_names.add(sub_rule.getMonitored_fragment_name());
                    fragment.update_queries();
                }
            });

        if (getFragments().containsKey(scaled_component_name)){
            Fragment fragment = getFragments().get(scaled_component_name);
            common_monitoring_metrics.values().forEach(monitoring_metric -> {
                fragment.getMonitoring_metrics().put(monitoring_metric.getName(),monitoring_metric);
            });
            fragment.update_queries();
        }else{
            Fragment fragment = new Fragment(scaled_component_name,common_monitoring_metrics);
            common_monitoring_metrics.values().forEach(monitoring_metric -> {
                fragment.getMonitoring_metrics().put(monitoring_metric.getName(),monitoring_metric);
            });
            new_fragment_names.add(scaled_component_name);
            fragment.update_queries();
        }

        return new_fragment_names;
    }

    private static boolean insert_required_new_monitoring_metrics(ArrayList<JSONObject> scalability_rule_sub_rules) {
        boolean new_monitoring_metric_required=false;

        for (JSONObject json_object: scalability_rule_sub_rules){
            String fragment_name = (String) json_object.get("componentName");
            String new_monitoring_metric_name = String.valueOf(json_object.get("metric"));
            if (new_monitoring_metric_name!=null && !new_monitoring_metric_name.equals(EMPTY)) {

                MonitoringMetric new_monitoring_metric;
                if (monitoring_metrics.containsKey(new_monitoring_metric_name)){
                    new_monitoring_metric = monitoring_metrics.get(new_monitoring_metric_name);
                }else{
                    new_monitoring_metric = new MonitoringMetric(new_monitoring_metric_name,"local",Double.NaN,Double.NaN);
                    if (new_monitoring_metric_name.startsWith("prediction_")){
                        new_monitoring_metric.set_predicted_metric(true);
                    }
                    monitoring_metrics.put(new_monitoring_metric_name, new_monitoring_metric);
                    Logger.getAnonymousLogger().log(INFO,"A new monitoring metric, named \""+new_monitoring_metric_name+"\" has been defined implicitly, through a rule definition");
                    new_monitoring_metric_required = true;
                }
                getFragments().get(fragment_name).add_monitoring_metric(new_monitoring_metric);
            }
        }
        return new_monitoring_metric_required;
    }


    private SiddhiStream get_aggregator_stream(){

        String aggregator_stream_name = "aggregator_stream_"+query_counter;
        String aggregator_stream_definition_body = EMPTY;


        for (int counter=0; counter<sub_rules.size();counter++) {
            Fragment monitored_fragment = getFragments().get(sub_rules.get(counter).getMonitored_fragment_name());
            String metric_name = sub_rules.get(counter).getMetric_name();
            String source_stream_name;

            if (monitored_fragment.getMonitoring_metrics().get(metric_name).is_predicted_metric()) {
                source_stream_name = monitored_fragment.getPredicted_monitoring_data_stream_name();
            }else if (monitored_fragment.getMonitoring_metrics().get(metric_name).is_locally_collected_metric()) {
                source_stream_name = monitored_fragment.getLocal_monitoring_data_stream_name();
            }else if (monitored_fragment.getMonitoring_metrics().get(metric_name).is_metric_collected_in_aggregate()){
                source_stream_name = monitored_fragment.getAggregate_monitoring_data_stream_name();
            }
            else{ //should never enter, existing as a catch-all
                source_stream_name =monitored_fragment.getCombined_metrics_monitoring_stream_name();
            }

            String distinctive_metric_name = metric_name+"_"+sub_rules.get(counter).getMonitored_fragment_id(); //in case that a metric is monitored from two different fragments simultaneously
            metric_names_map.put(source_stream_name+"."+metric_name,distinctive_metric_name);
            aggregator_stream_definition_body = aggregator_stream_definition_body+distinctive_metric_name+", ";

        }


        String aggregator_stream_definition = "define stream "+aggregator_stream_name+" ("+aggregator_stream_definition_body+"res_inst string)";


        aggregator_stream_name = "aggregator_stream_"+query_counter;


        SiddhiStream aggregator_stream = new SiddhiStream(aggregator_stream_name,aggregator_stream_definition);
        return aggregator_stream;
    }

    private SiddhiQuery get_aggregator_query() {

        SiddhiQuery aggregator_query;

        HashSet<String> input_stream_names_used = new HashSet<>();
        StringBuilder select_statement_string_builder = new StringBuilder("select ");

        for (int counter=0; counter<sub_rules.size();counter++){
            Fragment monitored_fragment = getFragments().get(sub_rules.get(counter).getMonitored_fragment_name());
            String metric_name = sub_rules.get(counter).getMetric_name();
            String source_stream_name;

            if (monitored_fragment.getMonitoring_metrics().get(metric_name).is_predicted_metric()){
                source_stream_name = monitored_fragment.getPredicted_monitoring_data_stream_name();
            }else if (monitored_fragment.getMonitoring_metrics().get(metric_name).is_locally_collected_metric()) {
                source_stream_name = monitored_fragment.getLocal_monitoring_data_stream_name();
            }else if (monitored_fragment.getMonitoring_metrics().get(metric_name).is_metric_collected_in_aggregate()){
                source_stream_name = monitored_fragment.getAggregate_monitoring_data_stream_name();
            }
            else{ //should never enter, existing as a catch-all
                source_stream_name =monitored_fragment.getCombined_metrics_monitoring_stream_name();
            }

            input_stream_names_used.add(source_stream_name);
            String distinctive_metric_name = metric_names_map.get(source_stream_name+"."+metric_name);


            if (counter ==0){ //add a res_inst attribute, which has a meaning in the case that a scalability rule for a particular fragment without metric aggregations is created
                select_statement_string_builder.append(source_stream_name+".res_inst as res_inst, ");
            }

            if (counter<sub_rules.size()-1) {
                select_statement_string_builder.append(source_stream_name+"."+metric_name + " as "+distinctive_metric_name+", ");
            }else{
                select_statement_string_builder.append(source_stream_name+"."+metric_name + " as "+distinctive_metric_name);
            }
        }
        String select_statement = select_statement_string_builder.toString();

        StringBuilder from_statement_string_builder = new StringBuilder("from every ");


        ArrayList input_stream_names_used_arraylist = new ArrayList(Arrays.asList(input_stream_names_used.toArray()));
        for (int counter = 0; counter<monitored_fragment_names.size() && counter<input_stream_names_used_arraylist.size() ;counter++) {
            if ((counter<monitored_fragment_names.size()-1)&&(counter<input_stream_names_used_arraylist.size()-1)) {
                from_statement_string_builder.append(input_stream_names_used_arraylist.get(counter)  + ", ");
            }else{
                from_statement_string_builder.append(input_stream_names_used_arraylist.get(counter));
            }
        }
        String from_statement = from_statement_string_builder.toString();


        String aggregator_query_definition = from_statement+"\n"+ select_statement+"\n"+ "insert into "+siddhi_aggregator_stream.getName()+"; \n\n";

        aggregator_query = new SiddhiQuery(aggregator_query_definition,metric_names_map, siddhi_aggregator_stream.getName());
        return aggregator_query;

    }


    public String getHex_id() {
        return hex_id;
    }

    public void setHex_id(String hex_id) {
        this.hex_id = hex_id;
    }

    public String getCallback_url() {
        return callback_url;
    }

    public void setCallback_url(String callback_url) {
        this.callback_url = callback_url;
    }

    public SituationAction getSituation_action() {
        return situation_action;
    }

    public void setSituation_action(SituationAction situation_action) {
        this.situation_action = situation_action;
    }

    public ArrayList<SubRule> getSub_rules() {
        return sub_rules;
    }

    public void setSub_rules(ArrayList<SubRule> sub_rules) {
        this.sub_rules = sub_rules;
    }

    public SiddhiQuery getRule_siddhi_query() {
        return rule_siddhi_query;
    }

    public void setRule_siddhi_query(SiddhiQuery rule_siddhi_query) {
        this.rule_siddhi_query= rule_siddhi_query;
    }

    public String getJson_representation() {
        return json_representation;
    }

    public void setJson_representation(String json_representation) {
        this.json_representation = json_representation;
    }

    public String getGraphHexID() {
        return graphHexID;
    }

    public void setGraphHexID(String graphHexID) {
        this.graphHexID = graphHexID;
    }

    public String getGraphInstanceHexID() {
        return graphInstanceHexID;
    }

    public void setGraphInstanceHexID(String graphInstanceHexID) {
        this.graphInstanceHexID = graphInstanceHexID;
    }

    public ArrayList<String> getMonitored_fragment_names() {
        return monitored_fragment_names;
    }

    public void setMonitored_fragment_names(ArrayList<String> monitored_fragment_names) {
        this.monitored_fragment_names = monitored_fragment_names;
    }



    public String getScaled_component_HexID() {
        return scaled_component_HexID;
    }

    public void setScaled_component_HexID(String scaled_component_HexID) {
        this.scaled_component_HexID = scaled_component_HexID;
    }

    public String getScaled_component_name() {
        return scaled_component_name;
    }

    public void setScaled_component_name(String scaled_component_name) {
        this.scaled_component_name = scaled_component_name;
    }

    public String getContext_on_action() {
        return context_on_action;
    }

    public void setContext_on_action(String context_on_action) {
        this.context_on_action = context_on_action;
    }

    public String getKafka_url() {
        return kafka_url;
    }

    public void setKafka_url(String kafka_url) {
        this.kafka_url = kafka_url;
    }

    public String getKafka_port() {
        return kafka_port;
    }

    public void setKafka_port(String kafka_port) {
        this.kafka_port = kafka_port;
    }

    public String getKafka_topic() {
        return kafka_topic;
    }

    public void setKafka_topic(String kafka_topic) {
        this.kafka_topic = kafka_topic;
    }

    public SiddhiStream getRule_output_stream() {
        return rule_output_stream;
    }

    public void setRule_output_stream(SiddhiStream rule_output_stream) {
        this.rule_output_stream = rule_output_stream;
    }

    public SiddhiQuery getAggregator_siddhi_query() {
        return aggregator_siddhi_query;
    }

    public void setAggregator_siddhi_query(SiddhiQuery aggregator_siddhi_query) {
        this.aggregator_siddhi_query= aggregator_siddhi_query;
    }

    public int getInertia_time() {
        return inertia_time;
    }

    public void setInertia_time(int inertia_time) {
        this.inertia_time = inertia_time;
    }
}
