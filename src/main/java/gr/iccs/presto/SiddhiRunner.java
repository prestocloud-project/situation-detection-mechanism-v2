package gr.iccs.presto;

import io.siddhi.core.SiddhiAppRuntime;
import io.siddhi.core.SiddhiManager;

import java.util.Map;
import java.util.logging.Logger;

import static gr.iccs.presto.App.TRUSTSTORE_AVAILABLE;
import static gr.iccs.presto.Globals.EMPTY;
import static gr.iccs.presto.Globals.enforce_ssl;
import static java.util.logging.Level.INFO;
import static java.util.logging.Level.SEVERE;

public class SiddhiRunner extends Thread implements Runnable {

    private boolean has_started_execution = false;
    private String constructed_siddhi_file;
    private SiddhiManager siddhi_manager;
    private SiddhiAppRuntime siddhi_app_runtime;

    public SiddhiRunner(Map<String,SiddhiStream> stream_definitions, Map<String,SiddhiQuery> queries){
        StringBuilder stream_definitions_string_builder = new StringBuilder();
        StringBuilder queries_string_builder = new StringBuilder();
        queries.values().forEach(query->{
            queries_string_builder.append(query.getDefinition());
        });
        stream_definitions.values().forEach(stream_definition -> {
           stream_definitions_string_builder.append(stream_definition.getDefinition());
        });

        constructed_siddhi_file = stream_definitions_string_builder.toString() + queries_string_builder.toString();
        Logger.getAnonymousLogger().log(INFO,"The constructed Siddhi execution plan is:\n"+constructed_siddhi_file);
    }
    public SiddhiRunner(){
        constructed_siddhi_file = EMPTY;
    }

    public void run() {
        synchronized (this) {
            try {
                siddhi_manager = new SiddhiManager();
                siddhi_app_runtime = siddhi_manager.createSiddhiAppRuntime(constructed_siddhi_file);
                synchronized (TRUSTSTORE_AVAILABLE) {
                    if (!TRUSTSTORE_AVAILABLE.getValue() && enforce_ssl){
                        TRUSTSTORE_AVAILABLE.wait();
                    }
                }
                siddhi_app_runtime.start();
                has_started_execution = true;
                this.setPriority(1);
                this.notifyAll();
            }catch (Exception e){
                Logger.getAnonymousLogger().log(SEVERE,"The constructed siddhi execution file is: \n"+constructed_siddhi_file);
                e.printStackTrace();
            }
        }
    }

    public boolean has_started_execution() {
        return has_started_execution;
    }

    public void set_execution_started() {
        this.has_started_execution = true;
    }

    public void setSiddhi_manager(SiddhiManager siddhi_manager) {
        this.siddhi_manager = siddhi_manager;
    }

    public void setSiddhi_app_runtime(SiddhiAppRuntime siddhi_app_runtime) {
        this.siddhi_app_runtime = siddhi_app_runtime;
    }

    public SiddhiManager getSiddhi_manager() {
        return siddhi_manager;
    }

    public SiddhiAppRuntime getSiddhi_app_runtime() {
        return siddhi_app_runtime;
    }
}
