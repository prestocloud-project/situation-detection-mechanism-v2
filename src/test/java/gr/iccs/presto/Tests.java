package gr.iccs.presto;

import consumer.AmqpConsumer;
import org.junit.jupiter.api.*;

import java.util.function.BiConsumer;

import static gr.iccs.presto.test_event_utilities.UtilityMethods.publish;
import static org.junit.jupiter.api.Assertions.*;

public class Tests {

    private static String SITUATIONS_TOPIC,REALTIME_MONITORING_TOPIC,PREDICTIONS_TOPIC;
    private static String BROKER_IP_ADDRESS;
    private static String example_situation, example_monitoring_event, example_prediction_event;
    private static String received_situation, received_monitoring_event, received_prediction_event;


    @BeforeAll

    static void initializeExternalResources() {
        System.out.println("Initializing external resources...");
        SITUATIONS_TOPIC = "situations";
        REALTIME_MONITORING_TOPIC = "monitoring";
        PREDICTIONS_TOPIC = "predicted";
        BROKER_IP_ADDRESS = "3.120.91.124";

        example_situation = "{\n" +
                "  \"event\": {\n" +
                "    \"rule_id\": \"0bce983d5b\",\n" +
                "    \"res_inst\": \"1cc5:1cc5:1cc5:1cc5:1cc5:1cc5:1cc5:1cc5\",\n" +
                "    \"zone\": 1,\n" +
                "    \"action\": \"scale_out\",\n" +
                "    \"attributes\": {\n" +
                "      \"custom_metric_1\": 14.084755145255173\n" +
                "    },\n" +
                "    \"timestamp\": 1568905129,\n" +
                "    \"fragid\": \"1cc5Fragment\"\n" +
                "  }\n" +
                "}";

        example_monitoring_event = "{\n" +
                "  \"disk_available\": 43.55321140192893,\n" +
                "  \"res_inst\": \"1cc5:1cc5:1cc5:1cc5:1cc5:1cc5:1cc5:1cc5\",\n" +
                "  \"net_sent\": 41.648744905912025,\n" +
                "  \"loadBalancerMonitoringModels\": [\n" +
                "    {\n" +
                "      \"linvocations_responsetime\": 8.923704487027747,\n" +
                "      \"linvocations_sec\": 977.8269553463543,\n" +
                "      \"loadbalancer_for_fragid\": \"1cc5Fragment\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"linvocations_responsetime\": 7.300072066907734,\n" +
                "      \"linvocations_sec\": 692.3686695744508,\n" +
                "      \"loadbalancer_for_fragid\": \"0613859f4f\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"linvocations_responsetime\": 0.36589727657737714,\n" +
                "      \"linvocations_sec\": 95.1675280780604,\n" +
                "      \"loadbalancer_for_fragid\": \"f687b3c5ff\"\n" +
                "    }\n" +
                "  ],\n" +
                "  \"cpu_user\": 42.05911152452124,\n" +
                "  \"cpu_io_wait\": 83.4826546977881,\n" +
                "  \"throughput_rate_fps\": 62.89336992779948,\n" +
                "  \"latency_ms\": 34.0522049134664,\n" +
                "  \"fragid\": \"1cc5Fragment\",\n" +
                "  \"cpu_sys\": 26.810479598158455,\n" +
                "  \"mem_perc\": 166.15661712635475,\n" +
                "  \"packets_sent\": 28.771534389358088,\n" +
                "  \"mem_free\": 69.31096967183824,\n" +
                "  \"packets_received\": 3210.1363981607738,\n" +
                "  \"net_rec\": 21.416854785195106,\n" +
                "  \"cpu_perc\": 67.30225605797057,\n" +
                "  \"response_time_ms\": 85.54960549235534,\n" +
                "  \"timestamp\": 1568977162\n" +
                "}";

        example_prediction_event = "";

    }

    @Test
    public void test_SubscribeToEvents_PublishSituations(){


        BiConsumer<String, String> message_handling_function = this::return_message;

        AmqpConsumer situations_consumer = new AmqpConsumer(BROKER_IP_ADDRESS,false, message_handling_function);
        AmqpConsumer monitoring_consumer = new AmqpConsumer(BROKER_IP_ADDRESS, false, message_handling_function);
        AmqpConsumer predictions_consumer = new AmqpConsumer(BROKER_IP_ADDRESS, false, message_handling_function);
        //TODO Create a similar consumer if needed to retrieve information from monitoring data
        try {
            situations_consumer.subscribe(SITUATIONS_TOPIC);
            monitoring_consumer.subscribe(REALTIME_MONITORING_TOPIC);
            predictions_consumer.subscribe(PREDICTIONS_TOPIC);
        }catch (Exception e){
            e.printStackTrace();
        }

        publish(SITUATIONS_TOPIC,example_situation);
        publish(REALTIME_MONITORING_TOPIC,example_monitoring_event);
        publish(PREDICTIONS_TOPIC,example_prediction_event);
        //allow some time for the message to be sent and received
        try {
            Thread.sleep(2000);
        }catch (Exception e){
            e.printStackTrace();
        }

        assertEquals(received_situation,example_situation);
        assertEquals(received_monitoring_event,example_monitoring_event);
        assertEquals(received_prediction_event,example_prediction_event);
    }

    public void return_message(String message, String topic){
        if (topic.startsWith(SITUATIONS_TOPIC)) {
            received_situation = message;
        }else if (topic.startsWith(REALTIME_MONITORING_TOPIC)){
            received_monitoring_event = message;
        }else if (topic.startsWith(PREDICTIONS_TOPIC)){
            received_prediction_event = message;
        }
    }

}
